# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 14:28:52 2020

Functions for file management

@author: Victor G. Marques (v.goncalvesmarques@maastrichtuniversity.nl)
"""

#%% Python libraries
import os
import scipy.io as sio

#%% Other dependencies



#%% 

def FastMatRead(filepath, struct,matfile, variable_name, struct_name = ' ',inner_struct = ''):
    '''
    This function speeds up the proccess of reading .mat files into Python.
    
    Parameters:
        - filepath: path to the .mat file.
        - struct: the number of structures (within each other) in the .mat file.
            In the moment, the possible values are 0 (no structure), 1 (simple 
            structure) and 2 (structure within structure).
        - matfile: name of the .mat file (with extension).
        - variable_name: variable to load from the .mat file.
        - struct_name: name of the outer structure.
        - inner_struct: name of the inner structure.

    '''
    cd = os.getcwd()
    os.chdir(filepath)
    if struct == 1:
        Struct  = sio.loadmat(matfile)
        variable = Struct[struct_name][variable_name][0, 0]
    elif struct==0: 
        variable = sio.loadmat(matfile)[variable_name]  
    elif struct==2:  
        outer_struct  = sio.loadmat(matfile)
        variable = outer_struct[struct_name][inner_struct][0,0][variable_name][0,0] # tbh, I don't know why I need these [0,0], but it works
    else:
        print('Invalid boolean')
    os.chdir(cd)
    
    return variable 
