# Atrial arrhythmia analysis in BSPM
This repository contains a series of functions related mostly to frequency and phase analysis of body surface potential mapping signals of atrial fibrillation, flutter and focal tachycardia. 

It is organized in modules:

- preprocessing: contains functions to manipulate the data shape prior to the subsequent analyses

- freq_analysis: contains the libraries with functions to calculate dominant frequency (DF), organization and regularity indexes (OI and RI, respectively). In the moment, this folder is subdivided in Models and Patients, both from Valencia. Ideally, this should be updated to include general BSPM layout and signal characteristics.

- phase_analysis: contains the libraries with functions to calculate Hilbert transform based phase maps, obtain singularity points and connect them over time in filaments. Functions are fit for Models from Valencia; the same remarks from above are valid here.

- feature_extraction: contains functions for extracting features based on the maps generated in the two above routines

- classification: contains functions for classifying the maps based on different sets of labels and features.

- fileIO: file management functions

- visualization: plotting functions

- accessory: other accessory functions that do not fit elsewhere

- scripts: contains scripts for generating the results observed in the publications from the libraries above.


This is work in progress, I am slowly adding the relevant functions and scripts.

A future goal for this projet would be to port the libraries to a more "object oriented" architecture, which would make it easier to develop a GUI.

## TODO list:

- Add scripts and fit with new paths
- Correct paths in the libraries, in particular the visualization library
- Add colormaps to visualization folder
