# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 14:28:52 2020

Functions making visualization of data easier

@author: Victor G. Marques (v.goncalvesmarques@maastrichtuniversity.nl)

"""

#%% Python libraries
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import animation
from matplotlib.colors import LinearSegmentedColormap
from mayavi import mlab
from tvtk.api import tvtk

#%% Other dependencies
root = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(root,'fileIO'))
sys.path.append(os.path.join(root,'accessory'))
sys.path.append(os.path.join(root,'phase_analysis'))

import fileIO_lib as fio
import accessory_lib as acs
import phaseAnalysis_lib as phl


#%% 2D

def add_heads(fig,ax,cax = None,cax2 = None):
    fig.set_size_inches([ 14.2 ,   9.7])
    fig_folder = os.path.join('..','visualization')
                              
    if cax == None: 
        ax.set_position([0.09,0.1,0.9,0.7])
        fig.text(0.53,0.85,'L',fontsize = 35)
        fig.text(0.96,0.85,'R',fontsize = 35)
        fig.text(0.09,0.85,'R',fontsize = 35)
        head_front = fig.add_axes([0.09,0.8,.45,.2])
        head_front.axis('off')
        head_front.imshow(plt.imread(fig_folder+'\head_front.png'))
        head_back = fig.add_axes([0.54,0.8,.45,.2])
        head_back.axis('off')
        head_back.imshow(plt.imread(fig_folder+'\head_back.png'))
    elif cax2 == None: 
        ax.set_position([0.085,0.1,0.8,0.7])
        cax.set_position([0.895,0.1,0.03,0.7]) 
        fig.text(0.485,0.85,'L',fontsize = 35)
        fig.text(0.855,0.85,'R',fontsize = 35)
        fig.text(0.085,0.85,'R',fontsize = 35)
        head_front = fig.add_axes([0.10,0.8,.4,.2])#[0.085,0.8,.4,.2])
        head_front.axis('off')
        head_front.imshow(plt.imread(fig_folder+'\head_front.png'))
        head_back = fig.add_axes([0.48,0.8,.4,.2])#[0.49,0.8,.4,.2])
        head_back.axis('off')
        head_back.imshow(plt.imread(fig_folder+'\head_back.png'))
    else:
        ax.set_position([0.09, 0.25, 0.7, 0.6])
        cax.set_position([0.09, 0.09, 0.7, 0.05])
        cax2.set_position([0.8, 0.25, 0.03, 0.6])
        fig.text(0.43,0.9,'L',fontsize = 35)
        fig.text(0.77,0.9,'R',fontsize = 35)
        fig.text(0.09,0.9,'R',fontsize = 35)
        
        head_front = fig.add_axes([0.09,0.85,.35,.15])
        head_front.axis('off')
        head_front.imshow(plt.imread(fig_folder+'\head_front.png'))
        head_back = fig.add_axes([0.44,0.85,.35,.15])
        head_back.axis('off')
        head_back.imshow(plt.imread(fig_folder+'\head_back.png'))

####

def animation_SP(phase,rot,fs,iniF = 0,endF = -1,nFrames = 0,
                 option = 'pcolormesh',cm = 0,save = False,
                 filename = 'animation_SP.mp4'):
    
    geom_path = os.path.join(os.path.join('D:\\','vgmar'),'Documents','Data',
                             'files_journal','Models','HR') # Attention: change this parameter

   
    xi = fio.fastMatRead(geom_path, 0,'xi.mat','xi' ).flatten()
    yi = fio.fastMatRead(geom_path, 0,'yi.mat','yi' ).flatten()
    
    if endF ==-1:
        endF = phase.shape[2]        
    length = endF-iniF
    if nFrames == 0:
        step = 1
        nFrames = length
    else:
        step = length/nFrames
    
    if cm == 0:
        filepath = os.path.join(os.path.join('D:\\','vgmar'),'Documents',
                                'MEGAsync','Master','Matlab','Colormaps')#Windows
#          
        matfile = 'colorCARTO_mod.mat'
#        variable = 'mycmap' 
        variable = 'C'    
        cm = fio.fastMatRead(filepath,0,matfile,variable)
        cm = LinearSegmentedColormap.from_list('cm', cm, N=255)
        
    if option == 'pcolormesh':
        fig, ax = plt.subplots(1,figsize=(16, 8))
    
#        ax.set(xlim=(0, phase.shape[1]-1), ylim=(0, phase.shape[0]-1))
        ax.set(xlim=(xi.min(),xi.max()), ylim=(yi.min(),yi.max()))
        ax.autoscale(False)
        ax.set_xlabel('cm',fontsize = 22)
        ax.set_ylabel('cm',fontsize = 22)
#        ax.axis('off')    
        cax = ax.pcolormesh(xi,yi,phase[:-1, :-1,iniF],cmap = cm)#,vmin = -np.pi,vmax = np.pi)
        x,y = np.where(rot[:,:,iniF]!=0)
        scat = ax.scatter(yi[x],xi[y],s = 150, edgecolors = 'k', marker = 'o',
                          facecolors='w',linewidth=2) 
        cbar = fig.colorbar(cax, ticks=[-np.pi, 0, np.pi])
        cbar.set_label('Phase (rad)', fontsize = 24)
        cbar.ax.set_yticklabels(['$-\pi$', '0', '$+\pi$'], fontsize = 24)   
        fig.suptitle('Frame %s '%(iniF), fontsize = 28)
        
        def animSP(i):
            cax.set_array(phase[:-1, :-1, int((i*step)+iniF)].flatten())
            x,y = np.where(rot[:,:,int((i*step)+iniF)]!=0)

            points = np.asarray([xi[y],yi[x]]).T
            
            scat.set_offsets(points) 
                
            fig.suptitle('Frame %s '%((i*step)+iniF), fontsize = 28)
            return fig
        
        anim = animation.FuncAnimation(fig, animSP, interval = 30, frames=nFrames,
                                   repeat_delay = 200)
        if save:
            writer = animation.FFMpegWriter(fps = 25)
            anim.save(filename, writer=writer)
        else:
            plt.show()
            return anim
    else:
        print('Sorry, pcolormesh is the only option so far.')

####

def animation_general(data1,fs,colormap,iniF = 0,endF = -1, nFrames = 0,
                      lim_vals = [],unit = 'mV',interval = 15,save = False,
                      filename = 'animation_general.mp4'):
    

    if endF ==-1:
        endF = data1.shape[2]      
        
    length = endF-iniF 
    
    if nFrames == 0:
        step = 1
        nFrames = length
    else: 
        step = length/nFrames

    current_cmap = cm.get_cmap(colormap)
    current_cmap.set_bad('gray',0.4) 


    ## Figure
    
    fig, ax = plt.subplots(1,figsize=(16, 8))
    fig.suptitle('Frame %s '%(iniF), fontsize = 28)
    
    ax.set(xlim=(0, data1.shape[1]-1), ylim=(0, data1.shape[0]-1))
    ax.autoscale(False)
    ax.axis('off')    
    if len(lim_vals)==0:
        cax1 = ax.pcolormesh(data1[:-1, :-1,0],cmap = current_cmap)
    elif len(lim_vals)==2:
        cax1 = ax.pcolormesh(data1[:-1, :-1,0],cmap = current_cmap,
                             vmin = lim_vals[0],vmax = lim_vals[1])
    else:
        print('Invalid limit values')

    fig.colorbar(cax1,ax = ax,label = unit)    
    
    def animSP(i):
        
        cax1.set_array(data1[:-1, :-1, int((i*step)+iniF)].flatten())
            
        fig.suptitle('Frame %s '%(i), fontsize = 28)
        return fig
    
    anim = animation.FuncAnimation(fig, animSP, interval = 15, frames=nFrames,
                               repeat_delay = 200)
    if save:
        writer = animation.FFMpegWriter()
        anim.save(filename, writer=writer) 
    else:
        plt.show()
        return anim

#%% 3D

def remove_nodes_mesh(vertices,faces,values,nodes_to_remove):
    '''
    removes a set of nodes from a mesh
    '''
    # Remove points from vertices (Atria)
    dict_new_vert = []
    dict_new_faces = []
   
    
    #Remove the face entries relative to the out_points
    for i in range(faces.shape[0]):
        if not any(elem in faces[i,:] for elem in nodes_to_remove):#np.sum([elem in nodes_to_remove for elem in faces[i,:]])!=0:
            dict_new_faces.append(faces[i,:]) #
            
    dict_new_faces = np.asarray(dict_new_faces)
    
    # Remove points from vertices relative to out_points
    # Store the relation between the new (k) and old (i) indexes
    k = 0
    for i in range(vertices.shape[0]):
        if i not in nodes_to_remove:
            dict_new_vert.append([k,i])
            k = k+1
    dict_new_vert = np.asarray(dict_new_vert)
      
    # Adjust faces and vertices to match new indexes
    new_values = np.zeros((len(dict_new_vert)))
    new_vert = np.zeros((len(dict_new_vert),3))
    new_faces = np.zeros((len(dict_new_faces),3),dtype = np.int64)
    
    
    
    for i in range(dict_new_vert.shape[0]):
        new_faces[dict_new_faces==dict_new_vert[i,1]] = dict_new_vert[i,0]
        new_vert[i,:] = vertices[dict_new_vert[i,1],:]
        new_values[i] = values[dict_new_vert[i,1]]
                                
    return new_vert,new_faces,new_values

####

def filaments_3d(filament_dict,full_shape,xi,yi,fs,
                    heatmap = False,colormap = 'jet',grid = True,HM_max = -1):
    
   
    ## Define color sequence for filaments
    color_dict = {0:(31/255,119/255,180/255),
    1:(255/255,127/255,14/255),
    2:(44/255,160/255,44/255),
    3:(214/255,39/255,40/255),
    4:(148/255,103/255,189/255),
    5:(140/255,86/255,75/255),
    6:(227/255,119/255,194/255),
    7:(127/255,127/255,127/255),
    8:(188/255,189/255,34/255),
    9:(23/255,190/255,207/255),
    10:(174/255,199/255,232/255),
    11:(255/255,187/255,120/255),
    12:(152/255,223/255,138/255),
    13:(255/255,152/255,150/255),
    14:(197/255,176/255,213/255),
    15:(196/255,156/255,148/255),
    16:(247/255,182/255,210/255),
    17:(199/255,199/255,199/255),
    18:(219/255,219/255,141/255),
    19:(158/255,218/255,229/255)}
        
    # some info from the axes
    range_x = xi[-1]-xi[0]
    range_y = yi[-1]-yi[0]
    rotors_fil = acs.dict2matrix(filament_dict,full_shape)

    # Plot the filaments
    Fig = mlab.figure(bgcolor = (1,1,1),size = (1050,900))
    for M,sp in enumerate(filament_dict):
        temp_sp = np.asarray(filament_dict[sp])
        temp_sp = temp_sp[temp_sp[:,2].argsort()]
        active_frames = np.unique(temp_sp[:,2])
        
        curr_sp = np.zeros((active_frames.shape[0],3))
        for i,f in enumerate(active_frames):
            xa,ya,_ = (np.mean(temp_sp[temp_sp[:,2]==f],axis = 0))
    
            curr_sp[i,:] = np.array([xi[int(ya)],yi[int(xa)],(1-int(f)/full_shape[-1])*100]) # /fs for s; axis is reverted in mayavi
        
        
        mlab.plot3d(curr_sp[:,2], curr_sp[:,0],curr_sp[:,1],color = color_dict[M%len(color_dict)],
                         tube_radius =0.75,extent = [curr_sp[:,2].min(),curr_sp[:,2].max(),
                                                     curr_sp[:,0].min(),curr_sp[:,0].max(),
                                                     curr_sp[:,1].min(),curr_sp[:,1].max(),],
                                                     transparent = False)
        
    if grid:
        grid_params = [0,100,-range_x/2,-range_x/2,yi[0],yi[-1]]

        x, y, z = np.meshgrid(np.linspace(0,100,11), np.linspace(xi[0],xi[-1],11),np.linspace(yi[0],yi[-1],11))
        # The actual points.
        pts = np.empty(z.shape + (3,), dtype=float)
        pts[..., 0] = x
        pts[..., 1] = y
        pts[..., 2] = z
        
        pts = pts.transpose(2, 1, 0, 3).copy()
        pts.shape = (int(pts.size / 3), 3)
        
        sg = tvtk.StructuredGrid(dimensions=x.shape, points=pts)
        d = mlab.pipeline.add_dataset(sg,extent = grid_params,color = (0,0,0))
        gx = mlab.pipeline.grid_plane(d,extent = grid_params,color = (0.3,0.3,0.3))
        gy = mlab.pipeline.grid_plane(d,extent = grid_params,color = (0.5,0.5,0.5))
        gy.grid_plane.axis = 'y'
        gz = mlab.pipeline.grid_plane(d,extent = grid_params,color = (0.3,0.3,0.3))
        gz.grid_plane.axis = 'z'
        
    if heatmap:
        cm_path = os.path.join('D:\\','vgmar','Documents','GitHub',
                      'BSPM-analysis','Library','colormap_custom_4.npy')  
        custom_cmap =  np.load(cm_path)
        
        HM = phl.HeatMap(-rotors_fil)/(full_shape[-1])*100
        
        if HM_max==-1:
            HM_max=np.max(HM)

        im = mlab.imshow(HM,colormap = colormap,vmin = 0,
                         extent = [yi[0]-yi[-1],0,xi[0],xi[-1],yi[0]+range_y/2,+range_y/2],
                         vmax = HM_max,interpolate = False)

        im.actor.orientation = [0, -90, 00]
        im.actor.position = [0,1,19] # experimental
        

        im.module_manager.scalar_lut_manager.lut.table = custom_cmap
        im.update_pipeline()
          
        
    Fig.scene.parallel_projection = True           
    Fig.scene.camera.parallel_scale = 65         
    mlab.view(azimuth=60, elevation=65, distance=270, focalpoint=[  7.36745464, -71.81685098, -21.11515557])#[0, xi[0], yi[0]])
    return Fig

