"""
Created on Tue Nov  5 14:43:59 2019

This function encompasses all of the figures generated for the Physiological Measurement
paper (ONLY FREQUENCY)

Index:
    1. Comparison between metrics in fdrive estimation
    2. DF maps 
    3. Spatial mask
    4. SSIM between layouts / Correlation between layouts
    5. Sensitivity and precision in SP detection
    6.1 Atrial HDF regions distributions (I think this does not go into the final paper)
    6.2 RA
    6.3 LA
    7. Filaments 3D
    8. Heatmaps (not in the final paper)
    9. Colorbars    
    10. Features along layouts

TODO: Function 6 needs to be replaced with the one that generates figures with the errors DF-atrial HDF

@author: vgmarques
"""
import numpy as np
import os
root = os.path.join('D:\\','vgmar')# expanduser does not work in my pc with 2 HDs
library_folder = os.path.join(root,'Documents','GitHub',
                              'BSPM-analysis','Library')
os.chdir(library_folder)
import refined_pf as pf
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from scipy.interpolate import griddata
from mayavi import mlab
import scipy.io as sio
import scipy.signal as sig
from  sklearn.metrics import balanced_accuracy_score,r2_score,confusion_matrix


folder_list = ['TA_ICV','TA_LSPV','TA_RAA','TA_RIPV',
                'Flutter_tip','Flutter_tiphor','Flutter_VCI','Flutter_VP',
                'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV',
                'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0',
                'Rotor_RIPV','AF_RotorRIPV','AF_RSPV']

# Atualizado em 15/10/2018
real_frequencies = np.array([4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.80,5.60,5.50,7.80,
                 7.60,5.40,5.80,5.80,5.5,5.4,6.8])
fs = 500
fs_low = 128
    
loc_list = ['IVC','LSPV','RAA','RIPV',
            'RA (CCW)','RA (CW)','IVC','PV',
            'LA (1)','LA (2)','LIPV','LSPV',
            'RA (1)','RA (2)','RAA (1)','RAA (2)',
            'RIPV (1)','RIPV (2)','RSPV']

layout = 'HR.mat'
Layouts = {'HR.mat':'HR',
           'layout_cardioinsight.mat':'layout_cardioinsight',
           'layout_leicester_healthy.mat':'layout_leicester_healthy',
           'layout_guillem.mat':'layout_guillem',
           'layout_salinet.mat':'layout_salinet',
           'layout_uniform32.mat':'layout_uniform32',
           'layout_uniform16.mat':'layout_uniform16',
           'layout_uniform64.mat':'layout_uniform64',
           'layout_bojarnejad.mat':'layout_bojarnejad',
           'layout_oosterom.mat':'layout_oosterom'}

layout_list = ['HR.mat','layout_cardioinsight.mat','layout_leicester_healthy.mat',
               'layout_guillem.mat','layout_salinet.mat','layout_uniform32.mat',
               'layout_uniform16.mat','layout_oosterom.mat']
layout_labels = ['567(HR)','252','131','67','64','32','16','12']


# for all maps
la_list = np.asarray(['TA_LSPV','TA_RIPV',
           'Flutter_VP',
           'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV','Rotor_RIPV',
           'AF_RotorRIPV','AF_RSPV'])

ra_list = np.asarray(['TA_ICV','TA_RAA',
           'Flutter_tip','Flutter_tiphor','Flutter_VCI',
           'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0'])

data_path = os.path.join(root,'Documents','Data')
geom_path = os.path.join(data_path,'files_journal','Models',Layouts[layout]) # Attention: change this parameter

cm_path = os.path.join(root,'Documents',
                                'MEGAsync','Master','Matlab','Colormaps')#Colormap

# Colormap

cm = pf.fastMatRead(cm_path,0,'DFColormaps.mat','mycmap')

CM = (cm-cm.min())/(cm.max()-cm.min())*255

CM_new = np.concatenate((CM,np.ones((CM.shape[0],1))*255),axis = 1)
CM_new = np.asarray(CM_new,dtype = np.uint8)

cm = LinearSegmentedColormap.from_list('cm', cm, N=255)

spat_filt= 0.07
if spat_filt == 0:
    DF_folder = 'DF_CBM_0p'#
else:
    DF_folder = 'DF_CBM_'+str(np.round(spat_filt*100,1))+'p'

figure_path = os.path.join(root,'Documents','Results','Models','Figures','CBM','R1')
#%% 1. Comparison between metrics in fdrive estimation

DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[layout],'CBM_R1',DF_folder)

df_results = pd.read_csv(DF_results_path+'\DF_results.csv')

measurements = ['Mean','Median','Mode','HDF']
methods = ['Method 1']
method_1 = ['mean_1','median_1','moda_1','HDF_1']

##### 

M1 = np.asarray([np.abs(df_results[i]-real_frequencies)/real_frequencies*100 for i in method_1]).T

AT_ind = 0
AFL_ind = 4
AF_ind = 8

spac_group = 1.5
spac_in = 0.75
width_box = 0.6
group_space = 0*width_box+2*spac_in+spac_group


#######
S = M1

fig, ax = plt.subplots(1,figsize = [14,8])
AT_plot = ax.boxplot([S[AT_ind:AFL_ind,0],S[AT_ind:AFL_ind,1],S[AT_ind:AFL_ind,2],S[AT_ind:AFL_ind,3]],
            positions = [0,group_space,2*group_space,3*group_space],
            widths = width_box,whis = [5,95],
            boxprops = {'linewidth':3,'color' : 'k','linestyle':'-'},
            medianprops = {'linewidth':3,'color':'gray'},
            whiskerprops = {'linewidth':2,'color' : 'k'},
            capprops = {'linewidth':3,'color' : 'k'},   
            flierprops = {'markerfacecolor':'None','markeredgecolor':'k', 'marker':'.','markersize':25})

AFL_plot = ax.boxplot([S[AFL_ind:AF_ind,0],S[AFL_ind:AF_ind,1],S[AFL_ind:AF_ind,2],S[AFL_ind:AF_ind,3]],
            positions = [spac_in,group_space+spac_in,2*group_space+spac_in,3*group_space+spac_in],
            widths = width_box,whis = [5,95],
            boxprops = {'linewidth':3,'color' : 'gray','linestyle':'-'},
            medianprops = {'linewidth':3,'color':'k'},
            whiskerprops = {'linewidth':2,'color' : 'gray'},
            capprops = {'linewidth':3,'color' : 'gray'},   
            flierprops = {'markerfacecolor':'None','markeredgecolor':'k', 'marker':'.','markersize':25})

AF_plot = ax.boxplot([S[AF_ind:,0],S[AF_ind:,1],S[AF_ind:,2],S[AF_ind:,3]],
            positions = [spac_in*2,group_space+spac_in*2,2*group_space+spac_in*2,3*group_space+spac_in*2],
            widths = width_box,whis = [5,95],
            boxprops = {'linewidth':3,'color' : 'k','linestyle':'--'},
            medianprops = {'linewidth':3,'color':'gray'},
#            meanprops = {'linewidth':3,'color':'gray','linestyle':'-'},showmeans = True,meanline = True,
            whiskerprops = {'linewidth':2,'color' : 'k','linestyle':'--'},
            capprops = {'linewidth':3,'color' : 'k'},   
            flierprops = {'markerfacecolor':'None','markeredgecolor':'k', 'marker':'.','markersize':25})

##
ax.spines['right'].set_visible(False);
ax.spines['top'].set_visible(False);
ax.spines['left'].set_linewidth(2);
ax.spines['bottom'].set_linewidth(2);

ax.set_xlim(-0.75,11.25)
#ax.axvspan(group_space-width_box,group_space+spac_in*2+width_box,color = 'lightgrey')
#ax.axvspan(3*group_space-width_box,3*group_space+spac_in*2+width_box,color = 'lightgrey')


ax.set_xticks([spac_in,group_space+spac_in,2*group_space+spac_in,3*group_space+spac_in])
ax.set_xticklabels(['Mean','Median','Mode','HDF'],fontsize = 40)

ax.tick_params(labelsize = 40)
ax.set_ylabel('Error from atrial HDF (%)',fontsize = 40)
ax.grid(1,which = 'major',axis = 'y')

plt.savefig(figure_path+'\\fdrive_error_'+str(spat_filt)+'.png',transparent = True)

#%% 2. DF maps 

# AT: 2, AFL: 6; AF: 9
M = 0;basename = folder_list[M]
method = 'HDF_1'
df_choice = {'HDF_0':'DF_0','HDF_1':'DF_1'}

filepath = os.path.join(data_path,'files_journal','Models',
                            Layouts[layout],basename,'raw_signals_CBM_R1',DF_folder)# attention: This parameter must be adjusted 
####
xi =  pf.fastMatRead(os.path.dirname(filepath), 0,'xi.mat', 'xi').flatten()
yi =  pf.fastMatRead(os.path.dirname(filepath), 0,'yi.mat', 'yi').flatten()

DF = pf.fastMatRead(filepath,0,'Maps.mat',df_choice[method]) #WTMM+Welch

fig,ax = plt.subplots(1,figsize = [10,6])

cax = ax.pcolormesh(xi,yi,DF,cmap = cm,vmin = 2,vmax = 14)

ax.spines['right'].set_visible(False);
ax.spines['top'].set_visible(False);
ax.spines['bottom'].set_visible(False);
ax.spines['left'].set_visible(False);
ax.set_xticks([])
ax.set_yticks([])
fig.tight_layout()

plt.savefig(figure_path+'\\df_map_ex_'+Layouts[layout]+'_M'+str(M)+'.png',transparent = True)
#layout_oosterom
### Uncomment if you want to generate several maps
for basename in folder_list:
    method = 'DF_1'
    savepath = os.path.join(figure_path,'Maps (160620)',Layouts[layout],method)

    DF  = pf.fastMatRead(os.path.join(data_path,'files_journal','Models',
                        Layouts[layout],basename,'raw_signals_CBM_R1',DF_folder), 0,'Maps.mat', method)
    
    fig,ax = plt.subplots(1,figsize = [12,6])
    cax = ax.pcolormesh(DF,vmin = 2,vmax = 14,cmap = cm)
    
    fig.subplots_adjust(top=0.90,bottom=0.055,left=0.03,right=0.8,hspace=0.2,wspace=0.2)
    
    cbar_ax = fig.add_axes([0.85, 0.1, 0.03, 0.8])
    cbar = fig.colorbar(cax, cax=cbar_ax,label = 'Hz')
    cbar.set_ticks(np.arange(2,15,2))
    cbar_ax.tick_params(labelsize = 30)
    cbar_ax.set_ylabel('Hz',fontsize = 40)
    
    ax.axis('off')
    
    if not os.path.exists(savepath):
        os.makedirs(savepath)    
    os.chdir(savepath)
    plt.savefig(basename+'.png')
    os.chdir(library_folder)
plt.close('all')

###Spatial downsample
#for basename in folder_list:
#    for L in layout_list:
#        method = 'DF_1'
#        savepath = os.path.join(figure_path,'Maps (080120)',Layouts[L],method)
#    
#        DF  = pf.fastMatRead(os.path.join(data_path,'files_journal','Models',
#                            Layouts[L],basename,'raw_signals_CBM_R1',DF_folder), 0,'Maps.mat', method)
#        
#        fig,ax = plt.subplots(1,figsize = [12,6])
#        cax = ax.pcolormesh(DF,vmin = 2,vmax = 14,cmap = cm)
#        
#        fig.subplots_adjust(top=0.90,bottom=0.055,left=0.03,right=0.8,hspace=0.2,wspace=0.2)
#        
#        cbar_ax = fig.add_axes([0.85, 0.1, 0.03, 0.8])
#        cbar = fig.colorbar(cax, cax=cbar_ax,label = 'Hz')
#        cbar.set_ticks(np.arange(2,15,2))
#        cbar_ax.tick_params(labelsize = 30)
#        cbar_ax.set_ylabel('Hz',fontsize = 40)
#        
#        ax.axis('off')
#        
#        if not os.path.exists(savepath):
#            os.makedirs(savepath)    
#        os.chdir(savepath)
#        plt.savefig(basename+'.png')
#        os.chdir(library_folder)
#    plt.close('all')

#%% 3. Spatial mask

spatial_percentage = np.linspace(0,10,11)

error_HDF = np.zeros((len(folder_list),len(spatial_percentage)))

for M,basename in enumerate(folder_list):
    
    filepath = os.path.join(data_path,'files_journal','Models',
                            Layouts[layout],basename,'raw_signals_CBM_R1',DF_folder)
    DF = pf.fastMatRead(filepath,0,'Maps.mat','DF_1')
    for j,p in enumerate(spatial_percentage):  
        dfs = np.sort(DF[~np.isnan(DF)].flatten())
        HDF = np.nanmax(dfs[:int(len(dfs)*(1-p/100))])
        
        error_HDF[M,j] = np.abs(HDF-real_frequencies[M])

###
current_plot = error_HDF

perc_error_general = np.array([np.mean(current_plot[:,i]) for i in range(current_plot.shape[-1])])#/current_plot[:,0]
perc_error_at = np.array([np.mean(current_plot[:4,i]) for i in range(current_plot.shape[-1])])#/current_plot[:4,0]
perc_error_afl =np.array([np.mean(current_plot[4:8,i]) for i in range(current_plot.shape[-1])])#/current_plot[4:8,0]
perc_error_af = np.array([np.mean(current_plot[8:,i]) for i in range(current_plot.shape[-1])])#/current_plot[8:,0]


std_at = np.array([np.std(current_plot[:4,i])/np.sqrt(4) for i in range(current_plot.shape[-1])])#/current_plot[:4,0]
std_afl= np.array([np.std(current_plot[4:8,i]/np.sqrt(4)) for i in range(current_plot.shape[-1])])#/current_plot[4:8,0]
std_af = np.array([np.std(current_plot[8:,i]/np.sqrt(11)) for i in range(current_plot.shape[-1])])#/current_plot[8:,0]

fig,ax = plt.subplots(3,1,sharex = True,sharey = True,figsize = (10,8))

ax[0].plot(spatial_percentage,perc_error_at,'k.-',linewidth = 3,markersize = 10)
ax[0].fill_between(spatial_percentage,perc_error_at-std_at,perc_error_at+std_at,
                  color = 'k',alpha = 0.3)

ax[0].spines['bottom'].set_visible(False);

ax[1].plot(spatial_percentage,perc_error_afl,'gray',marker = '.',linewidth = 3,markersize = 10)
ax[1].fill_between(spatial_percentage,perc_error_afl-std_afl,perc_error_afl+std_afl,
                  color = 'gray',alpha = 0.3)

ax[1].spines['bottom'].set_visible(False);

ax[2].plot(spatial_percentage,perc_error_af,'k.--',linewidth = 3,markersize = 10)
ax[2].fill_between(spatial_percentage,perc_error_af-std_af,perc_error_af+std_af,
                  color = 'k',alpha = 0.3)

ax[2].set_xlabel('Spatial mask size (% of torso)',fontsize = 30)

for AX in ax:
    AX.spines['right'].set_visible(False);
    AX.spines['top'].set_visible(False);
    AX.tick_params(labelsize = 28)
    AX.set_xticks(np.arange(0,11,1))
    AX.set_ylim([0,8.2]) #AX.set_ylim([0,201])#
    AX.set_yticks([0,4,8])#AX.set_yticks([0,100,200])
    AX.set_xlim([0,10.1])
    AX.grid(1,which = 'major',axis = 'both')

ax[1].set_ylabel('Error from atrial HDF (Hz)',fontsize = 30)#,rotation = 90)

plt.subplots_adjust(top=0.974,
bottom=0.123,
left=0.15,
right=0.963,
hspace=0.2,
wspace=0.2)

plt.savefig(figure_path+'\\spatial_mask.png',transparent = True)

#%% 4. SSIM between layouts / Correlation between layouts

method = 'SSIM_DF'#Corr_DF, SSIM_HM, Corr_HM

plot_data = np.zeros((3,len(layout_list[1:])))
std_data = np.zeros((3,len(layout_list[1:])))

for i,L in enumerate(layout_list[1:]):
    DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[L],'CBM_R1',DF_folder,'SSIM')
    ssim_results = pd.read_csv(DF_results_path+'\Comparison_HR.csv')
    
    
    plot_data[0,i] = np.mean(ssim_results[method][:4]) #AT only        
    plot_data[1,i] = np.mean(ssim_results[method][4:8]) #AFL only        
    plot_data[2,i] = np.mean(ssim_results[method][8:]) #AF only        
    
    std_data[0,i] = np.std(ssim_results[method][:4]) #AT only        
    std_data[1,i] = np.std(ssim_results[method][4:8] )#AFL only        
    std_data[2,i] = np.std(ssim_results[method][8:]) #AF only        


###
boxcolors = ['#CA3542','#27647B','#849FAD']

fig,ax = plt.subplots(3,1,sharex = True,sharey = True,figsize = (8.6,8))

ax[0].plot(np.linspace(0,7,7),plot_data[0,:],'.--',color = boxcolors[0],marker = '.',linewidth = 3,markersize = 25)
ax[0].fill_between(np.linspace(0,7,7),plot_data[0,:]-std_data[0,:],
                  plot_data[0,:]+std_data[0,:],
                  color = boxcolors[0],alpha = 0.3)

ax[0].spines['bottom'].set_visible(False);
#ax[0].set_ylabel('AT',fontsize = 20)

ax[1].plot(np.linspace(0,7,7),plot_data[1,:],'--',color = boxcolors[1],marker = 's',linewidth = 3,markersize = 15)
ax[1].fill_between(np.linspace(0,7,7),plot_data[1,:]-std_data[1,:],
                      plot_data[1,:]+std_data[1,:],
                      color =  boxcolors[1],alpha = 0.3)

ax[1].spines['bottom'].set_visible(False);
#ax[1].set_ylabel('AFL',fontsize = 20)

ax[2].plot(np.linspace(0,7,7),plot_data[2,:],'--',color = boxcolors[2],marker = '^',linewidth = 3,markersize = 15)
ax[2].fill_between(np.linspace(0,7,7),plot_data[2,:]-std_data[2,:],
                      plot_data[2,:]+std_data[2,:],
                      color = boxcolors[2],alpha = 0.3)

ax[2].set_xticks(np.linspace(0,7,7))
ax[2].set_xticklabels(layout_labels[1:],fontsize = 30)
ax[2].set_xlabel('Number of leads',fontsize = 30)

for AX in ax:
    AX.spines['right'].set_visible(False);
    AX.spines['top'].set_visible(False);
    AX.tick_params(labelsize = 30)
    AX.set_ylim([0,1]) #AX.set_ylim([0,201])#
    AX.set_yticks([0,0.5,1])#AX.set_yticks([0,100,200])
    AX.set_yticklabels([0,'',1])#AX.set_yticks([0,100,200])
#    AX.set_xlim([0,6)
    AX.grid(1,which = 'major',axis = 'both')

ax[1].set_ylabel(r'SSIM$_{HM}$',fontsize = 40)#,rotation = 90)


plt.subplots_adjust(top=0.962,
bottom=0.138,
left=0.119,
right=0.984,
hspace=0.302,
wspace=0.2)

plt.savefig(figure_path+'\\'+method+'.png',transparent = True)

#%% 5. Sensitivity and precision in SP detection

method = 'precision'#precision

plot_data = np.zeros((3,len(layout_list[1:])))
std_data = np.zeros((3,len(layout_list[1:])))

for i,L in enumerate(layout_list[1:]):
    DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[L],'CBM_R1',DF_folder,'Rotor stats')
    ssim_results = pd.read_csv(DF_results_path+'\Sens_prec_5.csv')
    
    
    plot_data[0,i] = np.mean(ssim_results[method][:4]) #AT only        
    plot_data[1,i] = np.mean(ssim_results[method][4:8]) #AFL only        
    plot_data[2,i] = np.mean(ssim_results[method][8:]) #AF only        
    
    std_data[0,i] = np.std(ssim_results[method][:4]) #AT only        
    std_data[1,i] = np.std(ssim_results[method][4:8])  #AFL only        
    std_data[2,i] = np.std(ssim_results[method][8:])  #AF only        

###
boxcolors = ['#CA3542','#27647B','#849FAD']
fig,ax = plt.subplots(3,1,sharex = True,sharey = True,figsize = (8.6,8))

ax[0].plot(np.linspace(0,7,7),plot_data[0,:],'.--',color = boxcolors[0],marker = '.',linewidth = 3,markersize = 25)
ax[0].fill_between(np.linspace(0,7,7),plot_data[0,:]-std_data[0,:],
                  plot_data[0,:]+std_data[0,:],
                  color = boxcolors[0],alpha = 0.3)

ax[0].spines['bottom'].set_visible(False);
#ax[0].set_ylabel('AT',fontsize = 20)

ax[1].plot(np.linspace(0,7,7),plot_data[1,:],'--',color = boxcolors[1],marker = 's',linewidth = 3,markersize = 15)
ax[1].fill_between(np.linspace(0,7,7),plot_data[1,:]-std_data[1,:],
                      plot_data[1,:]+std_data[1,:],
                      color =  boxcolors[1],alpha = 0.3)

ax[1].spines['bottom'].set_visible(False);
#ax[1].set_ylabel('AFL',fontsize = 20)

ax[2].plot(np.linspace(0,7,7),plot_data[2,:],'--',color = boxcolors[2],marker = '^',linewidth = 3,markersize = 15)
ax[2].fill_between(np.linspace(0,7,7),plot_data[2,:]-std_data[2,:],
                      plot_data[2,:]+std_data[2,:],
                      color = boxcolors[2],alpha = 0.3)

ax[2].set_xticks(np.linspace(0,7,7))
ax[2].set_xticklabels(layout_labels[1:],fontsize = 30)
ax[2].set_xlabel('Number of leads',fontsize = 30)

for AX in ax:
    AX.spines['right'].set_visible(False);
    AX.spines['top'].set_visible(False);
    AX.tick_params(labelsize = 30)
    AX.set_ylim([0,1.1]) #AX.set_ylim([0,201])#
    AX.set_yticks([0,0.5,1])#AX.set_yticks([0,100,200])
    AX.set_yticklabels([0,'',1])#AX.set_yticks([0,100,200])
#    AX.set_xlim([0,6)
    AX.grid(1,which = 'major',axis = 'both')

ax[1].set_ylabel('Precision',fontsize = 40)#,rotation = 90)


plt.subplots_adjust(top=0.962,
bottom=0.138,
left=0.119,
right=0.984,
hspace=0.302,
wspace=0.2)

plt.savefig(figure_path+'\\'+method+'.png',transparent = True)
#%% 6. Atrial HDF regions distributions

DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[layout],'CBM_R1',DF_folder)

geom_path = os.path.join(root,'Documents','MEGAsync',
                              'Master','BSPM Signals','models_Miguel') 

Cpts_raw = pf.fastMatRead(geom_path,0,'Cpts_raw.mat','Cpts')

filepath = os.path.join(data_path,'files_journal','Models',
                            Layouts[layout],'TA_ICV',
                            'raw_signals_CBM_R1',DF_folder)
xi =  pf.fastMatRead(os.path.dirname(filepath), 0,'xi.mat', 'xi').flatten()
yi =  pf.fastMatRead(os.path.dirname(filepath), 0,'yi.mat', 'yi').flatten()


xx,yy = np.meshgrid(xi,yi)
xx = xx.flatten()
yy = yy.flatten()    

torso_vert = pf.fastMatRead(geom_path,1,'Torso.mat','vertices','Torso')
torso_faces = pf.fastMatRead(geom_path,1,'Torso.mat','faces','Torso')-1

layout_points = pf.fastMatRead(geom_path, 0,layout,'vertices')
indexes = pf.fastMatRead(geom_path, 0,layout,'indexes')-1 # Correct matlab indexing
 

df_results = pd.read_csv(DF_results_path+'\DF_results.csv')

model_freq = df_results['HDF_1'] # Method

DF_binarized = {}
for M, basename in enumerate(folder_list):

    df_path = os.path.join(data_path,'files_journal','Models',
                             Layouts[layout],basename,DF_folder) # Signals
    DF = pf.fastMatRead(df_path, 0,'Maps.mat','DF_1')
    r,c = DF.shape
    # HDF
    DF_thresh = np.copy(DF)
    DF_thresh[DF<(model_freq[M]-1)] = 0
    DF_thresh[DF>(model_freq[M]+1)] = 0
    DF_thresh[DF_thresh!=0] = 1

    DF_binarized[basename] = DF_thresh
    

#%% RA
ra_list = np.asarray(['TA_ICV','TA_RAA',
           'Flutter_tip','Flutter_tiphor','Flutter_VCI',
           'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0'])

arr_ranges  = {'AT':[0,2],'AFL':[2,5],'AF':[5,9],'all':[0,8]}

arrhythmia= 'AF'
perc_fdrive = np.zeros_like(DF)
for basename in ra_list[arr_ranges[arrhythmia][0]:arr_ranges[arrhythmia][1]]:
    perc_fdrive += DF_binarized[basename]

perc_fdrive =perc_fdrive/len(ra_list[arr_ranges[arrhythmia][0]:arr_ranges[arrhythmia][1]])*100
#
Pq_df_ra = griddata((xx, yy), (perc_fdrive).flatten(),
                 (Cpts_raw[:,0],Cpts_raw[:,1]),
                 method='nearest')
scalars_torso = []

for i in range(len(Pq_df_ra)):
    if i in indexes:
        scalars_torso.append(Pq_df_ra[i])
    else:
        scalars_torso.append(Pq_df_ra[i])
scalars_torso = np.asarray(scalars_torso)

Fig = mlab.figure(bgcolor = (1,1,1))

T = mlab.triangular_mesh(torso_vert[:,0],
                torso_vert[:,1],
                torso_vert[:,2],
                torso_faces,
                scalars = scalars_torso,
                colormap = 'jet',vmax = 100,vmin = 0,
                opacity = 1,name  = 'Torso')
T.module_manager.scalar_lut_manager.lut.table = np.load('colormap_percentage.npy')

mlab.view(-50,80,distance = 135)
imgmap = mlab.screenshot(figure=Fig, mode='rgba', antialiased=True)

plt.imsave(os.getcwd()+'\\figures\\front_df0_ra.png',imgmap)
plt.imsave(os.getcwd()+'\\figures\\front_df1_ra.png',imgmap)
#
mlab.view(120,80,distance = 135)
imgmap = mlab.screenshot(figure=Fig, mode='rgba', antialiased=True)

plt.imsave(os.getcwd()+'\\figures\\back_df0_ra.png',imgmap)
plt.imsave(os.getcwd()+'\\figures\\back_df1_ra.png',imgmap)
#%% LA
la_list = np.asarray(['TA_LSPV','TA_RIPV',
           'Flutter_VP',
           'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV','Rotor_RIPV',
           'AF_RotorRIPV','AF_RSPV'])

arr_ranges  = {'AT':[0,2],'AFL':[2,3],'AF':[3,10],'all':[0,10]}

arrhythmia= 'AF'
perc_fdrive = np.zeros_like(DF)
for basename in la_list[arr_ranges[arrhythmia][0]:arr_ranges[arrhythmia][1]]:
    perc_fdrive += DF_binarized[basename]

perc_fdrive =perc_fdrive/len(la_list[arr_ranges[arrhythmia][0]:arr_ranges[arrhythmia][1]])*100

Pq_df_la = griddata((xx, yy), (perc_fdrive).flatten(),
                 (Cpts_raw[:,0],Cpts_raw[:,1]),
                 method='nearest')
scalars_torso = []

for i in range(len(Pq_df_ra)):
    if i in indexes:
        scalars_torso.append(Pq_df_la[i])
    else:
        scalars_torso.append(Pq_df_la[i])
scalars_torso = np.asarray(scalars_torso)

Fig = mlab.figure(bgcolor = (1,1,1))

T = mlab.triangular_mesh(torso_vert[:,0],
                torso_vert[:,1],
                torso_vert[:,2],
                torso_faces,
                scalars = scalars_torso,
                colormap = 'jet',vmax = 100,vmin = 0,
                opacity = 1,name  = 'Torso')
T.module_manager.scalar_lut_manager.lut.table = np.load('colormap_percentage.npy')

mlab.view(-50,80,distance = 135)
imgmap = mlab.screenshot(figure=Fig, mode='rgba', antialiased=True)

plt.imsave(os.getcwd()+'\\figures\\front_df0_la.png',imgmap)
plt.imsave(os.getcwd()+'\\figures\\front_df1_la.png',imgmap)

####
mlab.view(120,80,distance = 135)
imgmap = mlab.screenshot(figure=Fig, mode='rgba', antialiased=True)

plt.imsave(os.getcwd()+'\\figures\\back_df0_la.png',imgmap)
plt.imsave(os.getcwd()+'\\figures\\back_df1_la.png',imgmap)

#%% 7. Filaments 3D

# AT: 2, AFL: 6; AF: 9
xi =  pf.fastMatRead(os.path.join(data_path,'files_journal','Models','HR'), 0,'xi.mat', 'xi').flatten()
yi =  pf.fastMatRead(os.path.join(data_path,'files_journal','Models','HR'), 0,'yi.mat', 'yi').flatten()

<<<<<<< HEAD
layout = layout_list[-2]
SP_folder = 'SP_CBM'

M =8 ; basename = folder_list[M]
=======
layout = layout_list[0]
SP_folder = 'SP_CBM'

M =16 ; basename = folder_list[M]
>>>>>>> adapted_for_ML
load_path = os.path.join(data_path,'files_journal','Models',Layouts[layout],
                         basename,'raw_signals_CBM_R1',DF_folder,'SP_CBM')

rotors = pf.fastMatRead(load_path,0,'SP_analysis.mat','rotors')
HMG = pf.fastMatRead(load_path,0,'SP_analysis.mat','HMG')
#
## Load dicts
SPs_filt = sio.loadmat(load_path+'/SPs_filt.mat')
del SPs_filt['__header__']
del SPs_filt['__globals__']
del SPs_filt['__version__']
#
Fig = pf.filaments_3d(SPs_filt,HMG.shape,xi,yi,128,
                    heatmap = True,grid = True,HM_max = 50)

imgmap = mlab.screenshot(figure=Fig)#, mode='rgba') # Something is wrong with this method
plt.imsave(figure_path+'\\filaments_ex_M'+str(M)+'.png',imgmap)

#%% 8. Heatmaps (not in the final paper)
## Colormap

layout = layout_list[0]
SP_folder = 'SP_CBM'


cm_hm = np.load('colormap_custom_4.npy')
cm_hm = (cm_hm-cm_hm.min())/(cm_hm.max()-cm_hm.min())
cm_hm = LinearSegmentedColormap.from_list('cm_hm', cm_hm, N=255)

M = 7;basename = folder_list[M]

load_path = os.path.join(data_path,'files_journal','Models',Layouts[layout],
                         basename,'raw_signals_CBM_R1',DF_folder,
                         SP_folder)

HMG = pf.fastMatRead(load_path,0,'SP_analysis.mat','HMG')
#
## Load dicts
SPs_filt = sio.loadmat(load_path+'/SPs_filt.mat')
del SPs_filt['__header__']
del SPs_filt['__globals__']
del SPs_filt['__version__']

rotors_fil = pf.dict2matrix(SPs_filt,HMG.shape)

HM = pf.HeatMap(-rotors_fil)/(HMG.shape[-1])*100
####

fig,ax = plt.subplots(1)

cax = ax.pcolormesh(HM,cmap = cm_hm,vmin = 0,vmax = 50)

cbar = fig.colorbar(cax,extend = 'min')
cbar.set_label('% of SPs',fontsize = 50)
cbar.ax.tick_params(labelsize = 40)
pf.add_heads(fig,ax,cbar.ax)


ax.set_xticks([])
ax.set_yticks([])

plt.savefig(figure_path+'\\HM_'+str(M)+'_'+str(Layouts[layout])+'.png',transparent = True)
#%% 9. Colorbars

# DF-atrial HDF distribution
generic_data = np.ones((30,30))
generic_data[0,:] = np.linspace(0,2,30)

## Colormap
cm_fils = np.load('colormap_percentage.npy')
cm_fils = (cm_fils-cm_fils.min())/(cm_fils.max()-cm_fils.min())
cm_fils = LinearSegmentedColormap.from_list('cm', cm_fils, N=255)
cax = plt.pcolor(generic_data,vmin = 0,vmax = 2, cmap = cm_fils)

# Now adding the colorbar
fig = plt.figure(figsize = [4.6,9.8])
cbaxes = fig.add_axes([0.2, 0.1, 0.1, 0.8]) 
cb = plt.colorbar(cax, cax = cbaxes,aspect = 10,extend = 'max') 
cb.ax.set_yticks(np.arange(0,101,10))
cb.ax.set_ylabel('Average |DF-Atrial HDF|',fontsize = 35)
cb.ax.tick_params('both',labelsize = 40)

plt.savefig(figure_path+'\\colorbar_DF.png',transparent = 1)
plt.close()
## SPs

cm_fils = np.load('colormap_custom_4.npy')
cm_fils = (cm_fils-cm_fils.min())/(cm_fils.max()-cm_fils.min())
cm_fils = LinearSegmentedColormap.from_list('cm', cm_fils, N=255)

generic_data = np.ones((30,30))
generic_data[0,:] = np.linspace(0,100,30)

cax = plt.pcolor(generic_data,vmin = 0,vmax = 50, cmap = cm_fils)

# Now adding the colorbar 
fig = plt.figure()
cbaxes = fig.add_axes([0.1, 0.5, 0.8, 0.2]) 
cb = plt.colorbar(cax, cax = cbaxes,aspect = 10,extend = 'both',orientation = 'horizontal') 
cb.ax.set_yticks(np.arange(0,51,10))
cb.ax.set_xlabel('% of SPs',fontsize = 40)

cb.ax.tick_params('both',labelsize = 40)

plt.savefig(figure_path+'\\colorbar_HMs.png',transparent = 1)
plt.close()


## DF
#generic_data = np.ones((30,30))
#generic_data[0,:] = np.linspace(2,14,30)
#cax = ax.pcolor(generic_data,vmin = 2,vmax = 14, cmap = cm)
#fig = plt.figure()
## Now adding the colorbar
##cbaxes = fig.add_axes([0.2, 0.1, 0.1, 0.8]) 
#cbaxes = fig.add_axes([0.1, 0.5, 0.8, 0.2]) 
#cb = plt.colorbar(cax, cax = cbaxes,aspect = 10,orientation = 'horizontal') 
##cb.ax.set_xticks(np.arange(0,101,10))
#cb.ax.set_xticks(np.arange(2,14,2))
##cb.ax.set_ylabel('% of SPs',fontsize = 40)
#cb.ax.set_xlabel('Hz',fontsize = 40)
#
#cb.ax.tick_params('both',labelsize = 40)
##plt.savefig(os.getcwd()+'\\figures\\colorbar_DF.png',transparent = 1)


#%% 10. Features along layouts
feature = 'number of regions'

name_dict = {'number of regions':'# of HDF regions',
             'avg. size':'HDF region size (% of torso)',
             'range':'DF range (Hz)',
             'total area':'Total HDF area (% of torso)',
             'SSIM_DF':r'SSIM$_{DF}$','Corr_DF':'Correlation',
             'SSIM_HM':r'SSIM$_{DF}$','Corr_HM':'Correlation',
             'sensitivity':'Sensitivity in SP detection',
             'precision':'Precision in SP detection',
             'Mean Duration (% duration)':'Filament duration (% of signal)',
             'Mean Frequency (Hz)':'Filament frequency (Hz)',
             'Filament rate (N fil/s)':'Filaments per second',
             'Number of regions':'# of SP clusters',
             'Mean region size':'SP cluster size (% of torso)',
             'Mean  %SP/area':'SP density',
             'SPs/s':'SPs/s',
             'HDF_1':r'HDF$_{BSPM,7\%}$'}



if feature in ['SSIM_DF','Corr_DF','SSIM_HM','Corr_HM']:
    means_plot = np.zeros((3,len(layout_list[1:])))
    std_plot = np.zeros((3,len(layout_list[1:])))
    
    for i,layout in enumerate(layout_list[1:]):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',
                               Layouts[layout],'CBM_R1',DF_folder,'SSIM')
        data = pd.read_csv(filepath+'/Comparison_HR.csv')
        means_plot[0,i] = np.mean(data[feature][:4])
        means_plot[1,i] = np.mean(data[feature][4:8])
        means_plot[2,i] = np.mean(data[feature][8:])
        
        std_plot[0,i] = np.std(data[feature][:4])
        std_plot[1,i] = np.std(data[feature][4:8])
        std_plot[2,i] = np.std(data[feature][8:])
##   
elif feature in ['sensitivity','precision']:
    means_plot = np.zeros((3,len(layout_list[1:])))
    std_plot = np.zeros((3,len(layout_list[1:])))
    
    for i,layout in enumerate(layout_list[1:]):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',
                               Layouts[layout],'CBM_R1',DF_folder,'Rotor stats')
        data = pd.read_csv(filepath+'/Sens_prec_5.csv')
        means_plot[0,i] = np.mean(data[feature][:4])
        means_plot[1,i] = np.mean(data[feature][4:8])
        means_plot[2,i] = np.mean(data[feature][8:])
        
        std_plot[0,i] = np.std(data[feature][:4])
        std_plot[1,i] = np.std(data[feature][4:8])
        std_plot[2,i] = np.std(data[feature][8:])
##
elif feature in ['number of regions','avg. size','range','total area']:
    means_plot = np.zeros((3,len(layout_list)))
    std_plot = np.zeros((3,len(layout_list)))
    
    for i,layout in enumerate(layout_list):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',
                               Layouts[layout],'CBM_R1',DF_folder)
        data = pd.read_csv(filepath+'\\regions_hdf.csv')
        means_plot[0,i] = np.mean(data[feature][:4])
        means_plot[1,i] = np.mean(data[feature][4:8])
        means_plot[2,i] = np.mean(data[feature][8:])
        
        if feature in ['number of regions','range','total area']:
            std_plot[0,i] = np.std(data[feature][:4])
            std_plot[1,i] = np.std(data[feature][4:8])
            std_plot[2,i] = np.std(data[feature][8:]) 
        else:
            std_plot[0,i] = np.std(data[feature][:4])/np.sqrt(4)
            std_plot[1,i] = np.std(data[feature][4:8])/np.sqrt(4)
            std_plot[2,i] = np.std(data[feature][8:])/np.sqrt(11)   
##
elif feature in ['Mean Duration (% duration)','Mean Frequency (Hz)','Filament rate (N fil/s)']:
    means_plot = np.zeros((3,len(layout_list)))
    std_plot = np.zeros((3,len(layout_list)))
    
    for i,layout in enumerate(layout_list):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',
                               Layouts[layout],'CBM_R1',DF_folder)
        data = pd.read_csv(filepath+'/filament_analysis.csv')
        means_plot[0,i] = np.mean(data[feature][:4])
        means_plot[1,i] = np.mean(data[feature][4:8])
        means_plot[2,i] = np.mean(data[feature][8:])
        
        if feature=='Filament rate (N fil/s)':
            std_plot[0,i] = np.std(data[feature][:4])
            std_plot[1,i] = np.std(data[feature][4:8])
            std_plot[2,i] = np.std(data[feature][8:]) 
        else:
            std_plot[0,i] = np.std(data[feature][:4])/np.sqrt(4)
            std_plot[1,i] = np.std(data[feature][4:8])/np.sqrt(4)
            std_plot[2,i] = np.std(data[feature][8:])/np.sqrt(11)   
##
elif feature in ['Number of regions','Mean region size','Mean  %SP/area','SPs/s']:
    means_plot = np.zeros((3,len(layout_list)))
    std_plot = np.zeros((3,len(layout_list)))
    
    for i,layout in enumerate(layout_list):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',
                               Layouts[layout],'CBM_R1',DF_folder)
        data = pd.read_csv(filepath+'/heatmap_filaments_analysis.csv')
        means_plot[0,i] = np.mean(data[feature][:4])
        means_plot[1,i] = np.mean(data[feature][4:8])
        means_plot[2,i] = np.mean(data[feature][8:])
        
        if feature=='SPs/s':
            std_plot[0,i] = np.std(data[feature][:4])
            std_plot[1,i] = np.std(data[feature][4:8])
            std_plot[2,i] = np.std(data[feature][8:]) 
        else:
            std_plot[0,i] = np.std(data[feature][:4])/np.sqrt(4)
            std_plot[1,i] = np.std(data[feature][4:8])/np.sqrt(4)
            std_plot[2,i] = np.std(data[feature][8:])/np.sqrt(11)  
elif feature in ['HDF_1']:
    means_plot = np.zeros((3,len(layout_list)))
    std_plot = np.zeros((3,len(layout_list)))
    
    for i,layout in enumerate(layout_list):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',
                               Layouts[layout],'CBM_R1',DF_folder)
        data = pd.read_csv(filepath+'/DF_results.csv')
        means_plot[0,i] = np.mean(data[feature][:4])
        means_plot[1,i] = np.mean(data[feature][4:8])
        means_plot[2,i] = np.mean(data[feature][8:])
        
        if feature=='SPs/s':
            std_plot[0,i] = np.std(data[feature][:4])
            std_plot[1,i] = np.std(data[feature][4:8])
            std_plot[2,i] = np.std(data[feature][8:]) 
        else:
            std_plot[0,i] = np.std(data[feature][:4])/np.sqrt(4)
            std_plot[1,i] = np.std(data[feature][4:8])/np.sqrt(4)
            std_plot[2,i] = np.std(data[feature][8:])/np.sqrt(11) 

centers = np.linspace(0,14,len(layout_list[0:]))

vspancolor = ['k','gray','#404040']
boxcolors = ['#CA3542','#27647B','#849FAD']
medcolors = ['#CA3542','#27647B','#849FAD']
box_width = 0.5
spac = box_width*1.05
space_between = (centers[1]-spac-box_width/2)-(centers[0]+spac+box_width/2)

pos  = np.asarray([centers[0]-spac,centers[0],centers[0]+spac,
        centers[1]-spac,centers[1],centers[1]+spac,
        centers[2]-spac,centers[2],centers[2]+spac,
        centers[3]-spac,centers[3],centers[3]+spac,
        centers[4]-spac,centers[4],centers[4]+spac,
        centers[5]-spac,centers[5],centers[5]+spac,
        centers[6]-spac,centers[6],centers[6]+spac,
        centers[7]-spac,centers[7],centers[7]+spac
        ])
pos = pos.reshape((int(pos.shape[0]/3),3))



fig,ax = plt.subplots(1,figsize = (7.5,4))
ax.bar(pos[:,0],means_plot[0,:],width = box_width,color = boxcolors[0],yerr = std_plot[0,:],ecolor = vspancolor[0],capsize = 3)
ax.bar(pos[:,1],means_plot[1,:],width = box_width,color = boxcolors[1],yerr = std_plot[1,:],ecolor = vspancolor[1],capsize = 3)
ax.bar(pos[:,2],means_plot[2,:],width = box_width,color = boxcolors[2],yerr = std_plot[2,:],ecolor = vspancolor[2],capsize = 3)

# Error bars

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['bottom'].set_linewidth(2)
ax.spines['left'].set_linewidth(2)

ax.set_xlim([centers[0]-spac-box_width,centers[-1]+spac+box_width])
ax.set_xticks(centers)
ax.set_xticklabels(layout_labels[0:],fontsize = 14)


lim_y = 8
int_y = 2

ax.set_yticks(np.arange(0,lim_y+1,int_y))
ax.set_yticklabels(np.round(np.arange(0,lim_y+0.1,int_y),0),fontsize = 14)
ax.set_ylim(0,lim_y)
ax.set_ylabel(name_dict[feature],fontsize = 18)
fig.tight_layout()

#plt.savefig(figure_path+'\\Fils_s'+'.png',transparent = True)
#plt.savefig(figure_path+'\\SP_density'+'.png',transparent = True)
#plt.savefig(figure_path+'\\SP_s'+'.png',transparent = True)
plt.savefig(figure_path+'\\'+feature+'.png',transparent = True)

#%% 11. Confusion matrices
layout = layout_list[0]

class_path = os.path.join(root,'Documents','Results', 'Models', Layouts[layout],'CBM_R1',DF_folder)  

task = 'arr_class_output_new.csv' # #'arr_class_output.npy', 'loc_class_output.npy', 'reentrant x focal.npy'
N_class = 3
class_results = pd.read_csv(os.path.join(class_path,task))

# Generate confusion matrices
method = 'RF'
plot_matrix = confusion_matrix(class_results['labels'],class_results[method])
plot_matrix.dtype= np.float

for i in range(plot_matrix.shape[0]):
    plot_matrix[i,:] = plot_matrix[i,:]/np.sum(plot_matrix[i,:])*100

fig,ax = plt.subplots(1,figsize = [9,6.5])
cax = ax.matshow(plot_matrix,vmin = 0,vmax = 100,cmap = 'Blues')
ax.xaxis.set_ticks_position('bottom')

ax.set_xticks(np.linspace(0,N_class-1,N_class)); ax.set_yticks(np.linspace(0,N_class-1,N_class))
ax.set_xlim([0-0.5,(N_class-1)+0.5])
ax.set_ylim([(N_class-1)+0.5,0-0.5])


#Manual
ax.set_xticklabels(['AT','AFL','AF'],fontsize = 40)
ax.set_yticklabels(['AT','AFL','AF'],fontsize = 40)

#ax.set_xticklabels(['LA','RA'],fontsize = 40)
#ax.set_yticklabels(['LA','RA'],fontsize = 40)
cbar = fig.colorbar(cax)
cbar.ax.tick_params(labelsize = 30)
cbar.set_label('%',fontsize = 30)

ax.set_xlabel('Predicted class',fontsize = 40)
ax.set_ylabel('True class',fontsize = 40)

fig.tight_layout()
plt.savefig(figure_path+'\\CM_arr_'+method+'_'+str(Layouts[layout])+'.png',transparency =True)
#plt.savefig(figure_path+'\\CM_loc_'+method+'_'+str(Layouts[layout])+'.png',transparency =True)
Acc  = balanced_accuracy_score(class_results['labels'],class_results[method])

#%% 12. Balanced accuracy over layouts

task = 'arr_class_output_new.csv' # #'arr_class_output.npy', 'loc_class_output.npy', 'reentrant x focal.npy'
N_class = 3
method = 'RF'

if N_class==3:
    acc_layouts = {'balanced_mean':np.zeros(8),'AT':np.zeros(8),'AFL':np.zeros(8),'AF':np.zeros(8)}
    acc_layouts_std = {'balanced':np.zeros(8),'AT':np.zeros(8),'AFL':np.zeros(8),'AF':np.zeros(8)}
elif N_class==2:
    acc_layouts = {'balanced_mean':np.zeros(8),'LA':np.zeros(8),'RA':np.zeros(8)}
    acc_layouts_std = {'balanced':np.zeros(8),'LA':np.zeros(8),'RA':np.zeros(8)}
    
layout_list = ['HR.mat','layout_cardioinsight.mat','layout_leicester_healthy.mat','layout_guillem.mat',
               'layout_salinet.mat','layout_uniform32.mat','layout_uniform16.mat','layout_oosterom.mat']
lead_numbers = ['567','252','128','67','64','32','16','12']

for i,layout in enumerate(layout_list):
    
    class_path = os.path.join(root,'Documents','Results', 'Models', Layouts[layout],'CBM_R1',DF_folder)  
    class_results = pd.read_csv(os.path.join(class_path,task))
    plot_matrix = confusion_matrix(class_results['labels'],class_results[method])

    for j in range(N_class):
        acc_layouts['balanced_mean'][i] += plot_matrix[j,j]/np.sum(plot_matrix[j,:])
    acc_layouts['balanced_mean'][i] = acc_layouts['balanced_mean'][i]/N_class*100
    
    if N_class==3:
        acc_layouts['AT'][i] = plot_matrix[0,0]/np.sum(plot_matrix[0,:])*100
        acc_layouts['AFL'][i] = plot_matrix[1,1]/np.sum(plot_matrix[1,:])*100
        acc_layouts['AF'][i] = plot_matrix[2,2]/np.sum(plot_matrix[2,:])*100
    elif N_class==2:
        acc_layouts['LA'][i] = plot_matrix[0,0]/np.sum(plot_matrix[0,:])*100
        acc_layouts['RA'][i] = plot_matrix[1,1]/np.sum(plot_matrix[1,:])*100        
                                        

fig,ax = plt.subplots(2,figsize = [10.5,6],gridspec_kw = {'height_ratios':[102,0]})

ax[0].plot(lead_numbers,acc_layouts['balanced_mean'],'k.--',linewidth = 3,markersize = 20,label = 'Balanced')
ax[1].plot(lead_numbers,acc_layouts['balanced_mean'],'k.--',linewidth = 3,markersize = 20)
#ax[0].fill_between(lead_numbers, acc_layouts['balanced_mean']-acc_layouts['balanced_std'],
#                acc_layouts['balanced_mean']+acc_layouts['balanced_std'],
#                color = 'gray',alpha = 0.5)

if N_class==3:
    ax[0].plot(lead_numbers,acc_layouts['AT'],'.-',linewidth = 3,markersize = 20,color = '#CA3542',label = 'AT')
    ax[0].plot(lead_numbers,acc_layouts['AFL'],'.-',linewidth = 3,markersize = 20,color = '#27647B',label = 'AFL')
    ax[0].plot(lead_numbers,acc_layouts['AF'],'.-',linewidth = 3,markersize = 20,color = '#849FAD',label = 'AF')
    
    ax[1].plot(lead_numbers,acc_layouts['AT'],'.-',linewidth = 3,markersize = 20,color = '#CA3542',label = 'AT')
    ax[1].plot(lead_numbers,acc_layouts['AFL'],'.-',linewidth = 3,markersize = 20,color = '#27647B',label = 'AFL')
    ax[1].plot(lead_numbers,acc_layouts['AF'],'.-',linewidth = 3,markersize = 20,color = '#849FAD',label = 'AF')
        
      
    ax[0].set_ylim([0,102])
    ax[1].set_ylim([0,102])
    ax[1].get_yaxis().set_visible(False)
    
elif N_class==2:
    ax[0].plot(lead_numbers,acc_layouts['LA'],'.-',linewidth = 3,markersize = 20,color = '#994F00',label = 'LA')
    ax[0].plot(lead_numbers,acc_layouts['RA'],'.-',linewidth = 3,markersize = 20,color = '#006CD1',label = 'RA')
    
    ax[1].plot(lead_numbers,acc_layouts['LA'],'.-',linewidth = 3,markersize = 20,color = '#994F00',label = 'LA')
    ax[1].plot(lead_numbers,acc_layouts['RA'],'.-',linewidth = 3,markersize = 20,color = '#006CD1',label = 'RA')
        
      
    ax[0].set_ylim([20,102])
    ax[1].set_ylim([0,25])
    ax[1].get_yaxis().set_visible(False)
    
for AX in ax:
    AX.tick_params(labelsize = 30)
    AX.spines['top'].set_visible(False);
    AX.spines['right'].set_visible(False);
    AX.spines['bottom'].set_visible(False);
    AX.spines['left'].set_linewidth(2);   

ax[1].set_xlabel('Number of leads',fontsize = 30)
ax[0].get_xaxis().set_visible(False)
#ax[0].set_ylabel('Balanced Accuracy (%)',fontsize = 30)  
fig.text(0.02,0.35,'Accuracy (%)',rotation = 90,fontsize = 30)

ax[1].spines['bottom'].set_visible(True);
ax[1].spines['left'].set_linewidth(2);
ax[1].spines['bottom'].set_linewidth(2);
fig.subplots_adjust(top=0.949,bottom=0.185,left=0.143,right=0.986,hspace=0,wspace=0.2)


plt.savefig(figure_path+'\\layout_Acc_arr_'+method+'.png',transparency =True)
#plt.savefig(figure_path+'\\layout_Acc_loc_'+method+'_.png',transparency =True)
#%% Phase movie
layout = 'HR.mat'
DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[layout],'CBM_R1',DF_folder)
geom_path = os.path.join(root,'Documents','MEGAsync',
                              'Master','BSPM Signals','models_Miguel') 

Cpts_raw = pf.fastMatRead(geom_path,0,'Cpts_raw.mat','Cpts')

filepath = os.path.join(data_path,'files_journal','Models',
                            Layouts[layout],'TA_ICV',DF_folder)# attention: This parameter must be adjusted 
xi =  pf.fastMatRead(os.path.dirname(os.path.dirname(filepath)), 0,'xi.mat', 'xi').flatten()
yi =  pf.fastMatRead(os.path.dirname(os.path.dirname(filepath)), 0,'yi.mat', 'yi').flatten()


xx,yy = np.meshgrid(xi,yi)
xx = xx.flatten()
yy = yy.flatten()    

torso_vert = pf.fastMatRead(geom_path,1,'Torso.mat','vertices','Torso')
torso_faces = pf.fastMatRead(geom_path,1,'Torso.mat','faces','Torso')-1

layout_points = pf.fastMatRead(geom_path, 0,layout,'vertices')
indexes = pf.fastMatRead(geom_path, 0,layout,'indexes')-1 # Correct matlab indexing
#
HDFs = pd.read_csv(DF_results_path+'\\DF_results.csv')['HDF_1']

## Load signals, get Phase
M = 0; basename = folder_list[M]

isig_path = os.path.join(os.path.dirname(data_path),'Data','files_journal','Models',
                            Layouts[layout],basename, 'raw_signals_CBM_R1',DF_folder)

isig_filt = pf.fastMatRead(isig_path,0,'isig_filt.mat','isig_filt')


ms_to_remove = 200
cut = int(ms_to_remove*fs_low*1e-3)

isig_dec = sig.resample(isig_filt,int(isig_filt.shape[-1]/fs*fs_low),axis = 2) #Downsampled
Phase = pf.narrowBandHilbert(isig_dec,HDFs[M],fs_low,cut = ms_to_remove)

##
Phase_plot = griddata((xx, yy), (Phase[:,:,0]).flatten(),
         (Cpts_raw[:,0],Cpts_raw[:,1]),
         method='nearest')
scalars_torso = []

for i in range(len(Phase_plot)):
    if i in indexes:
        scalars_torso.append(Phase_plot[i])
    else:
        scalars_torso.append(Phase_plot[i])
scalars_torso = np.asarray(scalars_torso)

#%%

Fig = mlab.figure(bgcolor = (1,1,1))
T = mlab.triangular_mesh(torso_vert[:,0],
                torso_vert[:,1],
                torso_vert[:,2],
                torso_faces,
                scalars = scalars_torso,
                colormap = 'jet',vmax = np.pi,vmin = -np.pi,
                opacity = 1,name  = 'Torso')
T.module_manager.scalar_lut_manager.lut.table = CM_new

text= mlab.text(0.05,0.6,'f='+str(0),color = (0,0,0))
    
@mlab.animate(delay = 50)
def anim():
    for ind in range(Phase.shape[-1]):
        Phase_plot = griddata((xx, yy), (Phase[:,:,ind]).flatten(),
                 (Cpts_raw[:,0],Cpts_raw[:,1]),
                 method='nearest')
        scalars_torso = []
        
        for i in range(len(Phase_plot)):
            if i in indexes:
                scalars_torso.append(Phase_plot[i])
            else:
                scalars_torso.append(Phase_plot[i])
        scalars_torso = np.asarray(scalars_torso)
    
        T.mlab_source.scalars = scalars_torso
        text.text = 'f='+str(ind)
        yield

anim()
#%%
for ind in np.arange(350,390,5):
#ind = 175
    Phase_plot = griddata((xx, yy), (Phase[:,:,ind]).flatten(),
             (Cpts_raw[:,0],Cpts_raw[:,1]),
             method='nearest')
    scalars_torso = []
    
    for i in range(len(Phase_plot)):
        if i in indexes:
            scalars_torso.append(Phase_plot[i])
        else:
            scalars_torso.append(Phase_plot[i])
    scalars_torso = np.asarray(scalars_torso)
    
    
    Fig = mlab.figure(bgcolor = (1,1,1))
    T = mlab.triangular_mesh(torso_vert[:,0],
                    torso_vert[:,1],
                    torso_vert[:,2],
                    torso_faces,
                    scalars = scalars_torso,
                    colormap = 'jet',vmax = np.pi,vmin = -np.pi,
                    opacity = 1,name  = 'Torso')
    T.module_manager.scalar_lut_manager.lut.table = CM_new
    
    mlab.view(-50,80,distance = 135)

#    mlab.view(-45,54,distance = 140)
#    mlab.view(145,54,distance = 140)
#    mlab.view(120,80,distance = 140)

imgmap = mlab.screenshot(figure=Fig, mode='rgba', antialiased=True)
#
plt.imsave(os.getcwd()+'\\figures\\frame_'+str(ind)+'.png',imgmap)
#plt.imsave(os.getcwd()+'\\figures\\front_df1_la.png',imgmap)


#mlab.view(120,80,distance = 135)

#%% #%% Features in downsample, single arrhyhtmia

feature = 'SPs/s'
layout_list = ['HR.mat','layout_cardioinsight.mat','layout_leicester_healthy.mat',
               'layout_guillem.mat','layout_salinet.mat','layout_uniform32.mat',
               'layout_uniform16.mat']

layout_labels = ['HR','252','128','67','64','32','16']

if feature in ['SSIM_DF','Corr_DF','SSIM_HM','Corr_HM']:
    means_plot = np.zeros(len(layout_list[1:]))
    std_plot = np.zeros(len(layout_list[1:]))
    
    for i,layout in enumerate(layout_list[1:]):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',Layouts[layout],
                               DF_folder,'SSIM')
        data = pd.read_csv(filepath+'/Comparison_HR.csv').iloc[8:,:]
        means_plot[i] = np.mean(data[feature])
        
        std_plot[i] = np.std(data[feature])
        
##   
elif feature in ['sensitivity','precision']:
    means_plot = np.zeros(len(layout_list[1:]))
    std_plot = np.zeros(len(layout_list[1:]))    
    for i,layout in enumerate(layout_list[1:]):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',Layouts[layout],
                               DF_folder,'Rotor stats')
        data = pd.read_csv(filepath+'/Sens_prec_5.csv').iloc[8:,:]
        means_plot[i] = np.mean(data[feature])
        
        std_plot[i] = np.std(data[feature])
##
elif feature in ['HDF','number of regions','avg. size','range','total area']:
    means_plot = np.zeros(len(layout_list))
    std_plot = np.zeros(len(layout_list))    
    for i,layout in enumerate(layout_list):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',Layouts[layout],
                               DF_folder)
        data = pd.read_csv(filepath+'/regions_hdf_HDF_1.csv').iloc[8:,:]
        means_plot[i] = np.mean(data[feature])
        
        if feature in ['number of regions','range','total area']:
            std_plot[i] = np.std(data[feature]) 
        else:
            std_plot[i] = np.std(data[feature])/np.sqrt(11)
  
##
elif feature in ['Mean Duration (% duration)','Mean Frequency (Hz)','Filament rate (N fil/s)']:
    means_plot = np.zeros(len(layout_list))
    std_plot = np.zeros(len(layout_list))    
    for i,layout in enumerate(layout_list):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',Layouts[layout],
                               DF_folder)
        data = pd.read_csv(filepath+'/filament_analysis.csv').iloc[8:,:]
        means_plot[i] = np.mean(data[feature])

        
        if feature=='Filament rate (N fil/s)':
            std_plot[i] = np.std(data[feature]) 
        else:
            std_plot[i] = np.std(data[feature])/np.sqrt(11)   
##
elif feature in ['Number of regions','Mean region size','Mean  %SP/area','SPs/s']:
    means_plot = np.zeros(len(layout_list))
    std_plot = np.zeros(len(layout_list))    
    for i,layout in enumerate(layout_list):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',Layouts[layout],
                               DF_folder)
        data = pd.read_csv(filepath+'/heatmap_filaments_analysis.csv').iloc[8:,:]
        means_plot[i] = np.mean(data[feature])

        
        if feature=='SPs/s':
            std_plot[i] = np.std(data[feature]) 
        else:
            std_plot[i] = np.std(data[feature])/np.sqrt(11)   
elif feature in ['HDF_1']:
    means_plot = np.zeros(len(layout_list))
    std_plot = np.zeros(len(layout_list))    
    for i,layout in enumerate(layout_list):
        filepath= os.path.join(os.path.dirname(data_path),'Results','Models',Layouts[layout],
                               Layouts[layout],DF_folder)
        data = pd.read_csv(filepath+'/DF_results.csv').iloc[8:,:]
        means_plot[i] = np.mean(data[feature])
        
        if feature=='SPs/s':
            std_plot[i] = np.std(data[feature]) 
        else:
            std_plot[i] = np.std(data[feature])/np.sqrt(11)   


centers = np.linspace(0,len(layout_list),len(layout_list))

vspancolor = ['k','gray','#404040']
boxcolors = ['#CA3542','#27647B','#849FAD']
medcolors = ['#CA3542','#27647B','#849FAD']
box_width = 0.75
spac = box_width*1.05
space_between = (centers[1]-spac-box_width/2)-(centers[0]+spac+box_width/2)

pos  = centers#[1:]#

#%%
fig,ax = plt.subplots(1,figsize = (7,4))
ax.plot(pos,means_plot,'k.-',markersize = 20)

ax.fill_between(pos,means_plot-std_plot,means_plot+std_plot,color = 'gray',alpha = 0.7)

# Error bars

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['bottom'].set_linewidth(2)
ax.spines['left'].set_linewidth(2)

ax.set_xlim([pos[0]-box_width,pos[-1]+box_width])
ax.set_xticks(pos)
ax.set_xticklabels(layout_labels[0:],fontsize = 20)


lim_y = 4
int_y = 1

ax.set_yticks(np.arange(0,lim_y+1,int_y))
ax.set_yticklabels(np.round(np.arange(0,lim_y+1,int_y),1),fontsize = 20)
ax.set_ylim(0,lim_y)
ax.set_ylabel(r'SPs per second',fontsize = 20)
fig.tight_layout()

plt.savefig(root_folder+'\\figures\\'+'SPs_s'+'.png',transparent = True)
#plt.savefig(root_folder+'\\figures\\'+feature+'.png',transparent = True)
#%% SSIM/Corr one arrhythmia

method = 'SSIM_HM'#Corr_DF, SSIM_HM, Corr_HM

plot_data = np.zeros(len(layout_list[1:]))
std_data = np.zeros(len(layout_list[1:]))

for i,L in enumerate(layout_list[1:]):
    DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[L],DF_folder,'SSIM')
    ssim_results = pd.read_csv(DF_results_path+'\Comparison_HR.csv')
    
    
    plot_data[i] = np.mean(ssim_results[method][8:]) #AF only      
 
    std_data[i] = np.std(ssim_results[method][8:]) #AF only        
    


###
boxcolors = ['k']

fig,ax = plt.subplots(1,figsize = (8,5))

ax.plot(np.linspace(0,6,6),plot_data,'.--',color = boxcolors[0],marker = '.',linewidth = 3,markersize = 25)
ax.fill_between(np.linspace(0,6,6),plot_data-std_data,
                  plot_data+std_data,
                  color = boxcolors[0],alpha = 0.3)

ax.spines['bottom'].set_visible(True);
ax.spines['bottom'].set_linewidth(2)
ax.spines['left'].set_linewidth(2)
#ax[0].set_ylabel('AT',fontsize = 20)



ax.spines['right'].set_visible(False);
ax.spines['top'].set_visible(False);
ax.tick_params(labelsize = 30)
ax.set_ylim([0,1]) #AX.set_ylim([0,201])#
ax.set_yticks([0,0.5,1])#AX.set_yticks([0,100,200])
ax.set_yticklabels([0,0.5,1])#AX.set_yticks([0,100,200])
#    AX.set_xlim([0,6)
ax.grid(1,which = 'major',axis = 'both')
ax.set_xticks(np.linspace(0,6,6))
ax.set_xticklabels(layout_labels[1:],fontsize = 30)
ax.set_xlabel('Number of leads',fontsize = 30)

ax.set_ylabel(r'SSIM$_{HM}$',fontsize = 40)#,rotation = 90)


plt.subplots_adjust(top=0.939,
bottom=0.221,
left=0.188,
right=0.981,
hspace=0.302,
wspace=0.2)

plt.savefig('figures/ssim_HM.png',transparent = True)