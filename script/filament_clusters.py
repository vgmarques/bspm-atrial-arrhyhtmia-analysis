# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 14:41:58 2019

Phase analysis figures

@author: vgmarques
"""

import os
root = os.path.join('D:\\','vgmar')# expanduser does not work in my pc with 2 HDs
library_folder = os.path.join(root,'Documents','GitHub',
                              'BSPM-analysis','Library')
os.chdir(library_folder)
import numpy as np
from matplotlib.colors import LinearSegmentedColormap
import refined_pf as pf
import scipy.io as sio
import pandas as pd
from pandas import DataFrame as df
import cv2

folder_list = np.asarray(['TA_ICV','TA_LSPV','TA_RAA','TA_RIPV',
                'Flutter_tip','Flutter_tiphor','Flutter_VCI','Flutter_VP',
                'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV',
                'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0',
                'Rotor_RIPV','AF_RotorRIPV','AF_RSPV'])
real_frequencies = np.array([4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.80,5.60,5.50,7.80,
                 7.60,5.40,5.80,5.80,5.5,5.4,6.8])

loc_list = ['IVC','LSPV','RAA','RIPV','RA (CCW)','RA (CW)','IVC','PV',
            'LA (1)','LA (2)','LIPV','LSPV','RA (1)' ,'RA(2)','RAA (1)',
            'RAA (2)', 'RIPV (1)','RIPV (2)', 'RSPV']


layout = 'layout_guillem.mat' 
Layouts = {'HR.mat':'HR',
           'layout_cardioinsight.mat':'layout_cardioinsight',
           'layout_leicester_healthy.mat':'layout_leicester_healthy',
           'layout_guillem.mat':'layout_guillem',
           'layout_salinet.mat':'layout_salinet',
           'layout_uniform32.mat':'layout_uniform32',
           'layout_uniform16.mat':'layout_uniform16',
           'layout_oosterom.mat':'layout_oosterom'}



# Paths 

data_path = os.path.join(root,'Documents','Data')
spat_filt = 0.07
DF_folder = 'DF_CBM_'+str(np.round(spat_filt*100,1))+'p'

DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[layout],'CBM_R1',DF_folder)

SP_folder = 'SP_CBM' 

# Load colormap and geometries

# Colormap
cm_path = os.path.join(root,'Documents','MEGAsync','Master','Matlab','Colormaps')#Colormap

cm = pf.fastMatRead(cm_path,0,'DFColormaps.mat','mycmap')
CM = (cm-cm.min())/(cm.max()-cm.min())*255
CM_new = np.concatenate((CM,np.ones((CM.shape[0],1))*255),axis = 1)
CM_new = np.asarray(CM_new,dtype = np.uint8)
cm = LinearSegmentedColormap.from_list('cm', cm, N=255)

# Geometries

# 2D
coord_2d_path = os.path.join(data_path,'files_journal','Models',Layouts[layout]) 

Cpts  = pf.fastMatRead(coord_2d_path, 0,'Cpts.mat','Cpts' )
xi = pf.fastMatRead(coord_2d_path, 0,'xi.mat','xi' ).flatten()
yi = pf.fastMatRead(coord_2d_path, 0,'yi.mat','yi' ).flatten()

#%%
df_cols = ['layout','subject','arrhythmia','size','density','bounding_box']
data_types= dtype=[('layout', 'U20'),('subject', 'U20'), ('arrhythmia', 'U3')
                    ,('size', 'f8'),('density','f8'),('bounding_box','f8')]

DF_heatmaps = pd.DataFrame(columns = df_cols)
DF_heatmaps_ind = pd.DataFrame(columns = df_cols)


for M,basename in enumerate(folder_list):
# Load signals and filaments

#    isig_path = os.path.join(data_path,'files_journal','Models',
#                                Layouts[layout],basename,
#                                'raw_signals_CBM_R1',DF_folder)
    sp_path = os.path.join(data_path,'files_journal','Models',
                                Layouts[layout],basename,
                                'raw_signals_CBM_R1',DF_folder,SP_folder)

    rotors = pf.fastMatRead(sp_path,0,'SP_analysis.mat','rotors')
    r,c,n = rotors.shape
    
    ## Load dicts
    SPs_filt = sio.loadmat(sp_path+'/SPs_filt.mat')
    del SPs_filt['__header__']
    del SPs_filt['__globals__']
    del SPs_filt['__version__']  
    rotors_fil = pf.dict2matrix(SPs_filt,rotors.shape)
    
    #Temporary dafaframe for this model
    
    # For each filament
    for filament in SPs_filt:
        filament = SPs_filt[filament]
        partial_rotor = pf.dict2matrix({'filament':filament},rotors.shape)
                
        ## Plot cluster (check if spatial filtering/closing is required)
        partial_HM = pf.HeatMap(-partial_rotor)
        
        ## Calculate size and density (# of filaments will be in another "feature")
        ### Binary image
        partial_HM[partial_HM!=0] = 1
        ### Density
        sp_density = np.sum(partial_rotor)/np.sum(partial_HM)       
        ### Size
        size = np.sum(partial_HM)
        
        ## Calculate spatial displacement (new idea)
        a,b = np.where(partial_HM!=0)
        
        dx = b.max()-b.min()
        dy = a.max()-a.min()
        disp = dx*dy
        ## Put stuff in Dataframe
        df_temp = np.zeros(1,dtype =data_types)

        df_temp['layout'] = str(Layouts[layout]) 
        df_temp['subject'] = str(basename)
        df_temp['size'] = size
        df_temp['density'] =  sp_density
        df_temp['bounding_box'] =  disp

        if M<4:
            df_temp['arrhythmia'] = 'AT'
        elif M<8:
            df_temp['arrhythmia'] = 'AFL'
        else:
            df_temp['arrhythmia'] = 'AF'


        df_temp =pd.DataFrame(df_temp)
        DF_heatmaps_ind = DF_heatmaps_ind.append(df_temp,ignore_index = True)
        
    #Average per subject
    df_temp_2 = np.empty(1,dtype =data_types)
    df_temp_2['layout'] = df_temp['layout']
    df_temp_2['subject'] = df_temp['subject'] 
    df_temp_2['arrhythmia'] = df_temp['arrhythmia'] 
    df_temp_2['size'] = np.median(DF_heatmaps_ind[DF_heatmaps_ind['subject']==str(basename)]['size'])
    df_temp_2['density'] = np.median(DF_heatmaps_ind[DF_heatmaps_ind['subject']==str(basename)]['density'])
    df_temp_2['bounding_box'] = np.median(DF_heatmaps_ind[DF_heatmaps_ind['subject']==str(basename)]['bounding_box'])
    df_temp_2 =pd.DataFrame(df_temp_2)

    DF_heatmaps = DF_heatmaps.append(df_temp_2,ignore_index = True)

    
    
    
        
DF_heatmaps_ind.to_csv(DF_results_path+'/individual_clusters_filaments.csv')
DF_heatmaps.to_csv(DF_results_path+'/clusters_filaments.csv')
