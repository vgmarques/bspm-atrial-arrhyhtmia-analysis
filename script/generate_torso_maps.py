#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 23:56:51 2021

@author: italo
"""
# import matplotlib.pyplot as plt
import scipy.signal as sig
import scipy.io as sio
import oct2py
oc = oct2py.Oct2Py()
import numpy as np
import os

#%%
root = os.path.join('/media', 'italo', 'HDD', 'Users','ital_')
library_folder = os.path.join('/home','italo','scripts','vgmarques-bspm-atrial-arrhyhtmia-analysis')

# os.chdir(os.path.join(library_folder,'fileIO'))
# import fileIO_lib as fio

# os.chdir(os.path.join(library_folder,'accessory'))
# import accessory_lib as acc

os.chdir(os.path.join(library_folder,'phase_analysis'))
import phaseAnalysis_lib as pha

os.chdir(os.path.join(library_folder,'preprocessing'))
import preprocessing_lib as pre

os.chdir(os.path.join(library_folder,'freq_analysis'))
import frequencyAnalysis_lib as fqa

# os.chdir(os.path.join(library_folder,'script'))
# from myFacilities import medianfilter

#%%
def get_connectivity(triangles, autoindex=True):
    """Get 1st and 2nd order connectivity matrix from triangles."""
    size = triangles.max() + 1
    connectivity = np.zeros((size, size), bool)
    for i, j, k in triangles:
        connectivity[i,j] = True
        connectivity[i,k] = True
        connectivity[j,i] = True
        connectivity[k,i] = True
        connectivity[j,k] = True
        connectivity[k,j] = True
    
    if autoindex:
        connectivity[np.arange(size), np.arange(size)] = True

    connectivity2 = connectivity.copy()
    for i in range(size):
        connectivity2[i] = connectivity[connectivity[i],:].sum(axis=0).astype(bool)
    
    if not autoindex:
        connectivity2[np.arange(size), np.arange(size)] = False
    
    return connectivity, connectivity2


#%%

# nomes = ['Ut_cheio', 
#          'Ut_fimresp', 
#          'Ut_inicioresp',
#          'Ut_repouso']

# nomes = ['Ut_contraido',
#         'Ut_fimsistole',
#         'Ut_insistole',
#         'Ut_repouso']

# nomes = ['Ut_extremo']


keys = ['vertice_0p000',
        'vertice_0p333',
        'vertice_0p667',
        'vertice_1p000']


#%%

# arch = ['Modelo_140807_AF_RA_0p25_35.mat',
#         'Modelo_141219_AF_RAA_1to0.mat',
#         'Modelo_150115_AF_RSPV.mat',
#         'Modelo_150119_AF_LIPV.mat',
#         'Modelo_150120_RotorRIPV_reord.mat',
#         'Modelo_150121_AF_RotorRIPV.mat',
#         'Modelo_150203_RotorLSPV_reord.mat',
#         'Modelo_150603_AFlutter_tipic_reord.mat',
#         'Modelo_150730_AFlutter_tipichorari_reord.mat',
#         'Modelo_150730_AFlutter_VCI_cw_reord.mat',
#         'Modelo_150730_AFlutter_VP_reord.mat',
#         'Modelo_150730_TA_ICV_reord.mat',
#         'Modelo_150730_TA_LSPV_reord.mat',
#         'Modelo_150730_TA_RAA_reord.mat',
#         'Modelo_150730_TA_RIPV_reord.mat']


#%%
# matfile = oc.load('/media/italo/HDD/SpatialNoise/Torso/breathing/breathing4.mat')
# vertices = matfile[keys[3]]
# triangles = matfile['triangles']-1

# matfile = oc.load('/media/italo/HDD/SpatialNoise/Torso/extremo/Geometria_extrema.mat')
# vertices = matfile['Torso_cheio']['vertices']
# triangles = matfile['Torso_cheio']['faces'] -1

matfile = oc.load('/media/italo/HDD/SignalsInterpolation/layouts_and_geometry/Torso.mat')
vertices = matfile['Torso']['vertices']
triangles = matfile['Torso']['faces'] -1

# direc = '/media/italo/HDD/SignalsInterpolation/Laplacian/Torso/dados'
direc = '/media/italo/HDD/SignalsInterpolation/linear_torso_direta/data'

#%%

for path, dirs, files in os.walk(direc, topdown=False):
    for filename in files:
        if filename.endswith('.mat'):
            signal_file = oc.load(os.path.join(path, filename))
            
            signal_file = dict([[key, signal_file[key]] for key in signal_file])
            
            try:
                del signal_file['size'], signal_file['shape']
            except:
                []
                
            folder = filename[:-4]
                
            for nome in signal_file:
                # mesh = o3d.geometry.TriangleMesh()
        
                # mesh.vertices = o3d.utility.Vector3dVector(vertices)
                # mesh.triangles = o3d.utility.Vector3iVector(triangles)
                
                # o3d.visualization.draw_geometries([mesh], mesh_show_wireframe=True, mesh_show_back_face=True)
                
                connectivity, connectivity2 = get_connectivity(triangles)
                
                topval = vertices[:,2].max() - 0.05*(vertices[:,2].max()-vertices[:,2].min())
                botval = vertices[:,2].min() + 0.05*(vertices[:,2].max()-vertices[:,2].min())
                
                topmask = vertices[:,2] >= topval
                topind = np.where(topmask)[0]
                nontopmask = connectivity[~topmask,:].sum(axis=0).astype(bool)
                
                botmask = vertices[:,2]<=botval
                botind = np.where(botmask)[0]
                nonbotmask = connectivity[~botmask,:].sum(axis=0).astype(bool)
                
                edgemask = nontopmask*nonbotmask
                # edgemask = connectivity[edgemask].sum(axis=0)>0
                
                edge_vertices = vertices[edgemask,:]
                
                triangmask = np.zeros(triangles.shape, bool)
                for ind in np.where(edgemask)[0]:
                    triangmask[triangles==ind] = True
                
                triangmask = np.product(triangmask, axis=1).astype(bool)
                
                edge_triangles = triangles[triangmask,:]
                
                convert = np.zeros(edge_triangles.max()+1, int)
                
                
                convert[np.unique(edge_triangles)] = np.arange(np.unique(edge_triangles).shape[0])
                
                edge_triangles = convert[edge_triangles]
                
                # edge_mesh = o3d.geometry.TriangleMesh()
                # edge_mesh.vertices = o3d.utility.Vector3dVector(edge_vertices)
                # edge_mesh.triangles = o3d.utility.Vector3iVector(edge_triangles)
                
                # o3d.visualization.draw_geometries([edge_mesh], mesh_show_wireframe=True, mesh_show_back_face=True)
                savepath = os.path.join(os.path.dirname(path), 'results', nome, folder, )
                if not os.path.isdir(savepath):
                    os.makedirs(savepath)
                    
                meas_p, R = pre.cylinderProjection(edge_vertices, 0)
                np.save(savepath + '/cp.npy', meas_p)
                np.save(savepath + '/R.npy', R)        
                i_signals, xi, yi = pre.InterpolateAndReorganize(signal_file[nome][edgemask,:], meas_p, R, 65, 32)
                
                if max(i_signals.shape) <= 1000:
                    i_signals = sig.resample(i_signals,i_signals.shape[-1]*2,axis = -1)
                
    
                    
                np.save(savepath +'/isig.npy', i_signals[1:-1,...])
                np.save(savepath + '/xi.npy', xi)
                np.save(savepath + '/yi.npy', yi[1:-1])


#%%

direc = os.path.dirname(direc) + '/results'

for path, dirs, files in os.walk(direc, topdown=False):
    for filename in files:
       
        # signal_file = sio.loadmat(os.path.join(path, signal_name))
        if filename == 'isig.npy':
            signal_name = path[len(os.path.dirname(path)):]
            if not os.path.isdir(path):
                os.makedirs(path)
                
            METHOD = 4
            fmin = 0.5
            fmax = 30
            df_range = [2,15]
            lims = [3,30]
            filter_option= 'SPATIAL'
            spat_filt = 0.07
            
            #Load Signals
            fs = 500 # Hz
            isig_raw  = np.load(os.path.join(path, 'isig.npy')) #for torso signals
            
    
            
            
            DF_wtmm, mode_wtmm, HDF_wtmm, RI_wtmm, OI_wtmm, isig_filt = fqa.completeDF_BSPM(isig_raw ,fs,fmin,fmax,
                                                                                      det_method=METHOD,
                                                                                      filter_option=filter_option, 
                                                                                      spat_filt=spat_filt,
                                                                                      band_oi=1, wav_lims=lims)
            
            np.save(os.path.join(path, 'DF_map.npy'), DF_wtmm)
            np.save(os.path.join(path, 'HDF.npy'), HDF_wtmm)
            np.save(os.path.join(path, 'OI_map.npy'), OI_wtmm)
            np.save(os.path.join(path, 'isig_filt.npy'), isig_filt)
            
        
            fs_low = 128 #Hz
            xi = np.load(os.path.join(path, 'xi.npy'))
            yi = np.load(os.path.join(path, 'yi.npy'))
            
            
            # Remove begin and end of signal and downsample
            ms_to_remove = 200
            cut = int(ms_to_remove*fs_low*1e-3)
            isig_dec = sig.resample(isig_filt, int(isig_filt.shape[-1]/fs*fs_low), axis=2) #Downsampled
            
            # Phase with Hilbert transform
            # Phase = pha.narrowBandHilbert(isig_dec, HDF_wtmm,fs_low,cut = ms_to_remove) 
            Phase = pha.narrowBandHilbert(isig_dec, -1,fs_low,cut = ms_to_remove) 

            if np.isnan(Phase).any():
                l,c,n = Phase.shape
                mask = ~np.isnan(Phase)
                A,B,_ = np.min(np.where(mask),axis = -1)
                C,D,_ = np.max(np.where(mask),axis = -1)
                Phase = Phase[A:C+1,B:D+1,:]
            
                isig_dec = isig_dec[A:C+1,B:D+1,:]
                xi_old = xi; yi_old = yi
                yi = yi[A:C+1]
                xi = xi[B:D+1]
                correct = True
            else:
                correct = False
            
            np.save(os.path.join(path, 'Phase_map.npy'), Phase)
            
            #
            # =============================================================================
            #   Phase analysis
            # =============================================================================
            #Phase progression parameters
            radii = np.array([2,4,6,8,10])
            k = 8
            ppc = 12
            feat_list = ['p_range','%_ord','big_leaps','avg_leap','std_leap']
            final_SPs = np.zeros_like(Phase)
            
            # Allocate
            HMG = np.zeros_like(Phase)
            feature_block = np.zeros((Phase.shape[0],
                                     Phase.shape[1],
                                     Phase.shape[2],
                                     len(radii),
                                     len(feat_list)+1))
            
            
            # Image analysis (edge detection, endpoints) and phase progression analysis
            for frame in range(Phase.shape[-1]):
            
                ep_dil,ep_nodil,HMG[:,:,frame] = pha.EP_Analysis(Phase[:,:,frame], th=[400,600])
                v_ind = 0
            #             print("number of endpoints detected:", len(ep_dil))
                for i,r in enumerate(radii):
                    for ep in ep_dil:
            
                        int_sigs, circle = pha.local_interpolation(isig_dec,xi,yi,
                                                                        np.copy(ep),r,ppc,k = 8)   #0:yi; 1: xi
                        # Local phase                 
                        int_sigs = int_sigs.reshape(ppc,1,int_sigs.shape[-1]) 
            
                        loc_phase = pha.narrowBandHilbert(int_sigs,HDF_wtmm,fs_low,cut = ms_to_remove)
            
                        loc_phase = loc_phase.squeeze()
            
                        # Analyze local phase progression
                        loc_phase = loc_phase[:,frame] # Get the phase of the frame of interest
            
                        p_min = np.where(loc_phase == np.min(loc_phase))[0][0]
                        phase_prog = np.append(loc_phase[p_min:],loc_phase[:p_min])
            
                        # Correct direction of rotation if it is clockwise (this is not used in the paper) 
                        phase_prog_alt = np.roll(np.flipud(phase_prog),1,0)
                        phase_diff = np.diff(phase_prog)
                        phase_diff_alt =  np.diff(phase_prog_alt)
                        if phase_diff[phase_diff>=0].shape[0]/phase_prog.shape[0]>=phase_diff_alt[phase_diff_alt>=0].shape[0]/phase_prog_alt.shape[0]:
                            sinn = -1 #CCW
                            phase_prog = phase_prog
                        else: 
                            sinn = 1 #CW
                            phase_prog = phase_prog_alt
            
                        # Get features from progression      
                        feats = pha.extractFeaturesPhase(phase_prog,feat_list = feat_list)
            
                        # Save things in feature block. 
                        # These features are including metrics correlated to the criteria for a endpoint to be considered a SP
                        feature_block[ep[0],ep[1],frame,i,0] = feats[0] # Phase range
                        feature_block[ep[0],ep[1],frame,i,1] = feats[1] # % ordered
                        feature_block[ep[0],ep[1],frame,i,2] = feats[2] # Big leaps
                        feature_block[ep[0],ep[1],frame,i,3] = feats[3] # Average leap
                        feature_block[ep[0],ep[1],frame,i,4] = feats[4] # Std. leap
                        feature_block[ep[0],ep[1],frame,i,5] = sinn # Direction of rotation
            
                #==== Evaluate phase progression based on the three criteria ====
            
                # 1st: range (at least \pi in at least 2 rings)
                temp_map_range = np.zeros((Phase.shape[0],Phase.shape[1]))
                a,b,d = np.where(feature_block[:,:,frame,:,0]>0.5)#*2*np.pi
                for el in range(len(d)): 
                    temp_map_range[a[el],b[el]] += 1
            
                temp_map_range[temp_map_range<2] = 0
                temp_map_range[temp_map_range!=0] = 1
            
            
                # 2nd: ordered (at least 60% ordered)
                temp_map_ord = np.zeros((Phase.shape[0],Phase.shape[1]))
                a,b,d = np.where(feature_block[:,:,frame,:,1]>=0.6)
                for el in range(len(d)): 
                    temp_map_ord[a[el],b[el]] += 1
            
                temp_map_ord[temp_map_ord<2] = 0
                temp_map_ord[temp_map_ord!=0] = 1
            
            
                # 3rd leaps (no big leaps)
                temp_map_leap = np.zeros((Phase.shape[0],Phase.shape[1]))
                a,b,d = np.where(feature_block[:,:,frame,:,2]==1) #
                for el in range(len(d)): 
                    temp_map_leap[a[el],b[el]] += 1   
            
                temp_map_leap[temp_map_leap<2] = 0
                temp_map_leap[temp_map_leap!=0] = 1
            
            
                final_SPs[:,:,frame] = temp_map_range*temp_map_ord*temp_map_leap
            
            
            print('partially ready.')  
            
            
            ### Part 2: Judge and extract filaments
            
            #*************************  Filament Analysis   ****************************#
            minFrames = 2 # only filaments longer than this are considered
            #    rotors_er = rdec.thin_rotors(rotors, HMG)
            
            rotors_er = final_SPs  # I use this second variable because it can be interesting to erode the SPs
            
            # find connected components within 3x3x 100 ms
            SPs = pha.connectComponents3D(rotors_er, nbhood=(3,3,int(fs_low*100e-3)), length=int(fs_low/2)) 
            pre_filt,pre_dur = pha.filterCCDict(SPs, fs_low, minFrames/fs_low*1000) 
            side = 11
            nFrames = HMG.shape[2]
            pre_freq = {}
            pre_cyc = {}
            pre_sinn = {}
            for i in pre_filt:
                pre_freq[i],pre_cyc[i],\
                pre_sinn[i] = pha.cyclesFromFilament(pre_filt[i],HMG,side,fs_low)
            
            SPs_filt = {}
            SPs_dur = []
            freq_fil = {}
            cycles_fil = {}
            sinn_fil = {}
            k = 0
            for i in pre_freq:
                if not np.isnan(pre_freq[i]):
                    SPs_filt['fil_'+str(k)] = pre_filt[i]
                    SPs_dur.append(pre_dur[i])
            
                    freq_fil[str(k)],cycles_fil[str(k)],\
                    sinn_fil[str(k)] = pha.cyclesFromFilament(SPs_filt['fil_'+str(k)],HMG,side,fs_low)
            
                    k += 1   
            
            
            #******************************* Save Files ****************************#
            sio.savemat(os.path.join(path,'SPs_filt.mat'),SPs_filt)
            sio.savemat(os.path.join(path,'SP_analysis.mat'),{'HMG':HMG,'rotors':rotors_er,
                                                              'SPs_dur':np.asarray(SPs_dur),
                                                              'feature_block':feature_block})
            print('Ready.')  
            # sio.savemat(os.path.join(path,'configs.mat'),{"DF_method":METHOD})
