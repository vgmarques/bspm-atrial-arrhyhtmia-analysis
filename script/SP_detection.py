# -*- coding: utf-8 -*-
"""
Created on Mon May 27 13:56:06 2019

    Update of the SP_analysis function. 
    Up to this point, the idea is simply to check the parameters of the phase 
progression and save the information. 

@author: vgmarques
"""


import numpy as np
import os
import sys
import pandas as pd
import scipy.signal as sig
import scipy.io as sio

# BSPM libraries
root = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(root,'fileIO'))
sys.path.append(os.path.join(root,'accessory'))
sys.path.append(os.path.join(root,'preprocessing'))
sys.path.append(os.path.join(root,'phase_analysis'))

import fileIO_lib as fio
import accessory_lib as acs
import preprocessing_lib as prp
import phaseAnalysis_lib as pha

#%%
# =============================================================================
# Load interpolated grid, xi and yi and the HDFs
# =============================================================================
folder_list = ['TA_ICV','TA_LSPV','TA_RAA','TA_RIPV',
                'Flutter_tip','Flutter_tiphor','Flutter_VCI','Flutter_VP',
                'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV',
                'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0',
                'Rotor_RIPV','AF_RotorRIPV','AF_RSPV']

real_frequencies = np.array([4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.80,5.60,5.50,7.80,
                 7.60,5.40,5.80,5.80,5.5,5.4,6.8])

layout = 'layout_uniform160.mat'
Layouts = {'layout_guillem.mat':'layout_guillem',
          'layout_salinet.mat':'layout_salinet',
          'layout_bojarnejad.mat':'layout_bojarnejad',
          'layout_cardioinsight.mat':'layout_cardioinsight',
          'layout_leicester_healthy.mat':'layout_leicester_healthy',
          'layout_uniform64.mat':'layout_uniform64',
          'layout_uniform32.mat':'layout_uniform32',
          'layout_uniform16.mat':'layout_uniform16',
          'HR.mat':'HR',
          'layout_oosterom.mat':'layout_oosterom'}
data_path = os.path.join(root,'Documents','Data')

fs = 500 # Already upsampled the Flutter_VP signal
fs_low = 128 #Hz

folder_name = 'SP_CBM' 

spat_filt = 0.07
DF_folder = 'DF_CBM_'+str(np.round(spat_filt*100,1))+'p'

# DF results
DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',Layouts[layout],
                        'CBM_R1',DF_folder) # Signals 
freq_data = pd.read_csv(DF_results_path+'\DF_results.csv')
atrial_HDF_estimate = freq_data['HDF_1']

# Layout geometry
layout_path = filepath = os.path.join(data_path,'files_journal','Models',Layouts[layout])
xi =  fio.FastMatRead(layout_path, 0,'xi.mat', 'xi').flatten()
yi =  fio.FastMatRead(layout_path, 0,'yi.mat', 'yi').flatten()
Cpts  = fio.FastMatRead(layout_path, 0,'Cpts.mat', 'Cpts')

#Phase progression parameters
radii = np.array([2,4,6,8,10])
k = 8
ppc = 12
feat_list = ['p_range','%_ord','big_leaps','avg_leap','std_leap'] 
#%% 
for M,basename in enumerate(folder_list[:5]):
    os.chdir(root)
# =============================================================================
#   Load signals
# =============================================================================  
    load_path = os.path.join(data_path,'files_journal','Models',
                                Layouts[layout],basename,
                                'raw_signals_CBM_R1',DF_folder) # Signals
    
    isig_filt  = fio.FastMatRead(load_path, 0,'isig_filt.mat', 'isig_filt')
    
# =============================================================================
#   Preprocessing
# =============================================================================    
    # Remove begin and end of signal and downsample
    ms_to_remove = 200
    cut = int(ms_to_remove*fs_low*1e-3)
    isig_dec = sig.resample(isig_filt,int(isig_filt.shape[-1]/fs*fs_low),axis = 2) #Downsampled
    
    # Phase with Hilbert transform
    Phase = pha.narrowBandHilbert(isig_dec,atrial_HDF_estimate[M],fs_low,cut = ms_to_remove) 
    if np.isnan(Phase).any():
        l,c,n = Phase.shape
        mask = ~np.isnan(Phase)
        A,B,_ = np.min(np.where(mask),axis = -1)
        C,D,_ = np.max(np.where(mask),axis = -1)
        Phase = Phase[A:C+1,B:D+1,:]
        
        isig_dec = isig_dec[A:C+1,B:D+1,:]
        xi_old = xi; yi_old = yi
        yi = yi[A:C+1]
        xi = xi[B:D+1]
        correct = True
    else:
        correct = False
    final_SPs = np.zeros_like(Phase)
    
# =============================================================================
#   Phase analysis
# =============================================================================

    # Allocate
    HMG= np.zeros_like(Phase)
    feature_block = np.zeros((Phase.shape[0],
                         Phase.shape[1],
                         Phase.shape[2],
                         len(radii),
                         len(feat_list)+1))

    # Image analysis (edge detection, endpoints) and phase progression analysis
    for frame in range(Phase.shape[-1]):
        
        ep_dil,ep_nodil,HMG[:,:,frame] = pha.EP_Analysis(Phase[:,:,frame])
        v_ind = 0
        
        for i,r in enumerate(radii):
            for ep in ep_dil:
                
                int_sigs, circle = pha.local_interpolation(isig_dec,xi,yi,
                                                                np.copy(ep),r,ppc,k = 8)   #0:yi; 1: xi
                # Local phase                 
                int_sigs = int_sigs.reshape(ppc,1,int_sigs.shape[-1]) 
                
                loc_phase = pha.narrowBandHilbert(int_sigs,atrial_HDF_estimate[M],fs_low,cut = ms_to_remove)
                
                loc_phase = loc_phase.squeeze()
                
                # Analyze local phase progression
                loc_phase = loc_phase[:,frame] # Get the phase of the frame of interest
                                
                p_min = np.where(loc_phase == np.min(loc_phase))[0][0]
                phase_prog = np.append(loc_phase[p_min:],loc_phase[:p_min])
                                
                # Correct direction of rotation if it is clockwise (this is not used in the paper) 
                phase_prog_alt = np.roll(np.flipud(phase_prog),1,0)
                phase_diff = np.diff(phase_prog)
                phase_diff_alt =  np.diff(phase_prog_alt)
                if phase_diff[phase_diff>=0].shape[0]/phase_prog.shape[0]>=phase_diff_alt[phase_diff_alt>=0].shape[0]/phase_prog_alt.shape[0]:
                   sinn = -1 #CCW
                   phase_prog = phase_prog
                else: 
                    sinn = 1 #CW
                    phase_prog = phase_prog_alt

                # Get features from progression      
                feats = pha.extractFeaturesPhase(phase_prog,feat_list = feat_list)
                    
                # Save things in feature block
                feature_block[ep[0],ep[1],frame,i,0] = feats[0]# Phase range
                feature_block[ep[0],ep[1],frame,i,1] = feats[1] # % ordered
                feature_block[ep[0],ep[1],frame,i,2] = feats[2] # Big leaps
                feature_block[ep[0],ep[1],frame,i,3] = feats[3] # Average leap
                feature_block[ep[0],ep[1],frame,i,4] = feats[4] # Std. leap
                feature_block[ep[0],ep[1],frame,i,5] = sinn # Direction of rotation
                
        # Evaluate phase progression based on the three criteria
        
        # range (at least \pi)
        temp_map_1 = np.zeros((Phase.shape[0],Phase.shape[1]))
        temp_map_2 = np.ones((Phase.shape[0],Phase.shape[1]))
        
        a,b,d = np.where(feature_block[:,:,frame,:,0]>0.5)#*2*np.pi
        
        for el in range(len(d)): 
            temp_map_1[a[el],b[el]] += 1
         
        temp_map_1[temp_map_1<2] = 0
        temp_map_1[temp_map_1!=0] = 1
        temp_map_2 *= temp_map_1
        
        # order (at least 60% ordered)
        temp_map_1 = np.zeros((Phase.shape[0],Phase.shape[1]))
        a,b,d = np.where(feature_block[:,:,frame,:,1]>=0.6)
        for el in range(len(d)): 
            temp_map_1[a[el],b[el]] += 1
         
        temp_map_1[temp_map_1<2] = 0
        temp_map_1[temp_map_1!=0] = 1
        temp_map_2 *= temp_map_1
        
        # leaps (no big leaps)
        temp_map_1 = np.zeros((Phase.shape[0],Phase.shape[1]))
        a,b,d = np.where(feature_block[:,:,frame,:,2]==1) #
        for el in range(len(d)): 
            temp_map_1[a[el],b[el]] += 1   
        
        # clean up and save in dict
        temp_map_1[temp_map_1<2] = 0
        temp_map_1[temp_map_1!=0] = 1
        
        temp_map_2 *= temp_map_1
        final_SPs[:,:,frame] = temp_map_2
        

    print(basename +' is partially ready.')  

### Part 2: Judge and extract filaments
    
#*************************  Filament Analysis   ****************************#
    minFrames = 2 # only filaments longer than this are considered
#    rotors_er = rdec.thin_rotors(rotors, HMG)
    
    rotors_er = final_SPs  # I use this second variable because it can be interesting to erode the SPs
    
    # find connected components within 3x3x 100 ms
    SPs = pha.connectComponents3D(rotors_er,nbhood = (3,3,int(fs_low*100e-3)),length =int(fs_low/2)) 
    pre_filt,pre_dur = pha.filterCCDict(SPs, fs_low,minFrames/fs_low*1000) 
    side = 11
    nFrames = HMG.shape[2]
    pre_freq = {}
    pre_cyc = {}
    pre_sinn = {}
    for i in pre_filt:
        pre_freq[i],pre_cyc[i],\
        pre_sinn[i] = pha.cyclesFromFilament(pre_filt[i],HMG,side,fs_low)
        
    SPs_filt = {}
    SPs_dur = []
    freq_fil = {}
    cycles_fil = {}
    sinn_fil = {}
    k = 0
    for i in pre_freq:
        if not np.isnan(pre_freq[i]):
            SPs_filt['fil_'+str(k)] = pre_filt[i]
            SPs_dur.append(pre_dur[i])
            
            freq_fil[str(k)],cycles_fil[str(k)],\
            sinn_fil[str(k)] = pha.cyclesFromFilament(SPs_filt['fil_'+str(k)],HMG,side,fs_low)
            
            k += 1   
    #******************************* Save Files ****************************#
    save_path = os.path.join(data_path,'files_journal','Models',
                                Layouts[layout],basename,
                                'raw_signals_CBM_R1',DF_folder,folder_name)
        
    if not os.path.exists(save_path):
        os.makedirs(save_path)    
    os.chdir(save_path)
    
    sio.savemat('SPs_filt.mat',SPs_filt)
    sio.savemat('SP_analysis.mat',{'HMG':HMG,'rotors':rotors_er,
                                   'SPs_dur':np.asarray(SPs_dur),
                                   'feature_block':feature_block})
    print(basename +' is ready.')  
print('\a')#Noise