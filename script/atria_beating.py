#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 15:43:28 2021

@author: italo
"""


import numpy as np
import scipy.io as sio
import trimesh as trm
import oct2py
oc = oct2py.Oct2Py()


#%%
def get_connectivity(triangles, autoindex=True):
    """Get 1st and 2nd order connectivity matrix from triangles."""
    size = triangles.max() + 1
    connectivity = np.zeros((size, size), bool)
    for i, j, k in triangles:
        connectivity[i,j] = True
        connectivity[i,k] = True
        connectivity[j,i] = True
        connectivity[k,i] = True
        connectivity[j,k] = True
        connectivity[k,j] = True
    
    if autoindex:
        connectivity[np.arange(size), np.arange(size)] = True

    connectivity2 = connectivity.copy()
    for i in range(size):
        connectivity2[i] = connectivity[connectivity[i],:].sum(axis=0).astype(bool)
    
    if not autoindex:
        connectivity2[np.arange(size), np.arange(size)] = False
    
    return connectivity, connectivity2

#%%
class coef_function():
    def __init__(self, *args, smth=0.05):
        self.ncoef = np.asarray(args)[0::2]
        self.ths = np.asarray(args)[1::2]
        self.ths.sort()
        self.smth = smth
    
    def slope(self, x, x0, y0, x1, y1):
        a = (y1-y0)/(x1-x0)
        b = y0 - x0*a
        return x*a + b
    
    def eval(self, n):
        if n < self.ths[0] - self.smth:
            return self.ncoef[0]
        elif n > self.ths[-1] + self.smth:
            return self.ncoef[-1]
        elif n > self.ths[-1] - self.smth:
            return self.slope(n, self.ths[-1]-self.smth, self.ncoef[-2], self.ths[-1]+self.smth, self.ncoef[-1])
        else:
            for i in range(self.ths.shape[0]-1):
                if n < self.ths[i] + self.smth:
                    return self.slope(n, self.ths[i]-self.smth, self.ncoef[i], self.ths[i]+self.smth, self.ncoef[i+1])
                elif n < self.ths[i+1] - self.smth:
                    return self.ncoef[i+1]
                else:
                    continue

#%%
matfile = oc.load("/media/italo/HDD/SpatialNoise/Atrios/atrial_model.mat")
atria_model = matfile['atrial_model']
triangles = atria_model["faces"]-1
vertex = atria_model["vertices"]

conn1, conn2 = get_connectivity(triangles,)
#%%

mesh = trm.Trimesh()
mesh.vertices = vertex
mesh.faces = triangles

#%%
rel_vertices = mesh.vertices - mesh.center_mass
rel_vertices /= np.linalg.norm(rel_vertices, axis=1, keepdims=True)

#%%
smth_normals = np.copy(mesh.vertex_normals)
for _ in range(4):
    smth_normals0 = smth_normals.copy()
    for i in range(smth_normals.shape[0]):
        smth_normals[i,:] = np.mean(smth_normals0[conn1[i]], axis=0)

smth_normals[:,2] = 0
#%%
## Contraction (P wave)

alpha = 0.5
beta = 0.5
# mesh1 = trm.Trimesh(vertices=vertex - alpha*rel_vertices - beta*mesh.vertex_normals , faces=triangles)
disloc = - alpha*rel_vertices - beta*smth_normals
disloc -= disloc * np.array([[np.sqrt(2), 0, -np.sqrt(2)]])

mesh1 = trm.Trimesh()
vertex1 = vertex + disloc
mesh1.vertices = vertex1
mesh1.faces = triangles

#%%
## Sistole

max_dz = 2.5

vertex2 = vertex.copy()
vertex2[:,2] += - np.sqrt(max_dz) * (1-(vertex2[:,2]-vertex2[:,2].min())/(vertex2[:,2].max() - vertex2[:,2].min()))
vertex2[:,0] += np.sqrt(max_dz) * ((vertex2[:,0]-vertex2[:,0].min())/(vertex2[:,0].max() - vertex2[:,0].min()))
mesh2 = trm.Trimesh()
mesh2.vertices = vertex2
mesh2.faces = triangles



#%%
meshind = np.zeros(15)
meshind[:5] = np.linspace(1,0,5)
meshind[-5:] = np.linspace(0,1,5)

mesh1ind = np.zeros(15)
mesh1ind[:5] = np.linspace(0,1,5)
mesh1ind[5:10] = np.linspace(1,0,5)

mesh2ind = np.zeros(15)
mesh2ind[5:10] = np.linspace(0,1,5)
mesh2ind[10:15] = np.linspace(1,0,5)

#%%
cam = dict(pos=(27.22, -6.980, 55.89),
           focalPoint=(-2.746, 7.932, 34.53),
           viewup=(-0.4889, 0.2248, 0.8428),
           distance=39.71,
           clippingRange=(20.62, 63.82)) 

#%%

mat15 = {'__globals__':[],
             '__header__':b'MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: Wed Oct 13 17:22:04 2021',
             '__version__':'1.0',
             'faces':triangles+1}

for i in range(15):
    meshi = trm.Trimesh()
    meshi.faces = triangles
    mat15['vertices_' + str(i) + '_de_15'] = vertex*meshind[i] + vertex1*mesh1ind[i] + vertex2*mesh2ind[i]
    
    meshi.vertices = vertex*meshind[i] + vertex1*mesh1ind[i] + vertex2*mesh2ind[i]

    # scene = vedo.Plotter(interactive=True)
    # # scene += vedo.Mesh(mesh,).wireframe().lw(2).lc('black')
    # scene += vedo.Mesh(meshi, c='R').lw(2).lc('black')
    # # scene += vedo.Mesh(mesh2, c='B').lw(2).lc('black')
    # # scene += vedo.Arrows(mesh.vertices, mesh.vertices + rel_vertices)
    
    # scene.show(camera=cam).screenshot("frame" + str(i) + ".png").close()
    
    
sio.savemat("./atria_beating15.mat", mat15)

#%%

matoutput = {'__globals__':[],
             '__header__':b'MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: Wed Oct 13 17:22:04 2021',
             '__version__':'1.0',
             'vertices_0_repouso': vertex,
             'vertices_1_contraido':vertex1,
             'vertices_2_inicio_sistole':vertex1*0.5+vertex2*0.5,
             'vertices_3_fim_sistole':vertex2,
             'faces':triangles+1}

sio.savemat("./atria_beating.mat", matoutput)



