# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 10:44:18 2018

Analysis of HDF regions

@author: vgmar
"""
import os
root = os.path.join('D:\\','vgmar')# expanduser does not work in my pc with 2 HDs
library_folder = os.path.join(root,'Documents','GitHub',
                              'BSPM-analysis','Library')
os.chdir(library_folder)
import numpy as np
import refined_pf as pf
import pandas as pd
import cv2
from scipy.interpolate import griddata

## Import raw signals, interpolated
folder_list = ['TA_ICV','TA_LSPV','TA_RAA','TA_RIPV',
                'Flutter_tip','Flutter_tiphor','Flutter_VCI','Flutter_VP',
                'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV',
                'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0',
                'Rotor_RIPV','AF_RotorRIPV','AF_RSPV']

# Atualizado em 15/10/2018
real = np.array([4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.80,5.60,5.50,7.80,
                 7.60,5.40,5.80,5.80,5.5,5.4,6.8])

layout_list = [#'HR.mat'
               'layout_cardioinsight.mat','layout_leicester_healthy.mat',
               'layout_guillem.mat','layout_salinet.mat',
               'layout_uniform32.mat','layout_uniform16.mat','layout_oosterom.mat']

Layouts = {'HR.mat':'HR',
           'layout_cardioinsight.mat':'layout_cardioinsight',
           'layout_leicester_healthy.mat':'layout_leicester_healthy',
           'layout_guillem.mat':'layout_guillem',
           'layout_salinet.mat':'layout_salinet',
           'layout_uniform32.mat':'layout_uniform32',
           'layout_uniform16.mat':'layout_uniform16',
           'layout_uniform64.mat':'layout_uniform64',
           'layout_bojarnejad.mat':'layout_bojarnejad',
           'layout_oosterom.mat':'layout_oosterom'}

data_path = os.path.join(root,'Documents','Data')


spat_filt = 0.07 # filter used in the analyses
DF_folder = 'DF_CBM_'+str(np.round(spat_filt*100,1))+'p'

layout_path = os.path.join(data_path,'files_journal','Models',Layouts[layout_list[0]],'TA_ICV','raw_signals_CBM_R1')
xi = pf.fastMatRead(layout_path, 0,'xi.mat','xi').flatten()
yi = pf.fastMatRead(layout_path, 0,'yi.mat','yi').flatten()


Pos_HDF = {}
Perc_HDF = {}
Pos_LDF = {}
Perc_LDF = {}
LDF = {}
DF_range = {}

col_names = ['HDF','number of regions' ,'avg. size','std. size','range','total area']#,

pd_table = np.zeros(shape = (len(folder_list),len(col_names))) 
oi_hdf = np.zeros((len(folder_list),4))

df_cols = ['layout','subject','arrhythmia','model_freq','f_drive','areas']#,'oi_in','oi_out']
data_types= dtype=[('subject', 'U30'),('layout', 'U20'), ('arrhythmia', 'U3'), 
                   ('model_freq', 'f8'),('f_drive', 'f8'),
                   ('areas','f8')]

# for all maps
la_list = np.asarray(['TA_LSPV','TA_RIPV',
           'Flutter_VP',
           'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV','Rotor_RIPV',
           'AF_RotorRIPV','AF_RSPV'])

ra_list = np.asarray(['TA_ICV','TA_RAA',
           'Flutter_tip','Flutter_tiphor','Flutter_VCI',
           'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0'])

#%%
for layout in layout_list:
    DF_regions = pd.DataFrame(columns = df_cols)

    pd_table = np.zeros(shape = (len(folder_list),len(col_names))) 

    DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[layout],'CBM_R1',DF_folder)
    
    freq_data = pd.read_csv(DF_results_path+'\DF_results.csv')
    model_freq = freq_data['HDF_1'] # Method

    
    df_map_ra = np.zeros((len(yi),len(xi),len(ra_list)))
    df_map_la = np.zeros((len(yi),len(xi),len(la_list)))
    ra_ind = 0;la_ind = 0
    for M, basename in enumerate(folder_list):
    
        df_path = os.path.join(data_path,'files_journal','Models',
                                Layouts[layout],basename,
                                'raw_signals_CBM_R1',DF_folder) # Signals
            
        DF = pf.fastMatRead(df_path, 0,'Maps.mat','DF_1')
        r,c = DF.shape
        
        # HDF
        pd_table[M,0] = model_freq[M]
        DF_thresh = np.copy(DF)
        
        # Regions limited by +- 1 Hz
        DF_thresh[DF<(model_freq[M]-1)] = 0
        DF_thresh[DF>(model_freq[M]+1)] = 0
        DF_thresh[DF_thresh!=0] = 1
        
        # Total percentage
        pd_table[M,5] = np.sum(DF_thresh!=0)/(r*c)*100
        
        
        margin = c
        DF_thresh = pf.wrapMap(DF_thresh,margin)[margin:-margin,:]
        DF_thresh = np.asarray(DF_thresh,dtype = np.uint8)
        
        a,b = np.where(DF_thresh[:,margin:margin+c]==1)
        e,f = np.where(DF_thresh[:,margin:margin+c]==0)   
        # Connected components
        Ncc,labels,stats,centroids = cv2.connectedComponentsWithStats(DF_thresh,connectivity = 8)
    
        
        # Number of regions
        labs_hdf = np.unique(labels[:,margin:margin+c][a,b])   #unique labels   
        
        # Re-label to remain with only labs_hdf
        for i in labs_hdf:
            lab_area = stats[i,-1]
            if lab_area>1:
                copies = np.where(stats[:,-1]==lab_area)[0]
            else:
                copies = []        
            for C in copies:
                ind_x,ind_y = np.where(labels==C)
                labels[ind_x,ind_y] = i
        
        # get only central portion of labels
        labels = labels[:,margin:margin+c]
        
        # get areas
        un_areas = np.asarray([len(np.where(labels==i)[0]) for i in labs_hdf])

        # Unique labels
        pd_table[M,1] = len(un_areas)
        
        # Area sizes
        sz_areas = np.sort(un_areas)
        
        pd_table[M,2] = np.nanmean(sz_areas/(r*c)*100)
        pd_table[M,3] = np.nanstd(sz_areas/(r*c)*100)

        # gradient
        pd_table[M,4] = model_freq[M]-np.nanmin(DF)
        
        # Every region
        df_temp = np.zeros(len(un_areas),dtype=data_types)
        
        df_temp['layout'] = Layouts[layout]
        df_temp['subject'] = basename
        
        if M<4:
            df_temp['arrhythmia'] = 'AT'
        elif M<8:
            df_temp['arrhythmia'] = 'AFL'
        else:
            df_temp['arrhythmia'] = 'AF'
            
        df_temp['model_freq'] = real[M]
        df_temp['f_drive'] = model_freq[M]
        df_temp['areas'] = sz_areas/(r*c)*100

        
        df_temp = pd.DataFrame(df_temp,columns = df_cols)    
        DF_regions = DF_regions.append(df_temp,ignore_index = True)
        
        ## For the maps
        labels[labels!=0] = 1
        if basename in ra_list:
            df_map_ra[:,:,ra_ind] = labels
            ra_ind += 1
        elif basename in la_list:
            df_map_la[:,:,la_ind] = labels
            la_ind += 1
    # =============================================================================
    
    regions_hdf = pd.DataFrame(pd_table,index = folder_list,columns = col_names)
    
    
    regions_hdf.to_csv(DF_results_path+'\\regions_hdf.csv')
    DF_regions.to_csv(DF_results_path+'\\all_fdrive_regions.csv') 