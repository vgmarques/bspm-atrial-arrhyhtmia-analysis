#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 22:27:06 2021

@author: italo
"""


import scipy.io as sio
import pandas as pd
import numpy as np
import cv2
import os


#%%
library_folder = os.path.join('/home','italo','scripts','vgmarques-bspm-atrial-arrhyhtmia-analysis')

os.chdir(os.path.join(library_folder,'fileIO'))
import fileIO_lib as fio

os.chdir(os.path.join(library_folder,'accessory'))
import accessory_lib as acc

os.chdir(os.path.join(library_folder,'phase_analysis'))
import phaseAnalysis_lib as pha

os.chdir(os.path.join(library_folder,'preprocessing'))
import preprocessing_lib as pre

# os.chdir(os.path.join(library_folder,'freq_analysis'))
# import frequencyAnalysis_lib as fqa

# os.chdir(os.path.join(library_folder,'script'))
# from myFacilities import medianfilter
#%%
spat_filt = 0.07 # filter used in the analyses
DF_folder = 'DF_CBM_'+str(np.round(spat_filt*100,1)) +'p'
# motherdir = '/media/italo/HDD/SpatialNoise/Torso/extremo/results/'
# motherdir = '/media/italo/HDD/SignalsInterpolation/Laplacian/Torso/results'
motherdir = '/media/italo/HDD/SignalsInterpolation/linear_torso_direta/results'

col_names = ['mDF/HDF',
             'N. of HDF regions',
             'avg. size',
             'std. size',
             'total area',
             'IQR_DF', 
             'OI_mean',
             'IQR_OI', 
             'Mean Duration (%)',
             'Filament Rate', 
             'N. of HM regions',
             'Size',
             'Density',
             'Bounding Box',
             'Fil. Displacement mean',
             'path']

dtypes = [(name, 'f8') for name in col_names[:-1]] + [('path','U64')]

final_df = pd.DataFrame(columns = col_names)
#%%
idx = 0
for data_path, dirs, files in os.walk(motherdir, topdown=False):
    print(data_path)
    if 'isig.npy' in files:        
        xi =  np.load(os.path.join(data_path, 'xi.npy')).flatten()
        yi =  np.load(os.path.join(data_path, 'yi.npy')).flatten()
        
        Pos_HDF = {}
        Perc_HDF = {}
        Pos_LDF = {}
        Perc_LDF = {}
        LDF = {}
        DF_range = {}
        
        
        
        oi_hdf = np.zeros((1,4))
        
        
        pd_table = np.zeros(shape = 1, dtype=dtypes) 
        pd_table[0][-1] = data_path[len(motherdir):]
        
        model_freq = np.load(os.path.join(data_path, 'HDF.npy')) # Method
        
        
        df_map = np.zeros((len(yi),len(xi),1))
        ra_ind = 0;la_ind = 0
        
        df_path = os.path.join(data_path, 'DF_map.npy') # Signals
        
        DF = np.load(df_path)
        r,c = DF.shape
        
        # HDF
        pd_table[0][0] = DF.mean()/model_freq
        DF_thresh = np.copy(DF)
        
        # Regions limited by +- 1 Hz
        DF_thresh[DF<(model_freq-1)] = 0
        DF_thresh[DF>(model_freq+1)] = 0
        DF_thresh[DF_thresh!=0] = 1
        
        pd_table[0][4] = np.sum(DF_thresh!=0)/(r*c)*100 # Total Area (Percentage)
        
        
        margin = c
        DF_thresh = pre.wrapMap(DF_thresh,margin)[margin:-margin,:]
        DF_thresh = np.asarray(DF_thresh,dtype = np.uint8)
        
        a,b = np.where(DF_thresh[:,margin:margin+c]==1)
        e,f = np.where(DF_thresh[:,margin:margin+c]==0)   
        # Connected components
        Ncc,labels,stats,centroids = cv2.connectedComponentsWithStats(DF_thresh,connectivity = 8)
        
        # Number of regions
        labs_hdf = np.unique(labels[:,margin:margin+c][a,b])   #unique labels   
        
        # Re-label to remain with only labs_hdf
        for i in labs_hdf:
            lab_area = stats[i,-1]
            if lab_area>1:
                copies = np.where(stats[:,-1]==lab_area)[0]
            else:
                copies = []        
            for C in copies:
                ind_x,ind_y = np.where(labels==C)
                labels[ind_x,ind_y] = i
        
        # get only central portion of labels
        labels = labels[:,margin:margin+c]
        
        # get areas
        un_areas = np.asarray([len(np.where(labels==i)[0]) for i in labs_hdf])
        
        # Unique labels
        pd_table[0][1] = len(un_areas)
        
        # Area sizes
        sz_areas = np.sort(un_areas)
        
        pd_table[0][2] = np.nanmean(sz_areas/(r*c)*100)
        pd_table[0][3] = np.nanstd(sz_areas/(r*c)*100)
        
        pd_table[0][5] = abs(np.percentile(DF,75) - np.percentile(DF,25))
        
        OI_map = np.load(os.path.join(data_path, 'OI_map.npy'))
        
        pd_table[0][6] = OI_map.mean() 
        pd_table[0][7] = abs(np.percentile(OI_map,75) - np.percentile(OI_map,25))
        

        
        fs_low = 128
        
        Fils_information = np.zeros(shape = (1,len(col_names)))
        
        
        os.chdir(library_folder)
        
        load_path = os.path.join(data_path) # Signals
        
        rotors =  fio.FastMatRead(load_path, 0, 'SP_analysis.mat','rotors')
        HMG =  fio.FastMatRead(load_path, 0, 'SP_analysis.mat','HMG') 
        SPs_dur =  fio.FastMatRead(load_path, 0, 'SP_analysis.mat','SPs_dur')
        
        # Load dicts
        SPs_filt = sio.loadmat(load_path + '/SPs_filt.mat')
        del SPs_filt['__header__']
        del SPs_filt['__globals__']
        del SPs_filt['__version__']
        
        # Get overall duration
        over_dur = rotors.shape[-1]/fs_low
        
        #### Duration
        try:
            pd_table[0][8] = np.mean(SPs_dur/1000/over_dur)*100
            # Get "appearance rate"
            pd_table[0][9] = len(SPs_filt)/over_dur
        
        except:
            print('warning, over_dur=', over_dur)
            pd_table[0][8] = np.nan
            pd_table[0][9] = np.nan
        
        
        L_masks = np.zeros(shape = (len(yi), len(xi), 1))
        
        oi_sps = np.zeros((1,4))                  
        
        r,c,n = rotors.shape
        
        
        rotors_fil = acc.dict2matrix(SPs_filt, rotors.shape)
        
        hml = pha.HeatMap(-rotors_fil)
        
        
        hml_b = pre.wrapMap(hml,c)[c:-c,:]
        hml_b[hml_b!=0] = 255; 
        
        hml_b = np.asarray(hml_b,np.uint8)
        # Connected components
        Ncc, mask_L, stats, centroids = cv2.connectedComponentsWithStats(hml_b,
                                                                        connectivity = 8)
        
        mask = np.zeros_like(mask_L[:,c:-c])
        
        for i in np.unique(mask_L):
            a,b = np.where(mask_L ==i)
            b[b<=c] += c
            b[b>=2*c] -= c
            b[:] -= c
            mask[a,b] = i
        
        cc_labels = np.unique(mask) # I'm a bit confused now (12/11) as to why this works. but it does
        
        stats = stats[cc_labels,:]
        centroids = centroids[cc_labels,:]
        
        
        ###########################
        # SPs per region
        sp_r = np.zeros(len(cc_labels)-1)
        for i, L in enumerate(cc_labels[1:]):
            temp_mask = np.copy(mask)
            temp_mask[mask!=L] = 0
            temp_mask[mask == L] = 1
            sp_r[i] = np.nansum(temp_mask*hml)
            if stats[i,-1]==np.nanmax(stats[:,-1]):
                L_masks[:,:,0] = temp_mask
        sp_r = sp_r/np.nansum(sp_r)*100
        
        ##########################   
        #    ax.boxplot(sp_r*100,whis = [5,95],positions = [M])
        
        pd_table[0][10] = len(sp_r) # number of regions
        
        
        df_cols = ['layout','subject','arrhythmia','size','density','bounding_box']
        data_types= dtype=[('layout', 'U20'),('subject', 'U50'), ('arrhythmia', 'U3')
                            ,('size', 'f8'),('density','f8'),('bounding_box','f8')]
        
        DF_heatmaps = pd.DataFrame(columns = df_cols)
        DF_heatmaps_ind = pd.DataFrame(columns = df_cols)
        
        DF_heatmaps = pd.DataFrame(columns = df_cols)
        DF_heatmaps_ind = pd.DataFrame(columns = df_cols)
        # Load signals and filaments
        
        #Temporary dafaframe for this model
        
        # For each filament
        sizes = []
        densities = []
        bbs = []
        for filament in SPs_filt:
            filament = SPs_filt[filament]
            partial_rotor = acc.dict2matrix({'filament':filament},[r,c,n]) # <-------------
        
            ## Plot cluster (check if spatial filtering/closing is required)
            partial_HM = pha.HeatMap(-partial_rotor)
        
            ## Calculate size and density (# of filaments will be in another "feature")
            ### Binary image
            partial_HM[partial_HM!=0] = 1
            ### Density
            sp_density = np.sum(partial_rotor)/np.sum(partial_HM)       
            ### Size
            size = np.sum(partial_HM)
        
            ## Calculate spatial displacement (new idea)
            a,b = np.where(partial_HM!=0)
        
            if not 0 in a.shape:     # <-------------
                dy = a.max()-a.min()
            else:
                dy = 0
        
            if not 0 in b.shape:
                dx = b.max()-b.min()
            else:
                dx = 0
        
            disp = dx*dy
            ## Put stuff in Dataframe
            df_temp = np.zeros(1, dtype=data_types)
        
            sizes.append(size)
            densities.append(sp_density)
            bbs.append(disp)
        
        
        pd_table[0][11] = np.mean(sizes)
        pd_table[0][12] = np.mean(densities)
        pd_table[0][13] = np.mean(bbs)
        
        pd_table[0][14], _  = pha.fil_displacement(SPs_filt ,xi,yi)
        
        
        data_frame = pd.DataFrame(pd_table,columns = col_names, index=[idx])#,index = folder_list)
        idx += 1
        final_df = final_df.append(data_frame)

final_df.to_csv(data_path + '/biomarkers.csv', sep = ";", decimal=".")