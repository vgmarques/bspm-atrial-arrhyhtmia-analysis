#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 15 16:14:57 2021

@author: italo
"""


import numpy as np
import scipy.io as sio
# import trimesh as trm
import vedo

#%%
matfile = sio.loadmat('/media/italo/HDD/Solução direta e inversa/Camila/Paciente1/Potencial_FD_EGM_Paciente1.mat')
atria_vertices = matfile['HR_geometry_Atrio']['vertices'][0,0]
atria_faces= matfile['HR_geometry_Atrio']['faces'][0,0] -1

torso_vertices = matfile['HR_geometry_Torso']['vertices'][0,0]
torso_faces = matfile['HR_geometry_Torso']['faces'][0,0] -1

#%%
max_x, max_y, max_z = torso_vertices.max(axis=0)
min_x, min_y, min_z = torso_vertices.min(axis=0)
mx, my, mz = (torso_vertices.max(axis=0) + torso_vertices.min(axis=0))/2


#%%
class coef_function():
    def __init__(self, *args, smth=0.05):
        self.ncoef = np.asarray(args)[0::2]
        self.ths = np.asarray(args)[1::2]
        self.ths.sort()
        self.smth = smth
    
    def slope(self, x, x0, y0, x1, y1):
        a = (y1-y0)/(x1-x0)
        b = y0 - x0*a
        return x*a + b
    
    def eval(self, n):
        if n < self.ths[0] - self.smth:
            return self.ncoef[0]
        elif n > self.ths[-1] + self.smth:
            return self.ncoef[-1]
        elif n > self.ths[-1] - self.smth:
            return self.slope(n, self.ths[-1]-self.smth, self.ncoef[-2], self.ths[-1]+self.smth, self.ncoef[-1])
        else:
            for i in range(self.ths.shape[0]-1):
                if n < self.ths[i] + self.smth:
                    return self.slope(n, self.ths[i]-self.smth, self.ncoef[i], self.ths[i]+self.smth, self.ncoef[i+1])
                elif n < self.ths[i+1] - self.smth:
                    return self.ncoef[i+1]
                else:
                    continue
                
#%%

waist_coef = 0
waist_belly_th = 0.05
belly_coef = 1.25
belly_chest_th = 0.55
chest_coef = 0.75
shouder_chest_th = 0.95
shouder_coef = 0
zsmth = 0.15
z_func = coef_function(waist_coef,
                       waist_belly_th, 
                       belly_coef,
                       belly_chest_th,
                       chest_coef,
                       shouder_chest_th,
                       shouder_coef,
                       smth = zsmth)

side_coef = 0
side_center = 0.15
center_coef = 1
center_side = 0.85
xsmth = 0.15
x_func = coef_function(side_coef,
                       side_center,
                       center_coef,
                       center_side,
                       side_coef,
                       smth = xsmth)

front_coef = 1
front_back = 0.5
back_coef = 0
ysmth = 0.2
y_func = coef_function(front_coef,
                       front_back,
                       back_coef,
                       smth = ysmth)
#%%
xyz_coef = np.asarray([[1, 1.72, 0.25]])
norm_torso_vert = (torso_vertices - np.array([[min_x, min_y, min_z]])
                                             )/np.array([[max_x-min_x, max_y-min_y, max_z-min_z]])

coefs = np.zeros((torso_vertices.shape[0], 1))
for i, point in enumerate(norm_torso_vert):
    x, y, z = point
    coefs[i] = x_func.eval(x) * y_func.eval(y) * z_func.eval(z)

    
coefs = coefs*xyz_coef
coefs /= np.mean(np.linalg.norm(coefs, axis=1))

#%%
n = 24
relat = torso_vertices - np.array([[mx, my, mz]])

allgeo = {}
for amount in np.linspace(0, 0.1, n):
    new_vertices = torso_vertices + relat*coefs*amount
    name = '0000' + str(int(np.round((amount/0.2)*1000)))
    name = name[-4:]
    name = name[0] + 'p' + name[1:]
    allgeo['vertice_' + name] = new_vertices

#%%
allgeo['triangles'] = torso_faces +1
sio.savemat('/media/italo/HDD/Solução direta e inversa/Camila/Paciente1/breathing' + str(n) + '.mat', allgeo)

#%%
scene = vedo.Plotter(interactive=True)
scene += vedo.Mesh([new_vertices, torso_faces], c='w').lw(2).lc('black')

scene.show().close()
