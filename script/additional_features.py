# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 09:39:22 2020

This function creates extra features based on the data extracted in the methodology 
of the second paper (i.e. considering all the arrhythmias, a 6% spatial mask and 
subsequent phase analysis)

@author: vgmarques
"""

import os
import scipy.signal as sig
import numpy as np
root = os.path.join('D:\\','vgmar')# expanduser does not work in my pc with 2 HDs
library_folder = os.path.join(root,'Documents','GitHub',
                              'BSPM-analysis','Library')
os.chdir(library_folder)
import refined_pf as pf
import pandas as pd
import scipy.io as sio

folder_list = ['TA_ICV','TA_LSPV','TA_RAA','TA_RIPV',
                'Flutter_tip','Flutter_tiphor','Flutter_VCI','Flutter_VP',
                'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV',
                'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0',
                'Rotor_RIPV','AF_RotorRIPV','AF_RSPV']

layout_list = ['HR.mat','layout_cardioinsight.mat','layout_leicester_healthy.mat','layout_guillem.mat',
               'layout_salinet.mat','layout_uniform32.mat','layout_uniform16.mat','layout_oosterom.mat']
Layouts = {'HR.mat':'HR',
           'layout_cardioinsight.mat':'layout_cardioinsight',
           'layout_leicester_healthy.mat':'layout_leicester_healthy',
           'layout_guillem.mat':'layout_guillem',
           'layout_salinet.mat':'layout_salinet',
           'layout_uniform32.mat':'layout_uniform32',
           'layout_uniform16.mat':'layout_uniform16',
           'layout_uniform64.mat':'layout_uniform64',
           'layout_bojarnejad.mat':'layout_bojarnejad',
           'layout_oosterom.mat':'layout_oosterom'}
data_path = os.path.join(root,'Documents','Data')

spat_filt = 0.07
DF_folder = 'DF_CBM_'+str(np.round(spat_filt*100,1))+'p'

SP_folder = 'SP_CBM'

for layout in layout_list:
    
    # Freq results
    DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[layout],'CBM_R1',DF_folder)
    df_results = pd.read_csv(DF_results_path+'\DF_results.csv')
    model_freq = df_results['HDF_1']
    
    ## Layout geometry
    layout_path = os.path.join(data_path,'files_journal','Models',Layouts[layout_list[0]],'TA_ICV','raw_signals_CBM_R1')
    xi =  pf.fastMatRead(layout_path, 0,'xi.mat', 'xi').flatten()
    yi =  pf.fastMatRead(layout_path, 0,'yi.mat', 'yi').flatten()
    Cpts =  pf.fastMatRead(layout_path, 0,'Cpts.mat', 'Cpts')

#%% Features in the frequency domain

    ### % of front and back of torso with fdrive regions
    
    ## Front and back indexes
    lims_front = (np.where(xi<=0)[0][0],np.where(xi<=0)[0][-1]+1) #The +1 is for the indexing in python
    lims_back = (np.where(xi>0)[0][0],np.where(xi>0)[0][-1]+1) 

    ## left and right indexes
    lims_left = (int(len(xi)/4),int(3*len(xi)/4)) #
    lims_right = (0,int(len(xi)/4),int(3*len(xi)/4),len(xi)) #Needs "two halves"
    
    ## Loop over models and get the percentages
    perc_fdrive_regions = np.zeros((len(folder_list),12)) #front, back,left,right, for OI + overall mean and std OI
    for M,basename in enumerate(folder_list):
        # Load map
        df_path = os.path.join(data_path,'files_journal','Models',
                                Layouts[layout],basename,
                                'raw_signals_CBM_R1',DF_folder) # Signals
                
        DF = pf.fastMatRead(df_path, 0,'Maps.mat','DF_1')  
        OI = pf.fastMatRead(df_path,0,'Maps.mat','OI_1')
        
        perc_fdrive_regions[M,10] = np.percentile(DF.flatten(),75)-np.percentile(DF.flatten(),25)
        perc_fdrive_regions[M,11] = np.percentile(OI.flatten(),75)-np.percentile(OI.flatten(),25)
        
        #Front    
        DF_seg = np.copy(DF)[:,lims_front[0]:lims_front[1],]
        
        DF_seg[DF_seg<model_freq[M]-1] = 0
        DF_seg[DF_seg>model_freq[M]+1] = 0
        DF_seg[DF_seg!=0] = 1
        
        perc_fdrive_regions[M,0] = np.sum(DF_seg)/((lims_front[1]-lims_front[0])*len(yi))
        
        #OI
        perc_fdrive_regions[M,4] = np.mean(OI[:,lims_front[0]:lims_front[1]])
        
        #Back
        DF_seg = np.copy(DF)[:,lims_back[0]:lims_back[1]]
        
        DF_seg[DF_seg<model_freq[M]-1] = 0
        DF_seg[DF_seg>model_freq[M]+1] = 0
        DF_seg[DF_seg!=0] = 1
        
        perc_fdrive_regions[M,1] = np.sum(DF_seg)/((lims_back[1]-lims_back[0])*len(yi))    
        
        #OI
        perc_fdrive_regions[M,5] = np.mean(OI[:,lims_back[0]:lims_back[1]])    
        
        #Left
        DF_seg = np.copy(DF)[:,lims_left[0]:lims_left[1]]
        
        DF_seg[DF_seg<model_freq[M]-1] = 0
        DF_seg[DF_seg>model_freq[M]+1] = 0
        DF_seg[DF_seg!=0] = 1
        
        perc_fdrive_regions[M,2] = np.sum(DF_seg)/((lims_left[1]-lims_left[0])*len(yi))     
        #OI
        perc_fdrive_regions[M,6] = np.mean(OI[:,lims_left[0]:lims_left[1]])
        
        #Right    
        DF_seg = np.concatenate((np.copy(DF)[:,lims_right[0]:lims_right[1]],
                                 np.copy(DF)[:,lims_right[2]:lims_right[3]]),axis = 1)
        
        DF_seg[DF_seg<model_freq[M]-1] = 0
        DF_seg[DF_seg>model_freq[M]+1] = 0
        DF_seg[DF_seg!=0] = 1
        
        perc_fdrive_regions[M,3] = np.sum(DF_seg)/((lims_right[1]-lims_right[0]+lims_right[3]-lims_right[2])*len(yi))     
       
        #OI
        perc_fdrive_regions[M,7] = np.mean(OI[:,lims_right[0]:lims_right[1]])
        
        # MEan and std OI (overall)
        perc_fdrive_regions[M,8] = np.mean(OI)
        perc_fdrive_regions[M,9] = np.std(OI)
        
    perc_fdrive_regions = pd.DataFrame(perc_fdrive_regions,index = folder_list,
                                       columns = ['%fdr_front','%fdr_back','%fdr_left','%fdr_right',
                                                  'OI_front','OI_back','OI_left','OI_right','OI_mean','OI_std',
                                                  'IQR_DF','IQR_OI'])


#%% Features in the phase domain
 
    ### % of front and back of torso with SPs
    
    ## Front and back indexes
    lims_front = (np.where(xi<=0)[0][0],np.where(xi<=0)[0][-1]+1) #The +1 is for the indexing in python
    lims_back = (np.where(xi>0)[0][0],np.where(xi>0)[0][-1]+1) 
    
    ## left and right indexes
    lims_left = (int(len(xi)/4),int(3*len(xi)/4)) #
    lims_right = (0,int(len(xi)/4),int(3*len(xi)/4),len(xi)) #Needs "two halves"
    
    ## Loop over models and get the percentages
    perc_SPs = np.zeros((len(folder_list),10)) #front, back,left,right
    for M,basename in enumerate(folder_list):
    
        sp_path = os.path.join(data_path,'files_journal','Models',
                                Layouts[layout],basename,
                                'raw_signals_CBM_R1',DF_folder,SP_folder)
        
        rotors = pf.fastMatRead(sp_path,0,'SP_analysis.mat','rotors')
        r,c,n = rotors.shape
        
        ## Load dicts
        SPs_filt = sio.loadmat(sp_path+'/SPs_filt.mat')
        del SPs_filt['__header__']
        del SPs_filt['__globals__']
        del SPs_filt['__version__']
        
        rotors_fil = pf.dict2matrix(SPs_filt,rotors.shape)
        HM = pf.HeatMap(-rotors_fil)
        # Percentage of SPs wrt. total of SPs
        if np.sum(HM)==0:
            perc_SPs[M,0:4] = 0
        else:
        
            #Front    
            HM_seg = np.copy(HM)[:,lims_front[0]:lims_front[1],]
            
            HM_seg[HM_seg!=0] = 1
            
            perc_SPs[M,0] = np.sum(HM_seg)/np.sum(HM)
            
            #Back
            HM_seg = np.copy(HM)[:,lims_back[0]:lims_back[1]]
        
            HM_seg[HM_seg!=0] = 1
            
            perc_SPs[M,1] = np.sum(HM_seg)/np.sum(HM)    
            
            #Left
            HM_seg = np.copy(HM)[:,lims_left[0]:lims_left[1]]
        
            HM_seg[HM_seg!=0] = 1
            
            perc_SPs[M,2] = np.sum(HM_seg)/np.sum(HM)     
            
            #Right    
            HM_seg = np.concatenate((np.copy(HM)[:,lims_right[0]:lims_right[1]],
                                     np.copy(HM)[:,lims_right[2]:lims_right[3]]),axis = 1)
            
            HM_seg[HM_seg!=0] = 1
            
            perc_SPs[M,3] =np.sum(HM_seg)/np.sum(HM)
        
        # Percentage of SPs wrt. area
         #Front    
        HM_seg = np.copy(HM)[:,lims_front[0]:lims_front[1],]
        
        HM_seg[HM_seg!=0] = 1
        
        perc_SPs[M,4] = np.sum(HM_seg)/((lims_front[1]-lims_front[0])*len(yi))
        
        #Back
        HM_seg = np.copy(HM)[:,lims_back[0]:lims_back[1]]
    
        HM_seg[HM_seg!=0] = 1
        
        perc_SPs[M,5] = np.sum(HM_seg)/((lims_back[1]-lims_back[0])*len(yi))   
        
        #Left
        HM_seg = np.copy(HM)[:,lims_left[0]:lims_left[1]]
    
        HM_seg[HM_seg!=0] = 1
        
        perc_SPs[M,6] = np.sum(HM_seg)/((lims_left[1]-lims_left[0])*len(yi))      
        
        #Right    
        HM_seg = np.concatenate((np.copy(HM)[:,lims_right[0]:lims_right[1]],
                                 np.copy(HM)[:,lims_right[2]:lims_right[3]]),axis = 1)
        
        HM_seg[HM_seg!=0] = 1
        
        perc_SPs[M,7] =np.sum(HM_seg)/((lims_right[1]-lims_right[0]+lims_right[3]-lims_right[2])*len(yi))     
        
        
        #Other phase features
        try:
            perc_SPs[M,8], perc_SPs[M,9]  = pf.fil_displacement(SPs_filt,xi,yi)#  [0]   
        except: 
            perc_SPs[M,8] = np.nan  
            perc_SPs[M,9] = np.nan              
    
        
        
    perc_SPs = pd.DataFrame(perc_SPs,index = folder_list,
                            columns = ['%SP_front','%SP_back','%SP_left','%SP_right',
                                       '%SP_area_front','%SP_area_back','%SP_area_left','%SP_area_right',
                                       'fil_displacement_mean','fil_displacement_std'])

    #%% Other features?
    
    #%% Save
    
    final_DF = pd.merge(perc_fdrive_regions,perc_SPs,left_index = True,right_index = True)
    
    final_DF.index = folder_list
        
    os.chdir(data_path)
    os.chdir('..')
    savepath =os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[layout],'CBM_R1',DF_folder)# attention: This parameter must be adusted 
    
    if not os.path.exists(savepath):
        os.makedirs(savepath)    
    os.chdir(savepath)
    
    final_DF.to_csv('extra_features.csv')
    

