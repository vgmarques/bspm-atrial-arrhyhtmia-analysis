#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 28 15:48:47 2021

@author: italo
"""

import matplotlib.pyplot as plt
# import scipy.io as sio
import numpy as np
import os
import pandas as pd
import oct2py
oc = oct2py.Oct2Py()


from matplotlib import cm

# data_path = '/media/italo/HDD/atrial_interpolation/atria/solinversa_laplace_models/results'
# data_path = '/media/italo/HDD/SpatialNoise/Atrios/extremo/results'
data_path = '/media/italo/HDD/SignalsInterpolation/linear_atria_inversa/results'

library_folder = os.path.join('/home/italo/scripts/vgmarques-bspm-atrial-arrhyhtmia-analysis/')

# os.chdir(os.path.join(library_folder,'fileIO'))
# import fileIO_lib as fio

# os.chdir(os.path.join(library_folder,'accessory'))
# import accessory_lib as acc

# os.chdir(os.path.join(library_folder,'phase_analysis'))
# import phaseAnalysis_lib as pha

# os.chdir(os.path.join(library_folder,'preprocessing'))
# import preprocessing_lib as pre

os.chdir(os.path.join(library_folder,'freq_analysis'))
import frequencyAnalysis_lib as fqa

import mpl_toolkits.mplot3d as a3
import matplotlib.colors as colors

def trisurf(x, y, z, triangles, c, cmap='jet', edgecolor='k'):
    if type(cmap) is str:
        cmap = getattr(cm, cmap)
    
    fig = plt.figure()
    ax = a3.Axes3D(fig)
    for i, triangle in enumerate(triangles):
        ci = (np.min(c[triangle])-c.min())/(c.max()-c.min())
        tri = a3.art3d.Poly3DCollection(np.asarray([x[triangle], y[triangle], z[triangle]]).T.tolist())
        tri.set_color(cmap(ci))
        tri.set_edgecolor(edgecolor)
        ax.add_collection3d(tri)

    ax.set_xlim([x.min(), x.max()])
    ax.set_ylim([y.min(), y.max()])
    ax.set_zlim([z.min(), z.max()])

    fig.colorbar(cm.ScalarMappable(norm=colors.Normalize(vmin=c.min(), vmax=c.max()), cmap=cmap), ax=ax, shrink=0.5)
    return fig, ax

def get_displacement(filaments, geometry, samp_freq):
    disps = []
    for fil in filaments:
        fil = np.asarray(fil)
        if fil.shape[0] <= 1:
            continue
        fil = fil[np.argsort(fil[:,1]),:]
        xyz = geometry[fil[:,0]]
        ds = xyz[1:] - xyz[:-1]
        ds = np.sqrt(np.sum(ds**2, axis=1))
        dt = (fil[1:,1] - fil[:-1,1] + 1)/samp_freq
        disp = np.mean(ds/dt)
        disps.append(disp)
    
    if len(disps)==0:
        return 0

    return np.mean(disps)

def openmorph3d(components, connectivity, tol=0, r=1):
    opened = components.copy()
    for _ in range(r):
        mask = np.zeros(connectivity.shape[0], bool)
        mask[opened] = True
        opened = np.zeros(connectivity.shape[0], bool)
        for ind in components:
            if mask[connectivity[ind]].sum() >= min(max(connectivity[ind].sum()-tol,0),connectivity[ind].sum()):
                opened[ind] = True
        opened = np.where(opened)[0].tolist()
        if len(opened)==0:
            break
    return opened

def closemorph3d(components, connectivity, lim=1, r=1):
    closed = components.copy()
    for _ in range(r):
        mask = np.zeros(connectivity.shape[0], bool)
        mask[closed] = True
        closed = np.zeros(connectivity.shape[0], bool)
        closed |= mask
        for ind in np.where(~mask)[0]:
            if mask[connectivity[ind]].sum() >= max(1,lim):
                closed[ind] = True
        closed = np.where(closed)[0].tolist()
        if len(closed)==0:
            break
    return closed

# def connectedNodes(comp, triangles):
#     def explore_neig(neigh, tri, comp):
#         regi = []
#         for ind in neigh:
#             if ind in comp:
#                 regi.append(ind)
#                 comp.pop(np.where(np.asarray(comp)==ind)[0][0])
#                 neigh_inds = triangles[np.sum(triangles==ind, axis=1).astype(bool)].ravel()
#                 neigh_inds = np.unique(neigh_inds)
#                 neigh_inds = neigh_inds[neigh_inds!=ind]
#                 regi.extend(explore_neig(neigh_inds, tri, comp))
#         return regi
    
#     regions = []
#     while len(comp) > 0:
#         regii = []
#         regii.extend(explore_neig([comp[0]], triangles, comp))
#         regions.append(regii)
        
    return regions

def connectedNodes(comp, triangles, connect2 = False):
    comp = comp.copy()
    connectivity = np.zeros((2039,2039), bool)
    for i, j, k in triangles:
        connectivity[i,j] = True
        connectivity[i,k] = True
        connectivity[j,i] = True
        connectivity[k,i] = True
        connectivity[j,k] = True
        connectivity[k,j] = True
    
    if connect2:
        connectivity2 = connectivity.copy()
        for i in range(connectivity.shape[0]):
            connectivity2[i] = connectivity[connectivity[i],:].sum(axis=0).astype(bool)
        connectivity2[np.arange(connectivity2.shape[0]),np.arange(connectivity2.shape[0])] = False
        
    def explore_neig(neigh, tri, comp):
        regi = []
        for ind in neigh:
            if ind in comp:
                regi.append(ind)
                comp.pop(np.where(np.asarray(comp)==ind)[0][0])
                
                if connect2:
                    neigh_inds = np.where(connectivity2[ind])[0]
                else:
                    neigh_inds = np.where(connectivity[ind])[0]

                regi.extend(explore_neig(neigh_inds, tri, comp))
        return regi
    
    regions = []
    while len(comp) > 0:
        regii = []
        regii.extend(explore_neig([comp[0]], triangles, comp))
        regions.append(regii)
        
    return regions

def triang_areas(triangposis):
    trianvecs = triangposis[:,:2,:] - triangposis[:,[2],:]
    dets = np.cross(trianvecs[:,0,:], trianvecs[:,1,:], axis=-1)
    mod = np.sqrt(np.sum(dets**2, axis=-1))/2
    return mod

def get_region_areas(regions, geometry, triangles):
    areas = np.zeros(len(regions))

    for i, reg in enumerate(regions):
        isin = np.zeros(triangles.shape, bool)
        for r in reg:
            isin += triangles==r
        
        inner = np.product(isin,  axis=1).astype(bool)
        outer = np.sum(isin,  axis=1).astype(bool)
        onlyout = outer*(~inner)
        areain = triang_areas(geometry[triangles[inner,:],:]).sum()
        areaout = triang_areas(geometry[triangles[onlyout,:],:]).sum()/2
        areas[i] = areain + areaout
    
    return areas

#%%
# def get_fils(rotind, connectivity2):
#     rotind = rotind.copy()
#     #separate filaments in time
#     fils = []
#     for j in range(rotind.shape[1]):
#         fili = []
#         count = 0
#         for i in range(rotind.shape[0]):
#             if rotind[i,j] != 0:
#                 fili.append([rotind[i,j]-1, i])
#                 count = 0
#             else:
#                 count += 1

#             if count >= 3:
#                 if len(fili)>0:
#                     fils.append(fili)
#                     fili = []

#         if len(fili) > 0:
#             fils.append(fili)

#     #separate filaments in space
#     final_fils = []
#     for fili in fils:
#         filsi = []
#         nfili = [fili[0]]
#         for i in range(len(fili)-1):
#             if fili[i+1][0] in np.where(connectivity2[fili[i][0]])[0]:
#                 nfili.append(fili[i+1])

#             elif len(filsi)>0:
#                 if (fili[i+1][0] in np.where(connectivity2[filsi[-1][-1][0]])[0]) and (abs(fili[i+1][1] - filsi[-1][-1][1])<=5):
#                     filsi.append(nfili)
#                     nfili = filsi.pop(-2)
#                     nfili.append(fili[i+1])

#                 else:
#                     filsi.append(nfili)
#                     nfili = [fili[i+1]]

#             else:
#                 filsi.append(nfili)
#                 nfili = [fili[i+1]]

#         filsi.append(nfili)
#         final_fils.extend(filsi)
#         print("initial number of filaments:", len(fils))
#         print("final number of filaments:", len(final_fils))
#     return final_fils

def get_fils(rotind, connectivity, time_tol=5):
    rotind = np.asarray(rotind).copy()
    
    rotind = np.concatenate((rotors.ravel()[rotors.ravel()>0].reshape(-1,1)-1,
                              np.where(rotors>0)[0].reshape(-1,1)), axis=1)
    rotind = rotind[np.argsort(rotind[:,1])]
    if rotind.size == 0:
        return []
    fils = [[rotind[0,:].tolist()]]
    for i in range(1, rotind.shape[0]):
        isin = False
        for j, fil in enumerate(fils):
            if rotind[i,0] in np.where(connectivity[rotind[i,0]])[0]:
                fils[j].append(rotind[i,:].tolist())
                isin = True
                break
        if not isin:
            fils.append([rotind[i,:].tolist()])
        
    print("initial number of filaments:", len(fils))

    fils_2 = []
    for fil in fils:
        leaps = np.where(np.diff(np.asarray(fil)[:,1])>time_tol)[0]
        leaps = np.r_[0, leaps, len(fil)]
        for i in range(len(leaps)-1):
            fils_2.append(fil[leaps[i]:leaps[i+1]+1])
    print("final number of filaments:", len(fils_2))
    
    fils_final = []
    for fil in fils_2:
        fils_final.append(np.asarray(fil))    
    
    return fils_final

#%%
def completeDF_BSPM(isig, fs, fmin=0.5, fmax=30, df_range=[2,12], band_oi=1, thresh_oi=15, spat_filt=0.02,
                    wav_lims=[3,30], DF_peaks=0): #TODO: this function needs serious improvement
    
    """
    This function encompasses all of the implemented DF analysis so far:
        - DF is determined using welch periodograms (1.5 s segments, 50% overlap)
        - If the option is chosen, activation rates estimated via wavelet are 
        used to complement this analysis.
        - If the option is chosen, OI can be used to complement the previous 
        analysis
        - RI and OI are calculated and filters can be implemented to aid HDF and
        mode determination
        
    Parameters:
        - isig: signal in 3 dimensions, time is the -1 axis
        - fs: sampling frequency in Hz
        - fmin and fmax: limit frequencies for filtering
        - df_range: limits for DF determination
        - det_method: chooses method for determining DF:
            0 - only Welch
            1 - Welch + Activation
            2 - Welch + OI + Activation
            3 - Welch + RI + Activation
            4 - Wavelet energy method (only)
            5 - Wavelet energy and Welch
        - band_oi: frequency band around peaks for RI and OI determination
        - filter_option: chooses method for masking frequencies for HDF and mode
        determination:
            0 - no filter
            1 - harmonic filter (outdated)
            2 - fixed threshold OI filter
            3 - spatial filter
        - thresh_oi: threshold for OI filter above. If [0,1), the threshold is 
        fix , if [1,+inf) it is a percentile
    
    """
    
    # thresh_oi can be given either as OI value [0,1] or percentile ]1,100]
    l,c,n = isig.shape
        

    ## Add conditional for using activation and OI

    DF_peaks = fqa.energy_WTMM(isig,fs,nscales = 40,return_act = False, lims = wav_lims)
#    
    ## RI, OI
    RI,OI = fqa.getRIOI(isig,DF_peaks,fs,fmin,fmax,5,band_oi)
    
    dfs, counts = np.unique(DF_peaks[~np.isnan(DF_peaks)], return_counts=True)
#        if np.isnan(dfs)==np.prod(dfs.shape): 
#            HDF = -1
#        else:
    ind = np.where(counts==np.max(counts))
    moda = dfs[ind][0]

    HDF = np.nanmax(DF_peaks)

    return DF_peaks, moda, HDF, RI, OI, isig

#%%
def get_connectivity(triangles, max_dist=1, autoindex=True):
    """Get 1st and 2nd order connectivity matrix from triangles."""
    size = triangles.max() + 1
    connectivity = np.zeros((size, size), bool)
    for i, j, k in triangles:
        connectivity[i,j] = True
        connectivity[i,k] = True
        connectivity[j,i] = True
        connectivity[k,i] = True
        connectivity[j,k] = True
        connectivity[k,j] = True
    
    for _ in range(max_dist):
        connectivity2 = connectivity.copy()
        for i in range(size):
            connectivity2[i] = connectivity[connectivity[i],:].sum(axis=0).astype(bool)
            
        connectivity = connectivity2.copy()
        
    if autoindex:
        connectivity[np.arange(size), np.arange(size)] = True
    else:
        connectivity[np.arange(size), np.arange(size)] = False
    
    return connectivity

#%%

dtypes = [('mean_1/HDF_1', 'f8'),
          ('number of regions', 'f8'),
          ('avg. size', 'f8'),
          ('total area', 'f8'),
          ('IQR_DF', 'f8'),
          ('RI_mean','f8'),
          ('IQR_RI','f8'),
          ('Filament rate (N fil/s)','f8'),
          ('Mean Duration (% duration)','f8'),
          ('Number of regions', 'f8'),
          ('size','f8'),
          ('density','f8'),
          ('fil_displacement_mean','f8'),
          ('path', 'U64')]

columns = ['mean_1/HDF_1',
           'number of regions',
           'avg. size',
           'total area',
           'IQR_DF',
           'RI_mean',
           'IQR_RI',
           'Filament rate (N fil/s)',
           'Mean Duration (% duration)',
           'Number of regions',
           'size',
           'density',
           'fil_displacement_mean',
           'path']

dataframe = pd.DataFrame(columns = columns, index=[])


i = 0 
for root, dirs, files in os.walk(data_path):
    for file in files:
        if file.endswith('.mat'):

            # data = sio.loadmat(os.path.join(root, file))
            data = oc.load(os.path.join(root, file))
            
            # geom_matfile = data['atrial_model']
            geometry = data['atrial_model']['vertices']
            triangles = data['atrial_model']['faces'] -1
            triangles = triangles[np.product(triangles<2039, axis=1)==1]
            connectivity = get_connectivity(triangles, max_dist=1, autoindex=True)
            connectivity2 = get_connectivity(triangles, max_dist=2, autoindex=True)

            total_area = get_region_areas([np.arange(2039)], geometry, triangles)[0]
            
            df_i = np.zeros(1, dtype=dtypes)     

            DF_peaks = data['DF_wtmm'][:2039]
            RI = data['RI_wtmm'][:2039,0]
            dfs = np.sort(DF_peaks[~np.isnan(DF_peaks)].flatten())
            df_i['IQR_DF'] = np.percentile(dfs, 75) - np.percentile(dfs, 25)
            HDF = np.nanmax(dfs[:int(len(dfs)*(1-0.07))])
            df_i['mean_1/HDF_1'] = dfs.mean()/HDF
            signals = data['x_hat']
            fs = data['fs']
            rotors = np.asarray(data['finalrotors'])
            if rotors.size <=1:
                rotors = np.empty((0,1))
            
            rotors = rotors.astype(int)
            filaments = get_fils(rotors, connectivity2)
            

            HDF_regions = np.zeros(2039, bool)
            HDF_regions[(dfs<HDF+1)*(dfs>HDF-1)] = True
            components = np.where(HDF_regions)[0].ravel().tolist()

            regions = connectedNodes(
                            openmorph3d(
                                closemorph3d(components,
                                             connectivity, r=1),
                                                connectivity, r=1),
                                                         triangles)

            df_i['number of regions'] = len(regions)

            areas = get_region_areas(regions, geometry, triangles)/total_area * 100

            df_i['avg. size'] = areas.mean()
            df_i['total area'] = areas.sum()
            df_i['RI_mean'] = RI.mean()
            df_i['IQR_RI'] = np.percentile(RI, 75) - np.percentile(RI, 25)
            if rotors.size > 0:
                df_i['Filament rate (N fil/s)'] = len(filaments)/(signals.shape[-1]/fs)
                durations = [(fil[:,1].max() - fil[:,1].min() + 1)/fs for fil in filaments]
                df_i['Mean Duration (% duration)'] = np.median(durations)

                heatmap = np.zeros(signals.shape[0])
                sizes = []
                densities = []
                for fil in filaments:
                    idx, count = np.unique(fil[:,0], return_counts=True)
                    heatmap[idx] += count
                    sizes.append(get_region_areas([idx], geometry, triangles)[0])
                    densities.append(count.mean())

                heatmap_regions = get_region_areas([np.where(heatmap)[0]], geometry, triangles)

                df_i['Number of regions'] = len(heatmap_regions)
                
                df_i['size'] = np.median(sizes)/total_area * 100   #number of indexes where value is not zero
                df_i['density'] = np.median(densities) 

                df_i['fil_displacement_mean'] = get_displacement(filaments, geometry, fs)
            
            else:
                df_i['Filament rate (N fil/s)'] = 0
                df_i['Mean Duration (% duration)'] = 0
                df_i['Number of regions'] = 0
                df_i['size'] = 0
                df_i['density'] = 0
                df_i['fil_displacement_mean'] = 0
            
            name = root[len(data_path):]
            df_i['path'] = name
            
            df_i = pd.DataFrame(data=df_i, columns= columns, index=[i])
            dataframe = dataframe.append(df_i)
            i += 1
            
            print('pronto:', name)

dataframe.to_csv(data_path + '/biomarkers.csv', sep = ";", decimal=".")
