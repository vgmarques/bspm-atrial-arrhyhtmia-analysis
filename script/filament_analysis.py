# -*- coding: utf-8 -*-
"""
Created on Mon Jan  7 13:38:27 2019
To analyze the filaments from SP_analysis(120718)
@author: vgmarques
"""

import os
root = os.path.join('D:\\','vgmar')# expanduser does not work in my pc with 2 HDs
library_folder = os.path.join(root,'Documents','GitHub',
                              'BSPM-analysis','Library')
os.chdir(library_folder)
import numpy as np
import refined_pf as pf
import scipy.io as sio
import pandas as pd
from pandas import DataFrame as df
"""

"""

folder_list = ['TA_ICV','TA_LSPV','TA_RAA','TA_RIPV',
                'Flutter_tip','Flutter_tiphor','Flutter_VCI','Flutter_VP',
                'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV',
                'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0',
                'Rotor_RIPV','AF_RotorRIPV','AF_RSPV']

# Atualizado em 26/07/2019
real_frequencies = np.array([4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.80,5.60,5.50,7.80,
                             7.60,5.40,5.80,5.80,5.5,5.4,6.8])


layout_list = ['HR.mat', 'layout_cardioinsight.mat','layout_leicester_healthy.mat',
               'layout_guillem.mat','layout_salinet.mat',
               'layout_uniform32.mat','layout_uniform16.mat','layout_oosterom.mat']

Layouts = {'HR.mat':'HR',
           'layout_cardioinsight.mat':'layout_cardioinsight',
           'layout_leicester_healthy.mat':'layout_leicester_healthy',
           'layout_guillem.mat':'layout_guillem',
           'layout_salinet.mat':'layout_salinet',
           'layout_uniform32.mat':'layout_uniform32',
           'layout_uniform16.mat':'layout_uniform16',
           'layout_uniform64.mat':'layout_uniform64',
           'layout_bojarnejad.mat':'layout_bojarnejad',
           'layout_oosterom.mat':'layout_oosterom'}


fs_low = 128


col_names = ['Mean Duration (% duration)',
             'Std. Duration (% Duration)',
             'Mean Frequency (Hz)',   
             'Std. Frequency (Hz)',
             'Mean direction of rotation',
             'Filament rate (N fil/s)',
             'Simulation duration (s)']  
 
df_cols = ['layout','subject','arrhythmia','length','frequency','direction']
data_types= dtype=[('layout', 'U20'),('subject', 'U20'), ('arrhythmia', 'U3'), 
                   ('length', 'f8'),('frequency', 'f8'),
                   ('direction','i')]
data_path = os.path.join(root,'Documents','Data')

# DF results
spat_filt = 0.07
DF_folder = 'DF_CBM_'+str(np.round(spat_filt*100,1))+'p'
SP_folder = 'SP_CBM'

#%%

for layout in layout_list:

    Fils_information = np.zeros(shape = (len(folder_list),len(col_names)))
    DF_filaments = pd.DataFrame(columns = df_cols)
    
    for M,basename in enumerate(folder_list):
        os.chdir(library_folder)
    
        load_path = os.path.join(data_path,'files_journal','Models',
                                Layouts[layout],basename,
                                'raw_signals_CBM_R1',DF_folder,SP_folder) # Signals
            
        rotors =  pf.fastMatRead(load_path,0,'SP_analysis.mat','rotors')
        HMG =  pf.fastMatRead(load_path,0,'SP_analysis.mat','HMG')
        SPs_dur =  pf.fastMatRead(load_path,0,'SP_analysis.mat','SPs_dur')
        
        # Load dicts
        SPs_filt = sio.loadmat(load_path+'/SPs_filt.mat')
        del SPs_filt['__header__']
        del SPs_filt['__globals__']
        del SPs_filt['__version__']
    
        # New: create variable to stack the information
        df_temp = np.zeros(len(SPs_filt),dtype =data_types)
        
        df_temp['layout'] = Layouts[layout]
        df_temp['subject'] = basename
        
        if M<4:
            df_temp['arrhythmia'] = 'AT'
        elif M<8:
            df_temp['arrhythmia'] = 'AFL'
        else:
            df_temp['arrhythmia'] = 'AF'
         
        # Get overall duration
        over_dur = rotors.shape[-1]/fs_low
        Fils_information[M,6] = over_dur
    
        #### Duration
        try:
            df_temp['length'] = SPs_dur/1000/over_dur*100 # SPs_dur is in ms, 100 is for the %
        
            Fils_information[M,0] = np.mean(SPs_dur/1000/over_dur)*100
            Fils_information[M,1] = np.std(SPs_dur/1000/over_dur)
            # Get "appearance rate"
            Fils_information[M,5] = len(SPs_filt)/over_dur
        except:
            df_temp['length'] = np.nan
            Fils_information[M,0] = np.nan
            Fils_information[M,1] = np.nan
            Fils_information[M,5] = np.nan
        # Get other info
        freq_fil = {}
        cycles_fil = {}
        sinn_fil = {}
        
        for key in SPs_filt:
            freq_fil[key],cycles_fil[key],\
            sinn_fil[key] = pf.cyclesFromFilament(SPs_filt[key],HMG,11,fs_low)
                
        
        # Frequency
        freqs = np.asarray([freq_fil[key] for key in freq_fil])
        df_temp['frequency'] = freqs
        
        Fils_information[M,2] = np.mean(freqs)
        Fils_information[M,3]= np.std(freqs)
        
        # Sinn
        sinns = np.asarray([sinn_fil[key] for key in sinn_fil])
        df_temp['direction'] = sinns
        Fils_information[M,4]= np.mean(sinns)
        df_temp =pd.DataFrame(df_temp)
        
        DF_filaments = DF_filaments.append(df_temp,ignore_index = True)
        
     
    df_Fils = df(Fils_information,columns = col_names)#,index = folder_list)
    
    savepath = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[layout],'CBM_R1',DF_folder)  # Attention: adjust this parameter
       
    DF_filaments.to_csv(savepath+'/all_filaments.csv')
    df_Fils.to_csv(savepath+'/filament_analysis.csv') # Attention: adjust this parameter

    print(layout)
