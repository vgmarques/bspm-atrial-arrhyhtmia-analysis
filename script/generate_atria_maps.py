#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 23:56:51 2021

@author: italo
"""
# import matplotlib.pyplot as plt
# import scipy.signal as sig
import scipy.io as sio
# import open3d as o3d
import oct2py
oc = oct2py.Oct2Py()
import numpy as np
import os

#%%
root = os.path.join('/media', 'italo', 'HDD', 'Users','ital_')
library_folder = os.path.join('/home','italo','scripts','vgmarques-bspm-atrial-arrhyhtmia-analysis')

# os.chdir(os.path.join(library_folder,'fileIO'))
# import fileIO_lib as fio

# os.chdir(os.path.join(library_folder,'accessory'))
# import accessory_lib as acc

# os.chdir(os.path.join(library_folder,'phase_analysis'))
# import phaseAnalysis_lib as pha

# os.chdir(os.path.join(library_folder,'preprocessing'))
# import preprocessing_lib as pre

os.chdir(os.path.join(library_folder,'freq_analysis'))
import frequencyAnalysis_lib as fqa

# os.chdir(os.path.join(library_folder,'script'))
# from myFacilities import medianfilter

#%%
def get_connectivity(triangles, autoindex=True):
    """Get 1st and 2nd order connectivity matrix from triangles."""
    size = triangles.max() + 1
    connectivity = np.zeros((size, size), bool)
    for i, j, k in triangles:
        connectivity[i,j] = True
        connectivity[i,k] = True
        connectivity[j,i] = True
        connectivity[k,i] = True
        connectivity[j,k] = True
        connectivity[k,j] = True
    
    if autoindex:
        connectivity[np.arange(size), np.arange(size)] = True

    connectivity2 = connectivity.copy()
    for i in range(size):
        connectivity2[i] = connectivity[connectivity[i],:].sum(axis=0).astype(bool)
    
    if not autoindex:
        connectivity2[np.arange(size), np.arange(size)] = False
    
    return connectivity, connectivity2

#%%
#removing top and bottom of torso geometry
# path = os.path.join('/media', 'italo', 'HDD', 'Solução direta e inversa/Camila/Paciente2')
path = os.path.join('/media/italo/HDD/SpatialNoise/Atrios/')
# matfile = sio.loadmat(os.path.join(path, 'Modelo_140807_AF_RA_0p25_35.mat'))
atria_model = sio.loadmat(os.path.join(path, 'atrial_model.mat'))['atrial_model']

#%%

nomes = ['Ut_cheio', 
         'Ut_fimresp', 
         'Ut_inicioresp',
         'Ut_repouso']

keys = ['vertice_0p000',
        'vertice_0p333',
        'vertice_0p667',
        'vertice_1p000']

#%%

arch = ['Modelo_140807_AF_RA_0p25_35.mat',
        'Modelo_141219_AF_RAA_1to0.mat',
        'Modelo_150115_AF_RSPV.mat',
        'Modelo_150119_AF_LIPV.mat',
        'Modelo_150120_RotorRIPV_reord.mat',
        'Modelo_150121_AF_RotorRIPV.mat',
        'Modelo_150203_RotorLSPV_reord.mat',
        'Modelo_150603_AFlutter_tipic_reord.mat',
        'Modelo_150730_AFlutter_tipichorari_reord.mat',
        'Modelo_150730_AFlutter_VCI_cw_reord.mat',
        'Modelo_150730_AFlutter_VP_reord.mat',
        'Modelo_150730_TA_ICV_reord.mat',
        'Modelo_150730_TA_LSPV_reord.mat',
        'Modelo_150730_TA_RAA_reord.mat',
        'Modelo_150730_TA_RIPV_reord.mat']

#%%

# direc = '/media/italo/HDD/atrial_interpolation/atria/solinversa_laplace_models/data'
# direc = '/media/italo/HDD/SpatialNoise/Atrios/extremo/dados'
direc = '/media/italo/HDD/SignalsInterpolation/linear_atria_inversa/dados'

# keys = ['x_hat_000',
#         'x_hat_333',
#         'x_hat_667',
#         'x_hat_1000']

# keys = ['x_hat_0',
#         'x_hat_1',
#         'x_hat_2',
#         'x_hat_3']

models = [ 'x_hat_3_1000_model1',
           'x_hat_3_1000_model2',
           'x_hat_3_1000_model3',
           'x_hat_3_1000_model4',
           'x_hat_3_1000_model5' ]

geos = ['Atria_0',
        'Atria_1',
        'Atria_2',
        'Atria_3']



# models = ['model1',
#           'model2',
#           'model3',
#           'model4',
#           'model5']

# keys2 = ['MFFT_hat_000',
#         'MFFT_hat_333',
#         'MFFT_hat_667',
#         'MFFT_hat_1000']

for root, dirs, files in os.walk(direc, topdown=False):
    for filename in files:
        if filename.endswith('.mat'):
            
            signal_name = filename[:-4]
            file = sio.loadmat(os.path.join(root, filename))
            # assert Falsemode
            # for i in range(4):
            for model in file.keys():
                if model in ['__globals__', '__header__', '__version__', 'Atria_0', 'fs']:
                    continue
                
                METHOD = 1
                fmin = 0.5
                fmax = 30
                df_range = [2,15]
                lims = [3,30]
                filter_option= 'SPATIAL'
                spat_filt = 0.07
                
                #Load Signals
                fs = 500 # Hz
                

                # nokey = True
                # for k in file.keys():
                #     if k.startswith(model):
                #         model = k
                #         nokey = False
                #         break
                
                
                # if nokey:
                #     if model == models[0]:
                #         print('oops')
                #     continue
                    
                # keys = root[len(os.path.dirname(root))+1:]
                path = os.path.join(os.path.dirname(direc), 'results', model)
                
                # breating=====
                # path = os.path.join(os.path.dirname(direc), 'results', keys[i], root[len(direc)+1:], signal_name)
                
                
                if not os.path.isdir(path):
                    os.makedirs(path)  
                
                # isig_raw = file[model][0,0][keys[i]][:,np.newaxis,:]
                isig_raw = file[model][:,np.newaxis,:]
                # breating=====
                # isig_raw = file[keys[i]][:,np.newaxis,:]

                
                
                DF_wtmm, mode_wtmm, HDF_wtmm, RI_wtmm, OI_wtmm, isig_filt = fqa.completeDF_BSPM(isig_raw ,fs,fmin,fmax,
                                                                                          det_method=METHOD,
                                                                                          filter_option=filter_option, 
                                                                                          spat_filt=spat_filt,
                                                                                          band_oi=1, wav_lims=lims)
                
                atria_model = file['Atria_0']
                atria_model = dict([['vertices', atria_model['node'][0,0]],
                                    ['faces', atria_model['face'][0,0]]])
                
                
                file_out = dict([['__header__', b'MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: Wed Nov 24 00:55:36 2021'],
                             ['__version__', '1.0'],
                             ['__globals__', []],
                             ['x_hat', file[model]], 
                             # ['x_hat', file[keys[i]]], 
                             ['MFFT_hat', DF_wtmm],                                     
                             ['DF_wtmm', DF_wtmm],
                             ['mode_wtmm', mode_wtmm],
                             ['HDF_wtmm', HDF_wtmm],
                             ['RI_wtmm', RI_wtmm],
                             ['OI_wtmm', OI_wtmm],
                             ['isig_filt', isig_filt],
                             ['atrial_model', atria_model],
                             ['fs', fs]])
                
                np.save(os.path.join(path, 'DF_map.npy'), DF_wtmm)
                np.save(os.path.join(path, 'HDF.npy'), HDF_wtmm)
                np.save(os.path.join(path, 'OI_map.npy'), OI_wtmm)
                np.save(os.path.join(path, 'isig_filt.npy'), isig_filt)
                sio.savemat(os.path.join(path, filename), file_out)
                