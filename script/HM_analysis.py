# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 14:41:58 2019

Phase analysis figures

@author: vgmarques
"""

import os
root = os.path.join('D:\\','vgmar')# expanduser does not work in my pc with 2 HDs
library_folder = os.path.join(root,'Documents','GitHub',
                              'BSPM-analysis','Library')
os.chdir(library_folder)
import numpy as np
from matplotlib.colors import LinearSegmentedColormap
import refined_pf as pf
import scipy.io as sio
import pandas as pd
from pandas import DataFrame as df
import cv2

folder_list = np.asarray(['TA_ICV','TA_LSPV','TA_RAA','TA_RIPV',
                'Flutter_tip','Flutter_tiphor','Flutter_VCI','Flutter_VP',
                'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV',
                'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0',
                'Rotor_RIPV','AF_RotorRIPV','AF_RSPV'])
real_frequencies = np.array([4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.00,4.80,5.60,5.50,7.80,
                 7.60,5.40,5.80,5.80,5.5,5.4,6.8])

loc_list = ['IVC','LSPV','RAA','RIPV','RA (CCW)','RA (CW)','IVC','PV',
            'LA (1)','LA (2)','LIPV','LSPV','RA (1)' ,'RA(2)','RAA (1)',
            'RAA (2)', 'RIPV (1)','RIPV (2)', 'RSPV']


layout = 'HR.mat' 
Layouts = {'HR.mat':'HR',
           'layout_cardioinsight.mat':'layout_cardioinsight',
           'layout_leicester_healthy.mat':'layout_leicester_healthy',
           'layout_guillem.mat':'layout_guillem',
           'layout_salinet.mat':'layout_salinet',
           'layout_uniform32.mat':'layout_uniform32',
           'layout_uniform16.mat':'layout_uniform16',
           'layout_uniform64.mat':'layout_uniform64',
           'layout_bojarnejad.mat':'layout_bojarnejad',
           'layout_oosterom.mat':'layout_oosterom'}



#%% Paths 

data_path = os.path.join(root,'Documents','Data')
spat_filt = 0.07
DF_folder = 'DF_CBM_'+str(np.round(spat_filt*100,1))+'p'

DF_results_path = os.path.join(os.path.dirname(data_path),'Results','Models',
                            Layouts[layout],'CBM_R1',DF_folder)

SP_folder = 'SP_CBM' 

#%% Load colormap and geometries

# Colormap
cm_path = os.path.join(root,'Documents','MEGAsync','Master','Matlab','Colormaps')#Colormap

cm = pf.fastMatRead(cm_path,0,'DFColormaps.mat','mycmap')
CM = (cm-cm.min())/(cm.max()-cm.min())*255
CM_new = np.concatenate((CM,np.ones((CM.shape[0],1))*255),axis = 1)
CM_new = np.asarray(CM_new,dtype = np.uint8)
cm = LinearSegmentedColormap.from_list('cm', cm, N=255)

# Geometries

# 2D
coord_2d_path = os.path.join(data_path,'files_journal','Models',Layouts[layout]) 

Cpts  = pf.fastMatRead(coord_2d_path, 0,'Cpts.mat','Cpts' )
xi = pf.fastMatRead(coord_2d_path, 0,'xi.mat','xi' ).flatten()
yi = pf.fastMatRead(coord_2d_path, 0,'yi.mat','yi' ).flatten()

#%%
df_cols = ['layout','subject','arrhythmia','size','density','sps_s']
data_types= dtype=[('layout', 'U20'),('subject', 'U20'), ('arrhythmia', 'U3')
                    ,('size', 'f8'),
                   ('density','f8'),('sps_s','f8')]

DF_heatmaps = pd.DataFrame(columns = df_cols)

#
#'''
################################# Heatmaps (full) ##############################
#
#What I'm doing below is actually plotting the heatmap using all SP location data.
#I want to calculate some parameters to compare the different arrhythmias:
#    - Number of regions (CC analysis, saved in Stats_hm_regions)
#    - Average region size (idem, % of torso)
#    - Average SPs per region (SP_r/(n_reg*s))
#'''
#
#Stats_hmf_regions = {}
#SPs_per_region = {}
#
#col_names = np.array(['Number of regions',
#                      'Min. region size','Mean region size','Std. region size','Max. region size',
##                      'Min. %SP/region','Mean  %SP/region','Max.  %SP/region',
#                      'Min. %SP/area','Mean  %SP/area','Std. %Sps/area','Max.  %SP/area',
#                      'SPs/s'])
#
#HMF_STATS = np.zeros(shape = (len(folder_list),len(col_names)))
#oi_sps = np.zeros((len(folder_list),4))

#%%
'''
####################### Heatmaps (only from filaments) #####################
'''
df_cols = ['layout','subject','arrhythmia','size','density','sps_s']
data_types= dtype=[('layout', 'U20'),('subject', 'U20'), ('arrhythmia', 'U3')
                    ,('size', 'f8'),
                   ('density','f8'),('sps_s','f8')]


DF_heatmaps = pd.DataFrame(columns = df_cols)

col_names = np.array(['Number of regions','Mean region size','Std. region size',
                      'Mean  %SP/area','Std. %Sps/area','SPs/s'])

HML_STATS = np.zeros(shape = (len(folder_list),len(col_names)))
Stats_hml_regions = {}
L_masks = np.zeros(shape = (len(yi),len(xi),len(folder_list)))

oi_sps = np.zeros((len(folder_list),4))

for M,basename in enumerate(folder_list):
    

    isig_path = os.path.join(data_path,'files_journal','Models',
                                Layouts[layout],basename,
                                'raw_signals_CBM_R1',DF_folder)
    
    isig_raw_path = os.path.join(data_path,'files_journal','Models',
                                Layouts[layout],basename,
                                'raw_signals_CBM_R1')    

    isig_raw = pf.fastMatRead(isig_raw_path,0,'isig_'+Layouts[layout]+'.mat','isig')
    sp_path = os.path.join(data_path,'files_journal','Models',
                                Layouts[layout],basename,
                                'raw_signals_CBM_R1',DF_folder,SP_folder)
    
    
    rotors = pf.fastMatRead(sp_path,0,'SP_analysis.mat','rotors')
    r,c,n = rotors.shape
    
    ## Load dicts
    SPs_filt = sio.loadmat(sp_path+'/SPs_filt.mat')
    del SPs_filt['__header__']
    del SPs_filt['__globals__']
    del SPs_filt['__version__']
    
    rotors_fil = pf.dict2matrix(SPs_filt,rotors.shape)
    
    hml = pf.HeatMap(-rotors_fil)
    
    
    hml_b = pf.wrapMap(hml,c)[c:-c,:]
    hml_b[hml_b!=0] = 255; 
    
    hml_b = np.asarray(hml_b,np.uint8)
    # Connected components
    Ncc,mask_L,stats,centroids = cv2.connectedComponentsWithStats(hml_b,
                                                connectivity = 8)
    
    mask = np.zeros_like(mask_L[:,c:-c])
    for i in np.unique(mask_L):
        a,b = np.where(mask_L ==i)
        
        b[b<=c] += c
        b[b>=2*c] -= c
        b[:] -= c
        mask[a,b] = i
    cc_labels = np.unique(mask) # I'm a bit confused now (12/11) as to why this works. but it does
     
    
    stats = stats[cc_labels,:]
    centroids = centroids[cc_labels,:]
    
    Stats_hml_regions[M] = stats
    
    ###########################
    # SPs per region
    sp_r = np.zeros(len(cc_labels)-1)
    for i,L in enumerate(cc_labels[1:]):
        temp_mask = np.copy(mask)
        temp_mask[mask!=L] = 0
        temp_mask[mask == L] = 1
        sp_r[i] = np.nansum(temp_mask*hml)
        if stats[i,-1]==np.nanmax(stats[:,-1]):
            L_masks[:,:,M] = temp_mask
    sp_r = sp_r/np.nansum(sp_r)*100
    ###########################
    # OI   
    
    DF = pf.fastMatRead(isig_path,0,'Maps.mat','DF_1')
    fs = 500
    _,OI = pf.getRIOI(isig_raw,DF,fs,2,20,5)
    
    oi_sps[M,0] = np.nanmean(OI[mask!=0])
    oi_sps[M,1] = np.nanstd(OI[mask!=0])
    oi_sps[M,2] = np.nanmean(OI[mask==0])
    oi_sps[M,3] = np.nanstd(OI[mask==0])
    
    ##########################   
#    ax.boxplot(sp_r*100,whis = [5,95],positions = [M])
    
    HML_STATS[M,0] = len(sp_r) # number of regions
    HML_STATS[M,1] = np.nanmean(stats[1:,-1]/(r*c)*100) # mean area (%)
    HML_STATS[M,2] = np.nanstd(stats[1:,-1]/(r*c)*100 )  # std area (%)
    HML_STATS[M,3] = np.nanmean(sp_r/(stats[1:,-1]))# mean #SPs/point
    HML_STATS[M,4] = np.nanstd(sp_r/(stats[1:,-1]))# std #SPs/point
    HML_STATS[M,5] = np.nansum(hml)/n #  max #SPs/point
    
    ## For all clusters
    df_temp = np.zeros(len(sp_r),dtype =data_types)
    df_temp['layout'] = str(Layouts[layout]) 
    df_temp['subject'] = str(basename) # Isso tem cara de bug, mas o str meio que resolveu
    
    if M<4:
        df_temp['arrhythmia'] = 'AT'
    elif M<8:
        df_temp['arrhythmia'] = 'AFL'
    else:
        df_temp['arrhythmia'] = 'AF'
    
    df_temp['size'] = stats[1:,-1]/(r*c)*100
    df_temp['density'] = sp_r/(stats[1:,-1])
    df_temp['sps_s'] = np.nansum(hml)/n
    
    df_temp =pd.DataFrame(df_temp)
    
    DF_heatmaps = DF_heatmaps.append(df_temp,ignore_index = True)
    

    
DF_HM_fil = df(HML_STATS,columns = col_names,index = folder_list)

DF_HM_fil.to_csv(DF_results_path+'/heatmap_filaments_analysis.csv')
DF_heatmaps.to_csv(DF_results_path+'/all_clusters_filaments.csv')


df(oi_sps,index = folder_list,columns = ['oi_in_mean','oi_in_std',
                                         'oi_out_mean','oi_out_std']).to_csv(DF_results_path+'/OI_heatmaps.csv')
