# -*- coding: utf-8 -*-
"""

Created on Mon Jun  1 09:38:02 2020

Leave one out cross validation for the three machine learning algorithms implemented in the dissertation.
This routine is used to generate the results for the CBM paper, considering only random forests

@author: vgmar
"""

import os 
root = os.path.join('D:\\','vgmar')# expanduser does not work in my pc with 2 HDs
library_folder = os.path.join(root,'Documents','GitHub','BSPM-analysis','Library')
os.chdir(library_folder)
import numpy as np
import pandas as pd
import refined_pf as pf
 

from sklearn.model_selection import LeaveOneOut
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from  sklearn.metrics import balanced_accuracy_score,r2_score,confusion_matrix
#from sklearn.datasets import load_iris

layout_list = ['HR.mat','layout_cardioinsight.mat','layout_leicester_healthy.mat','layout_guillem.mat',
               'layout_salinet.mat','layout_uniform32.mat','layout_uniform16.mat','layout_oosterom.mat']
Layouts = {'HR.mat':'HR',
           'layout_cardioinsight.mat':'layout_cardioinsight',
           'layout_leicester_healthy.mat':'layout_leicester_healthy',
           'layout_guillem.mat':'layout_guillem',
           'layout_salinet.mat':'layout_salinet',
           'layout_uniform32.mat':'layout_uniform32',
           'layout_uniform16.mat':'layout_uniform16',
           'layout_uniform64.mat':'layout_uniform64',
           'layout_bojarnejad.mat':'layout_bojarnejad',
           'layout_oosterom.mat':'layout_oosterom'}

spat_filt = 0.07
DF_folder = 'CBM_R1\\DF_CBM_'+str(np.round(spat_filt*100,1))+'p' 

#just for test
#feature_vector, labels = load_iris(return_X_y=True)

#%% ******************** Arrhythmia classification ********************

for layout in layout_list:
    # Labels
    labels = pf.get_feature('labels',DF_folder,**{'layout':layout})
    labels = labels[:,0] # 0= classification of arrhythmias, 1= classification of driver location
    
    arr_class_output = np.zeros((labels.shape[0],1+4))# labels and 3 classifiers, to save
    arr_class_output[:,0] = labels

    # Generate feature vector
    feature_names = ['mean_1',
                     'HDF_1',
                     'number of regions',
                     'avg. size',
                     'total area',
#                     'range',
                     'IQR_DF',
                     'OI_mean',
                     'IQR_OI',
#                     'fil_displacement_mean',
                      'Filament rate (N fil/s)',
                      'Mean Duration (% duration)',
                      'Number of regions',
#                      'Mean region size',
#                      'Mean  %SP/area',
                      'size',
                      'density',
                      'bounding_box']#,'SPs/s'
    
    feature_vector = pf.get_feature(feature_names,DF_folder,**{'layout':layout})
 
    feature_names = ['mean_1/HDF_1',
                     'number of regions',
                     'avg. size',
                     'total area',#'range'
                     'IQR_DF',
                     'OI_mean',
                     'IQR_OI',
#                     'fil_displacement_mean',
#                      'Filament rate (N fil/s)',
                      'Mean Duration (% duration)',
                      'Number of regions',
#                      'Mean region size',
#                      'Mean  %SP/area',
                      'size',
                      'density',
                      'displacement']#,'SPs/s'
    
    ## Correct features
    ratio_freqs = feature_vector[:,0]/feature_vector[:,1] # to get the actual ratio
    feature_vector = feature_vector[:,1:]; feature_vector[:,0] = ratio_freqs
    
    # Leave one out splits
    loo = LeaveOneOut()
    loo.get_n_splits(feature_vector)
    
    # Least squares classification
    LS_class = LinearRegression(fit_intercept = True, normalize = True)
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        LS_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = LS_class.predict(feature_vector[test_index])[0]
        predicts[predicts>=labels.max()] = labels.max()
        predicts[predicts<=labels.min()] = labels.min()
    arr_class_output[:,1] = np.round(predicts)
    
    # K-nearest neighbors
    k = 3
    knn_class = KNeighborsClassifier(n_neighbors = k,weights = 'distance')
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        knn_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = knn_class.predict(feature_vector[test_index])[0]
    arr_class_output[:,2] = predicts
    
    # Random forests
    rf_class = RandomForestClassifier(n_estimators=40,criterion = 'entropy',
                                       max_depth = 2,bootstrap=False,n_jobs = -1,
                                       max_features='auto',)
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        rf_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = rf_class.predict(feature_vector[test_index])[0]
    arr_class_output[:,3] = predicts
    
    # SVM
    SVM_class = SVC()
    predicts = np.zeros(labels.shape)
    for train_index, test_index in loo.split(feature_vector):
        SVM_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = SVM_class.predict(feature_vector[test_index])[0]
    arr_class_output[:,4] = predicts



    # save .csv
    arr_class_output = pd.DataFrame(arr_class_output,columns = ['labels','LS','KNN','RF','SVM'])
    savepath = os.path.join(root,'Documents','Results', 'Models', Layouts[layout],DF_folder)  
    
    os.chdir(savepath)
#    arr_class_output.to_csv(savepath+'/arr_class_output.csv')
    os.chdir(library_folder)
#%% ******************** Localization classification ********************

for layout in layout_list:
    # Labels
    labels = pf.get_feature('labels',DF_folder,**{'layout':layout})
    labels = labels[:,1] # 0= classification of arrhythmias, 1= classification of driver location
    
    loc_class_output = np.zeros((len(labels),1+4)) # labels and 4 classifiers, to save
    loc_class_output[:,0] = labels

    # Generate feature vector
    feature_names = ['%fdr_front','%fdr_back','%fdr_left','%fdr_right',
#                     '%SP_front','%SP_back','%SP_left','%SP_right',
                     '%SP_area_front','%SP_area_back','%SP_area_left','%SP_area_right',
                     'OI_front','OI_back','OI_left','OI_right',
                     ]
    
    feature_vector = pf.get_feature(feature_names,DF_folder,**{'layout':layout})
    
    # Leave one out splits
    loo = LeaveOneOut()
    loo.get_n_splits(feature_vector)
    
    # Least squares classification
    LS_class = LinearRegression(fit_intercept = True, normalize = True)
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        LS_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = LS_class.predict(feature_vector[test_index])[0]
        predicts[predicts>=labels.max()] = labels.max()
        predicts[predicts<=labels.min()] = labels.min()
    loc_class_output[:,1] = np.round(predicts)
    
    # K-nearest neighbors
    k = 3
    knn_class = KNeighborsClassifier(n_neighbors = k,weights = 'distance')
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        knn_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = knn_class.predict(feature_vector[test_index])[0]
    loc_class_output[:,2] = np.round(predicts)
    
    # Random forests
    rf_class = RandomForestClassifier(n_estimators=40,criterion = 'entropy',
                                       max_depth = 2,bootstrap=False,n_jobs = -1,
                                       max_features='auto',)
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        rf_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = rf_class.predict(feature_vector[test_index])[0]
    loc_class_output[:,3] = np.round(predicts)

    # SVM
    SVM_class = SVC()
    predicts = np.zeros(labels.shape)
    for train_index, test_index in loo.split(feature_vector):
        SVM_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = SVM_class.predict(feature_vector[test_index])[0]
    loc_class_output[:,4] = predicts
    

    # save .csv
    loc_class_output = pd.DataFrame(loc_class_output,columns = ['labels','LS','KNN','RF','SVM'])
    savepath = os.path.join(root,'Documents','Results', 'Models', Layouts[layout],DF_folder)  
    
    os.chdir(savepath)
    loc_class_output.to_csv(savepath+'/loc_class_output.csv')
    os.chdir(library_folder)
    
#%% EXTRA: classification: rotational x focal
    
for layout in layout_list:

    # Labels
    labels = pf.get_feature('labels',DF_folder,**{'layout':layout})
    labels = labels[:,0] # 0= classification of arrhythmias, 1= classification of driver location
    labels[labels==2] = 1
    
    focal_vs_reentrant_class_output = np.zeros((labels.shape[0],1+3))# labels and 3 classifiers, to save
    focal_vs_reentrant_class_output[:,0] = labels

    
    # Generate feature vector
    feature_names = ['mean_1',
                     'HDF_1',
                     'number of regions',
                     'avg. size',
                     'total area',#'range'
                     'IQR_DF',
                     'OI_mean',
                     'IQR_OI',
#                     'fil_displacement_mean',
#                      'Filament rate (N fil/s)',
                      'Mean Duration (% duration)',
                      'Number of regions',
#                      'Mean region size',
#                      'Mean  %SP/area',
                      'size',
                      'density',
                      'bounding_box']#,'SPs/s'
    
    feature_vector = pf.get_feature(feature_names,DF_folder,**{'layout':layout})
 
    feature_names = ['mean_1/HDF_1',
                     'number of regions',
                     'avg. size',
                     'total area',#'range'
                     'IQR_DF',
                     'OI_mean',
                     'IQR_OI',
#                     'fil_displacement_mean',
#                      'Filament rate (N fil/s)',
                      'Mean Duration (% duration)',
                      'Number of regions',
#                      'Mean region size',
#                      'Mean  %SP/area',
                      'size',
                      'density',
                      'bounding_box']#,'SPs/s'
    
    ## Correct features
    ratio_freqs = feature_vector[:,0]/feature_vector[:,1] # to get the actual ratio
    feature_vector = feature_vector[:,1:]; feature_vector[:,0] = ratio_freqs
    
    
    # Leave one out splits
    loo = LeaveOneOut()
    loo.get_n_splits(feature_vector)
    
    # Least squares classification
    LS_class = LinearRegression(fit_intercept = True, normalize = True)
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        LS_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = LS_class.predict(feature_vector[test_index])[0]
        predicts[predicts>=labels.max()] = labels.max()
        predicts[predicts<=labels.min()] = labels.min()
    focal_vs_reentrant_class_output[:,1] = np.round(predicts)
    
    # K-nearest neighbors
    k = 3
    knn_class = KNeighborsClassifier(n_neighbors = k,weights = 'distance')
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        knn_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = knn_class.predict(feature_vector[test_index])[0]
    focal_vs_reentrant_class_output[:,2] = np.round(predicts)
    
    # Random forests
    rf_class = RandomForestClassifier(n_estimators=40,criterion = 'entropy',
                                       max_depth = 2,bootstrap=False,n_jobs = -1,
                                       max_features='auto',)
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        rf_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = rf_class.predict(feature_vector[test_index])[0]
    focal_vs_reentrant_class_output[:,3] = np.round(predicts)


    # save .csv
    focal_vs_reentrant_class_output = pd.DataFrame(focal_vs_reentrant_class_output,columns = ['labels','LS','KNN','RF'])
    savepath = os.path.join(root,'Documents','Results', 'Models', Layouts[layout],DF_folder)  
    
    os.chdir(savepath)
    focal_vs_reentrant_class_output.to_csv(savepath+'/focal_vs_reentrant_class_output.csv')
    os.chdir(library_folder)
    
#%% Layered classification
    
    
for layout in layout_list:
    # Stage 1: AF x AFL and AT

    ## Labels
    labels = pf.get_feature('labels',DF_folder,**{'layout':layout})
    labels = labels[:,0] # 0= classification of arrhythmias, 1= classification of driver location

    arr_class_output = np.zeros((labels.shape[0],1+4))# labels and 3 classifiers, to save
    arr_class_output[:,0] = labels
    
    labels[labels==1] =0 ; labels[labels==2]=1 # AFL grouped with AT, AF alone
    
     

    ## Generate feature vector
    feature_names = ['mean_1',
                     'HDF_1',
                     'number of regions',
                     'avg. size',
#                     'total area',
                     'range',
#                     'IQR_DF',
                     'OI_mean',
                     'IQR_OI',
#                     'fil_displacement_mean',
                      'Filament rate (N fil/s)',
                      'Mean Duration (% duration)',
#                      'Std. Duration (% Duration)',
                      'Number of regions',
#                      'Std. region size',
#                      'Mean region size',
#                      'Mean  %SP/area',
#                      'std. size',
                      'size',
                      'density',
                      'bounding_box']#,'SPs/s'
    
    feature_vector = pf.get_feature(feature_names,DF_folder,**{'layout':layout})
    
     ## Correct features
    ratio_freqs = feature_vector[:,0]/feature_vector[:,1] # to get the actual ratio
    feature_vector = feature_vector[:,1:]; feature_vector[:,0] = ratio_freqs
    
    feature_names = ['mean_1/HDF_1',
                     'number of regions',
                     'avg. size',
                     'total area',
                     'range'
#                     'IQR_DF',
                     'OI_mean',
                     'IQR_OI',
                     'fil_displacement_mean',
                      'Filament rate (N fil/s)',
                      'Mean Duration (% duration)',
#                      'Std. Duration (% Duration)',
                      'Number of regions',
#                      'Mean region size',
                      'Mean  %SP/area',
                      'size',
                      'density',
                      'bounding_box']#,'SPs/s'
    

    
    ## Leave one out splits
    loo = LeaveOneOut()
    loo.get_n_splits(feature_vector)
    
    ## Least squares classification
    LS_class = LinearRegression(fit_intercept = True, normalize = True)
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        LS_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = LS_class.predict(feature_vector[test_index])[0]
        predicts[predicts>=labels.max()] = labels.max()
        predicts[predicts<=labels.min()] = labels.min()
    arr_class_output[:,1] = np.round(predicts)
    
    ## K-nearest neighbors
    k = 3
    knn_class = KNeighborsClassifier(n_neighbors = k,weights = 'distance')
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        knn_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = knn_class.predict(feature_vector[test_index])[0]
    arr_class_output[:,2] = predicts
    
    ## Random forests
    rf_class = RandomForestClassifier(n_estimators=40,criterion = 'entropy',
                                       max_depth = 2,bootstrap=False,n_jobs = -1,
                                       max_features='auto',)
    predicts = np.zeros(labels.shape)
    
    for train_index, test_index in loo.split(feature_vector):
        rf_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = rf_class.predict(feature_vector[test_index])[0]
    arr_class_output[:,3] = predicts


    ## SVM
    SVM_class = SVC()
    predicts = np.zeros(labels.shape)
    for train_index, test_index in loo.split(feature_vector):
        SVM_class.fit(feature_vector[train_index], labels[train_index])
        predicts[test_index] = SVM_class.predict(feature_vector[test_index])[0]
    arr_class_output[:,4] = predicts


    # Stage 2: AT x AFL
    
    arr_class_output[:,1:][arr_class_output[:,1:]==1] = 2
    feature_vector_train = feature_vector[:8] # true AT and AFL
    labels = pf.get_feature('labels',DF_folder,**{'layout':layout})[:8,0]

    
    # Least squares classification
    ind = np.where(arr_class_output[:,1]==0)[0] # only AT and AFL, as classified   

    LS_class = LinearRegression(fit_intercept = True, normalize = True)
    predicts = []
    
    for i in ind:
        if i<8:
            LS_class.fit(np.concatenate([feature_vector_train[:i],feature_vector_train[(i+1):]]),
                        np.concatenate([labels[:i],labels[(i+1):]]))
        else:
            LS_class.fit(feature_vector_train,labels)   
            
        predicts.append(LS_class.predict(feature_vector[i,:].reshape(1,-1))[0])
    predicts = np.asarray(predicts)
    predicts[predicts>=labels.max()] = labels.max()
    predicts[predicts<=labels.min()] = labels.min()
    
    arr_class_output[ind,1] = np.round(predicts)
    
    # K-nearest neighbors
    ind = np.where(arr_class_output[:,2]==0)[0]
    k = 3
    knn_class = KNeighborsClassifier(n_neighbors = k,weights = 'distance')
    predicts = []
    
    for i in ind:
        if i<8:
            knn_class.fit(np.concatenate([feature_vector_train[:i],feature_vector_train[(i+1):]]),
                        np.concatenate([labels[:i],labels[(i+1):]]))
        else:
            knn_class.fit(feature_vector_train,labels)  
        predicts.append(knn_class.predict(feature_vector[i,:].reshape(1,-1))[0])
    predicts = np.asarray(predicts)
    arr_class_output[ind,2] = predicts
    
    #Random forests
    ind = np.where(arr_class_output[:,3]==0)[0]    
    rf_class = RandomForestClassifier(n_estimators=40,criterion = 'entropy',
                                       max_depth = 2,bootstrap=False,n_jobs = -1,
                                       max_features='auto',)
    predicts = []
    
    for i in ind:
        if i<8:
            rf_class.fit(np.concatenate([feature_vector_train[:i],feature_vector_train[(i+1):]]),
                        np.concatenate([labels[:i],labels[(i+1):]]))
        else:
            rf_class.fit(feature_vector_train,labels)  
        predicts.append(rf_class.predict(feature_vector[i,:].reshape(1,-1))[0])
    predicts = np.asarray(predicts)

    arr_class_output[ind,3] = predicts

    ## SVM
    ind = np.where(arr_class_output[:,4]==0)[0]
    
    
    SVM_class = SVC()
    predicts = []
    
    for i in ind:
        if i<8:
            SVM_class.fit(np.concatenate([feature_vector_train[:i],feature_vector_train[(i+1):]]),
                        np.concatenate([labels[:i],labels[(i+1):]]))
        else:
            SVM_class.fit(feature_vector_train,labels)  
        predicts.append(SVM_class.predict(feature_vector[i,:].reshape(1,-1))[0])
    predicts = np.asarray(predicts)
    arr_class_output[ind,4] = predicts


    # save .csv
    arr_class_output = pd.DataFrame(arr_class_output,columns = ['labels','LS','KNN','RF','SVM'])

    savepath = os.path.join(root,'Documents','Results', 'Models', Layouts[layout],DF_folder)  
    
    os.chdir(savepath)
    arr_class_output.to_csv(savepath+'/arr_class_output_new.csv')
    os.chdir(library_folder)
    
    
