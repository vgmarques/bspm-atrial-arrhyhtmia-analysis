# -*- coding: utf-8 -*-
"""
Created on Fri Dec 11 10:05:29 2020

Dummy script for showing the way around the functions used to get the features

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

# Python libraries
import os
import numpy as np
import scipy.io as sio

# Paths
ScriptsFolder = os.getcwd()
base = os.path.dirname(ScriptsFolder)

# BSPM libraries
os.chdir(os.path.join(base,'freq_analysis'))
import frequencyAnalysis_lib as fqa
os.chdir(os.path.join(base,'accessory'))
import accessory_lib as acc
os.chdir(os.path.join(base,'fileIO'))
import fileIO_lib as fio
os.chdir(os.path.join(base,'feature_extraction'))
import featureExtraction_lib as fext


os.chdir(base)
#%% Load data

# Iterate over models and over quadrant
fs = 500 # Hz
#Frequency
load_path = 'D:\\vgmar\Documents\Data\\files_journal\Models\HR\AF_LA_09\DF Analysis (welch_wtmm)' #change, naturally
DFMap = fio.FastMatRead(load_path, 0,'Maps.mat','DF_1')
OIMap = fio.FastMatRead(load_path, 0,'Maps.mat','OI_1')

#Phase
load_path = 'D:\\vgmar\Documents\Data\\files_journal\Models\HR\AF_LA_09\DF Analysis (welch_wtmm)\SP_analysis(150120,HR,wavelet)'

spMaps =  fio.FastMatRead(load_path,0,'SP_analysis.mat','rotors')
edges =  fio.FastMatRead(load_path,0,'SP_analysis.mat','HMG')
filamentDurationsList =  fio.FastMatRead(load_path,0,'SP_analysis.mat','SPs_dur')

filamentPositionsDict = sio.loadmat(load_path+'/SPs_filt.mat')
del filamentPositionsDict['__header__']
del filamentPositionsDict['__globals__']
del filamentPositionsDict['__version__']
fs_low = 128 #Hz

load_path = os.path.join('D:\\vgmar\Documents\Data','files_journal','Models','HR','AF_LA_09','raw_signals_CBM_R1')
xi =  fio.FastMatRead(load_path, 0,'xi.mat', 'xi').flatten()
yi =  fio.FastMatRead(load_path, 0,'yi.mat', 'yi').flatten()
#%% Frequency Features
HDF1 = 4.8

ra_list = []
la_list = ['AF_LA_09']
basename = 'AF_LA_09'

Output = fext.DFMapFeatures(DFMap,HDF1,ra_list = ra_list,la_list = la_list,basename = basename)

#%% Phase features

'''
A large portion of the features are already calculated when the phase maps are 
generated. 
This will be tricky with the different quadrants. In order to do that, you need
to run the SP_detection.py individually for each quadrant. This function takes
a significant time to run for the whole map. 

The other functions are organized below
'''

GeneralOutput,IndividualOutput = fext.FilamentFeatures(spMaps,filamentDurationsList,filamentPositionsDict,edges,fs_low) # Please double check this function because the division of the dicts is confusing. sorry about that

HMOutput,OIOutput = fext.HMFeatures(spMaps,filamentPositionsDict,OI = OIMap)

#%% Extra features
len_xi = len(xi)
len_yi = len(yi)

## Front and back indexes (Ideally follow the commented strategy, I made a lazy workaround to avoid loading too much stuff)
lims_front = [np.where(xi<=0)[0][0],np.where(xi<=0)[0][-1]+1] #The +1 is for the indexing in python
lims_back = [np.where(xi>0)[0][0],np.where(xi>0)[0][-1]+1]
 
## left and right indexes
lims_left = [int(len_xi/4),int(3*len_xi/4)] #
lims_right = [0,int(len_xi/4),int(3*len_xi/4),len_xi] #Needs "two halves"
limits = [lims_front,lims_back,lims_left,lims_right]

FreqExtraOutputDict,PhaseExtraOutputDict = fext.MixedFeatures(limits,DFMap,OIMap,HDF1,spMaps,filamentPositionsDict,xi,yi)
