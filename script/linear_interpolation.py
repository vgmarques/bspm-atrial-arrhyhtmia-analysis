#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 17:03:58 2021.

@author: italo
"""
import scipy.io as sio

import numpy as np
import open3d as o3d
from scipy.interpolate import griddata
from time import time
import oct2py
oc = oct2py.Oct2Py()
import trimesh as trm
from datetime import datetime
import os

def get_connectivity(triangles, autoindex=True):
    """Get 1st and 2nd order connectivity matrix from triangles."""
    size = triangles.max() + 1
    connectivity = np.zeros((size, size), bool)
    for i, j, k in triangles:
        connectivity[i,j] = True
        connectivity[i,k] = True
        connectivity[j,i] = True
        connectivity[k,i] = True
        connectivity[j,k] = True
        connectivity[k,j] = True
    
    if autoindex:
        connectivity[np.arange(size), np.arange(size)] = True

    connectivity2 = connectivity.copy()
    for i in range(size):
        connectivity2[i] = connectivity[connectivity[i],:].sum(axis=0).astype(bool)
    
    if not autoindex:
        connectivity2[np.arange(size), np.arange(size)] = False
    
    return connectivity, connectivity2

def seg2hms(seconds):
    """Convert time in seconds to string in format ...h:mm:ss ."""
    return (str(int(np.floor(seconds/(60*60)))) + ":" 
            + ('00' + str( int(np.floor((seconds%(60*60))/60))))[-2:] + ":"
            + ('00' + str(int(np.floor(seconds%60))))[-2:])


def visualizer(mesh, rsteps=[(0,0)]):
    """Visualizer with custom field of view."""
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    vis.add_geometry(mesh)
    opt = vis.get_render_option()
    opt.mesh_show_wireframe = True
    ctr = vis.get_view_control()
    for rstep in rsteps:
        ctr.rotate(x=rstep[0], y=rstep[1])
        
    # print("Field of view (after changing) %.2f" % ctr.get_field_of_view())
    vis.run()
    vis.destroy_window()
    
#%%
# ===========Torso===========

# HR_vert = oc.load('/media/italo/HDD/SignalsInterpolation/layouts_and_geometry/Torso.mat')['Torso']['vertices']
# HR_face = oc.load('/media/italo/HDD/SignalsInterpolation/layouts_and_geometry/Torso.mat')['Torso']['faces'] -1


# HR_mesh = trm.Trimesh(HR_vert, HR_face, process=False)



# # scene = vedo.Plotter(interactive=True)
# # scene += vedo.Arrows(HR_vert, HR_vert - np.asarray(HR_norms)*5)
# # scene += vedo.Mesh((HR_vert, HR_face), c='w').lw(2).lc('black')

# # scene.show().close()

# idx_mat = oc.load('/media/italo/HDD/SignalsInterpolation/lower_resolution_indexes.mat')

# layouts = ['indexes_cardioinsight_249',
#             'indexes_leicester_131',
#             'indexes_guillem_68',
#             'indexes_salinet_62',
#             'indexes_uniform_32',
#             'indexes_uniform_16',
#             ]


# data_path = '/media/italo/HDD/SignalsInterpolation/oringinal_torso'



# cam = dict(pos=(-130.3, -91.80, 85.91),
#             focalPoint=(1.066, 3.934, 20.07),
#             viewup=(0.3592, 0.1415, 0.9225),
#             distance=175.4,
#             clippingRange=(91.18, 281.8))

# folder = 'linear_torso'
# ===========================

# ===========Atria===========

idx_mat = oc.load('/media/italo/HDD/SignalsInterpolation/atria_LR_indexes.mat')

HR_vert = oc.load('/media/italo/HDD/SignalsInterpolation/atrial_model.mat')['atrial_model']['vertices']
HR_face = oc.load('/media/italo/HDD/SignalsInterpolation/atrial_model.mat')['atrial_model']['faces'] -1
# HR_mesh = trm.Trimesh(HR_vert, HR_face, process=False)
# HR_mesh = trm.smoothing.filter_laplacian(HR_mesh, lamb=0.8, iterations = 100, volume_constraint=True)
# HR_vert = HR_mesh.vertices

layouts = [
            'indexes_geometry_16_16',
            'indexes_geometry_32_32',
            'indexes_geometry_64_64',
            'indexes_geometry_128_128',
            ]

data_path = '/media/italo/HDD/SignalsInterpolation/original_atria'

cam = dict(pos=(17.69, -16.55, 59.66),
            focalPoint=(-2.749, 7.926, 34.52),
            viewup=(-0.4614, 0.4184, 0.7823),
            distance=40.61,
            clippingRange=(19.53, 67.25))

folder = 'linear_atria'

# ===========================

verbose = 1
show = True


for layout in layouts:
    tic = time()
    print('========', layout, '========\n')
    HR_mesh = trm.Trimesh(HR_vert, HR_face, process=False)
    LR_idx = idx_mat[layout].ravel().astype(int) -1
    # LR_idx = np.unique(LR_idx)
    LR_vert = HR_vert[LR_idx]
    run1time = False
    lenghts = [0]
    nomes = []
    HR_signals = np.empty((HR_vert.shape[0], 0))
    
    print('Loading signals', end='')
    
    for root, dirs, files in os.walk(data_path):
        for file in files:
            HR_signals_file = oc.load(os.path.join(root, file))
            for key in HR_signals_file:
                HR_signals_i = HR_signals_file[key]
            
                lenghts.append(lenghts[-1] + HR_signals_i.shape[1])
                nomes.append(key)
                HR_signals = np.append(HR_signals, HR_signals_i, axis=1)
                print('.', end='')
            
    print(' Concluded')
    
    LR_signals = HR_signals[LR_idx.ravel()]

    nsignals = np.ones((HR_vert.shape[0], LR_signals.shape[1]))*np.nan

    tic = time()
    verbose = 1
    theresnan = True
    
    HR_vert_i = HR_vert.copy()
    while theresnan:
                
        nsignals_i = griddata(LR_vert, LR_signals, xi=HR_vert_i, method='linear')
        
        where = (np.isnan(nsignals[:,0]))*(~np.isnan(nsignals_i[:,0]))
        nsignals[where, :] = nsignals_i[where,:]   
        # Torso lamb=0.8
        HR_mesh = trm.smoothing.filter_laplacian(HR_mesh, lamb=0.8, iterations = 1, volume_constraint=False)
        HR_vert_i = HR_mesh.vertices
        
        # scene = vedo.Plotter(interactive=True)
        # scene += vedo.Mesh((HR_vert_i, HR_face), c='w').lw(2).lc('black')
        # scene += vedo.Points(LR_vert, r=13, c='r')
        # scene += vedo.Points(HR_vert_i[np.isnan(nsignals[:,0])], r=10, c='g')
        # scene.show(camera=cam).close()
        
        theresnan = np.isnan(nsignals[:,0]).sum() > 0

    
    print('General Mean Quadratic Error:', format(((HR_signals - nsignals)**2).mean()/(HR_signals**2).mean() * 100, '.2f'), '%')
    if verbose>0: print('Saving', end='')
    for i, nome in enumerate(nomes):
        nsignals_i = nsignals[:, lenghts[i]:lenghts[i+1]]
        
        # print(nome, 'Mean Quadratic Error:', format(((HR_signals[:, lenghts[i]:lenghts[i+1]] - nsignals_i)**2).mean()/
        #                                             (HR_signals[:, lenghts[i]:lenghts[i+1]]**2).mean() * 100, '.2f'), '%')
        
        date = bytes(datetime.now().strftime('%a %b %d %H:%M:%S %Y'), encoding='utf-8')
        saving_file = dict([['__header__', b'MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: ' + date],
                            ['__version__', '1.0'],
                            ['__globals__', []],
                            ['interpolated_signals', nsignals_i],
                            ['HR_geometry', {'vertices':HR_vert,
                                             'faces':HR_face+1}],
                            ['LR_geometry', LR_vert],
                            ['LR_HR_index', LR_idx+1],
                            ])
        if not os.path.exists(os.path.join(os.path.dirname(root),folder)):
            os.mkdir(os.path.join(os.path.dirname(root),folder))
        
        sio.savemat(os.path.join(os.path.dirname(root),
                                 folder, nome + '_' + layout + '.mat'), 
                    saving_file)
        
    
    if verbose>0: print('Concluded. Elapsed:', seg2hms(time()-tic))

#%%
# for layout in reversed(layouts):
#     print('========', layout, '========\n')

#     HR_signals = np.empty((HR_vert.shape[0], 0))
#     nsignals = np.empty((HR_vert.shape[0], 0))
#     lenghts = [0]
#     print('Loading signals', end='')
#     for root, dirs, files in os.walk(data_path):
#         for file in files:
#             lenghts.append(lenghts[-1] + HR_signals_i.shape[1])
#             HR_signals_i = oc.load(os.path.join(root, file))
#             HR_signals_i = HR_signals_i[[key for key in  HR_signals_i][0]]
#             HR_signals = np.append(HR_signals, HR_signals_i, axis=1)
            
#             nsignals_i = oc.load(os.path.join(os.path.dirname(root), 'linear2', file[:-14] + '_' + layout + '.mat'))['interpolated_signals']
#             nsignals = np.append(nsignals, nsignals_i, axis=1)
#             print(file, 'Mean Quadratic Error:', format(((HR_signals_i - nsignals_i)**2).mean()/
#                                                         (HR_signals_i**2).mean() * 100, '.2f'), '%') 
           
            
        
    
#     print('General Mean Quadratic Error:', format(((HR_signals - nsignals)**2).mean()/(HR_signals**2).mean() * 100, '.2f'), '%')   
#     # mat = sio.loadmat('/mnt/e/atrial_interpolation/torso/interpolated_torso_signals.mat')
    