# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 14:28:52 2020

Functions for calculating dominant frequencies, organization indexes and associate tasks

@author: Victor G. Marques (v.goncalvesmarques@maastrichtuniversity.nl)

"""

#%% Python libraries
import sys
import os
import numpy as np
import pywt
import scipy.signal as sig
import math

#%% Other dependencies
root = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(root,'accessory'))
sys.path.append(os.path.join(root,'preprocessing'))

from detect_peaks import detect_peaks
import preprocessing_lib as prep

#%% DF analysis
    
def waveletActivation(signals,fs,initial_frequency,final_frequency,nscales = 10):
                      
    '''
    This function detects activations using wavelet transform modulus maxima and
    determining the activation time by the mean of the maxima along selected scales
    
    Parameters:
        - signals: data (ndim = 1,2 or 3)
        - fs: sampling frequency (Hz)
        - initial_frequency: initial frequency in the range you want to analyze (Hz)
        - final_frequency: final frequency in the range you want to analyze (Hz)
        These values should be select to encompass the frequency content of the signal
        
        - nscales: number of scales in which the maxima should be detected
        
    Fixed parameters:
        - wav: gaus1
        - distance to be considered in the same activation: 40 ms
    '''
    
    
    wav = pywt.ContinuousWavelet('gaus1')
    scales = (np.linspace(initial_frequency,final_frequency,nscales)/fs*1/pywt.scale2frequency(wav,1))**-1 # Equivalent to selecting frequencies between 1 and 100
    # The scales are not dyadic
    
    #### Adjust dimensions
    original_dim = signals.ndim
    
    original_shape = signals.shape

    signals = prep.adjustDimSignal(signals,2)
    
    ### Activation detection       
    activation_matrix = np.zeros_like(signals)
    
    for i in range(signals.shape[0]):
        cwt,freqs = pywt.cwt(signals[i,:],scales,wav,1/fs)
        
        #To avoid double counting peaks
        b,a = sig.butter(5,20/(fs/2),'low')
        cwt = sig.filtfilt(b,a,cwt)
        
        pk_dict = {}
        k = 0 
        prev_list = []
        allocate_dict = {}

        for s in range(0,len(scales)):#range(1,scales.shape[0]+1):
            pks = detect_peaks(cwt[s,:],edge='rising',mpd =int(fs/final_frequency))
            if s == 0:
                for p in pks:
                    pk_dict[k] = []
                    pk_dict[k].append(p)
                    allocate_dict[k] = []
                    k += 1
                prev_list = pks
            else:
                # Adds to a temporary dict so that we select only 1 pt per scale
                for p in pks:
                    k_loc = np.where(np.abs(p-prev_list)<int(fs*40e-3))[0]#fs/final_frequency
                    if len(k_loc)!=0:
                        allocate_dict[k_loc[0]].append(p)
                # takes the median from each entry in the temp. dict to represent the line
                for p in allocate_dict: 
                    if len(allocate_dict[p])!=0:
                        pk_dict[p].append(int(np.median(allocate_dict[p])))
                        allocate_dict[p] = []

        activations = []
        for d in pk_dict:
            activations.append(int(np.mean(pk_dict[d]))) 
        activation_matrix[i,activations] = 1      
    
    #### Adjust dimensions back
    
    signals = prep.adjustDimSignal(signals,original_dim,original_shape)
    activation_matrix = prep.adjustDimSignal(activation_matrix,0,original_shape)
        
    return activation_matrix
    
def DF_by_Wavelet(isig,fs,slope = 1):
    '''
    This function estimates the activation times of a BSPM with fixed parameters.
    The only option is regarding the polarity of the wave considered as activation.
    
    For the qualification exam, the polarity used was positive (slope = 1) and 
    the parameters were selected to match the frequency content of atrial arrhythmias:
    
    Fixed parameters:
        - initial_frequency: 3.125 Hz
        - final_frequency: 12.5 Hz
        - nscales: 5
    '''
    
    
    r,c,nF = isig.shape
    DFp = np.zeros((r,c))
    
    initial_frequency = 3#3.125
    final_frequency = 20#12.5
    nscales = 5
    if slope ==1:
        activation = waveletActivation(-isig,fs,initial_frequency,final_frequency,nscales)
    if slope ==-1:
        activation = waveletActivation(isig,fs,initial_frequency,final_frequency,nscales)
        
    
    DFp = np.mean(activation,axis = -1)*fs

    CL = 1/DFp
    
    return DFp,CL

####

def getRIOI(signals,DF_peaks,fs,fmin,fmax,max_harm=5,bandwidth=1):
    
    '''
    This function calculates the regularity and the organization indexes. I use
    a window around a peak for that end rather than finding valleys that surround
    this peak.
    
    Parameters:
        - signals: signals ndim = 1, 2 or 3
        - DF_peaks: dominant frequencies wrt. which the indexes are calculated
        - bandwidth: bandwidth around peaks
        - fs: sampling frequency (Hz)
        - fmin, fmax: limits of the spectrum
        - max_harm: maximum number of harmonics for OI
    '''
            
     #### Adjust dimensions
    original_dim = signals.ndim
    original_shape = signals.shape
    original_shape_df = DF_peaks.shape
    signals = prep.adjustDimSignal(signals,2)
    DF_peaks = prep.adjustDimSignal(DF_peaks,2)
       
    OI = np.zeros_like(DF_peaks)    
    
    ### Calculate spectra
    nseg = 2.7*fs #int(isig.shape[-1]/5) # Number of points per segment  
    if nseg>signals.shape[-1]:
        nseg = int(0.9*signals.shape[-1])
    nfft = 2**(math.ceil(math.log(nseg,2)))*2

    freqs_welch,f_welch = sig.welch(signals,fs,nperseg = nseg,
                                    nfft = nfft,noverlap = int(0.9*nseg),
                                    axis = -1)


    fstep = freqs_welch[1]
    
    bandwidth = int(bandwidth/fstep)
    
    total_area = np.sum(f_welch[:,int(fmin/fstep):int(fmax/fstep)],axis = -1)


    Index = np.max([DF_peaks.shape[-1],signals.shape[0]])

    
    ### Calculate RI
    RI = np.zeros(DF_peaks.shape[-1])    
    for i in range(Index):
    
        if DF_peaks.shape[-1]!=signals.shape[0]:
            DF_ind = int(DF_peaks[0,i]/fstep)
            I = 0
        else:
            DF_ind = int(DF_peaks[i]/fstep)
            I = i
            
        peak_area = np.sum(f_welch[I,DF_ind-bandwidth:DF_ind+bandwidth+1])
        RI[i] = peak_area/total_area[I]

    ### Calculate OI
    
    OI = np.zeros(DF_peaks.shape[-1])    
    for i in range(Index):


                        
        if DF_peaks.shape[-1]!=signals.shape[0]:
            DF_ind = int(DF_peaks[0,i]/fstep)
            I = 0
            nHarm = int(fmax/DF_peaks[0,i])
            if nHarm>max_harm:
                nHarm = max_harm
        else:
            DF_ind = int(DF_peaks[i]/fstep)
            I = i
            try:
                nHarm = int(fmax/DF_peaks[i])
            except:
                nHarm = 0
            if nHarm>max_harm:
                nHarm = max_harm
        
        for k in range(1, nHarm+1):
            
            Harm_ind = int(k*DF_ind) 
            
            # Check if it has gone over the frequency borders, cutting the windows to remain inside its limits
            #obs: this causes windows to be smaller in boundary conditions.
            
            if Harm_ind+bandwidth>fmax/fstep:
                OI[i] += np.sum(f_welch[I, Harm_ind-bandwidth:int(fmax/fstep)]) #gone above fmax
           
            elif Harm_ind-bandwidth<fmin/fstep:
                OI[i] += np.sum(f_welch[I, int(fmin/fstep):Harm_ind+bandwidth]) #gone below fmin
            
            else:                    
                OI[i] += np.sum(f_welch[I, Harm_ind-bandwidth:Harm_ind +bandwidth+1]) # in range
        
        OI[i] = OI[i]/total_area[I]  

    #### Adjust back dimensions
    signals = prep.adjustDimSignal(signals,original_dim,original_shape)
    RI = prep.adjustDimSignal(RI,0,original_shape_df)
    OI = prep.adjustDimSignal(OI,0,original_shape_df)
    

    return RI,OI

####

def completeDF_BSPM(isig,fs,fmin,fmax,df_range = [2,12],det_method = 1,
                    band_oi = 1,filter_option = 0,thresh_oi = 15,spat_filt = 0.02,
                    wav_lims = [3,30]): #TODO: this function needs serious improvement
    """
    This function encompasses all of the implemented DF analysis so far:
        - DF is determined using welch periodograms (1.5 s segments, 50% overlap)
        - If the option is chosen, activation rates estimated via wavelet are 
        used to complement this analysis.
        - If the option is chosen, OI can be used to complement the previous 
        analysis
        - RI and OI are calculated and filters can be implemented to aid HDF and
        mode determination
        
    Parameters:
        - isig: signal in 3 dimensions, time is the -1 axis
        - fs: sampling frequency in Hz
        - fmin and fmax: limit frequencies for filtering
        - df_range: limits for DF determination
        - det_method: chooses method for determining DF:
            0 - only Welch
            1 - Welch + Activation
            2 - Welch + OI + Activation
            3 - Welch + RI + Activation
            4 - Wavelet energy method (only)
            5 - Wavelet energy and Welch
        - band_oi: frequency band around peaks for RI and OI determination
        - filter_option: chooses method for masking frequencies for HDF and mode
        determination:
            0 - no filter
            1 - harmonic filter (outdated)
            2 - fixed threshold OI filter
            3 - spatial filter
        - thresh_oi: threshold for OI filter above. If [0,1), the threshold is 
        fix , if [1,+inf) it is a percentile
    
    """
    
    # thresh_oi can be given either as OI value [0,1] or percentile ]1,100]
    l,c,n = isig.shape

    # downsample,  LP @ lower freq., upsample and subtract
    isig_proc = sig.resample(isig,int(n/40),axis = -1) #downsample to fs/40
    
    try:
        b,a = sig.butter(10,fmin/((fs/40)/2),'low')
        isig_proc = sig.filtfilt(b,a,isig_proc,axis = -1)

    except:
        b,a = sig.butter(4,fmin/((fs/40)/2),'low')
        isig_proc = sig.filtfilt(b,a,isig_proc,axis = -1)

        print('Padlen error')
    

    isig_proc = sig.resample(isig_proc,n,axis = -1)
    isig_no_baseline = isig-isig_proc
    
    # Than LP at highest frequency
    try:
        b,a = sig.butter(10,fmin/((fs/40)/2),'low')
    except:
        b,a = sig.butter(4,fmin/((fs/40)/2),'low')
        print('Padlen error')
    
    isig_filt = sig.filtfilt(b,a,isig_no_baseline,axis = -1)

    
    ## Welch periodogram, DF map
    nseg = int(2.7*fs) #int(n/5) # Number of points per segment
    if nseg>n:
        nseg = 0.9*n
    
    nfft = fs/0.1#2**(math.ceil(math.log(nseg,2)))*2 # Next power of 2 based on length

    f_welch = np.zeros(shape = (l,c,int(nfft/2)+1))
    
    DF_peaks = np.zeros(shape = (l,c))    
    fstep = (fs/2)/(nfft/2+1) # approximately
    fmin_samples = int(np.ceil(df_range[0]/fstep))
    fmax_samples = int(np.ceil(df_range[1]/fstep))
    
    number_peaks = 5
    ## Add conditional for using activation and OI
    if det_method ==0: #Only Welch
        print('Frequency resolution: %.2f'%(fs/nseg))
        for i in range(l):
            for j in range(c):
                # Detect if the vector is all nan and skip
                if np.sum(np.isnan(isig_filt[i,j,:]))==len(isig_filt[i,j,:]):
                    DF_peaks[i,j] = np.nan
                    continue
                
                # detect all peaks, select by activation
                freqs_welch,f_welch[i,j,:] = sig.welch(isig_filt[i,j,:],
                                   fs,nperseg = nseg,nfft = nfft,
                                   noverlap = int(nseg*.9))
                fstep = freqs_welch[1]
                DF_1 = np.where(f_welch[i,j,fmin_samples:fmax_samples]==np.max(f_welch[i,j,fmin_samples:fmax_samples]))[0]
                DF_peaks[i,j] = freqs_welch[DF_1+fmin_samples]
        
    elif det_method ==1 or det_method==5: #Welch + Activation
        print('Frequency resolution: %.2f'%(fs/nseg))

        ## Activation count, pre-DF map
#        
        if det_method==1:
            DFp,CL = DF_by_Wavelet(isig,fs,slope = 1)#DF_dVdt(isig_filt,fs,df_range[1])#DF_by_zc(isig_filt,fs,df_range[1])#*******        
        elif det_method==5:
            DFp = energy_WTMM(isig,fs,nscales = 40,lims = wav_lims)

        for i in range(l):
            for j in range(c):
                # Detect if the vector is all nan and skip 
                if np.sum(np.isnan(isig_filt[i,j,:]))==len(isig_filt[i,j,:]):
                    DF_peaks[i,j] = np.nan
                    continue
                
                freqs_welch,f_welch[i,j,:] = sig.welch(isig_filt[i,j,:],
                                                       fs,nperseg = nseg,nfft = nfft,
                                                       noverlap = int(nseg*.9))
                
                ind = detect_peaks(f_welch[i,j,:])
                ind[ind<fmin_samples] = 0
                ind[ind>fmax_samples] =0
                ind = ind[ind!=0]
        
                if len(ind)!=0:
                    ampls = f_welch[i,j,ind] # get the amplitudes
                    ind = np.asarray([x for _,x in sorted(zip(ampls,ind))]) # sort by amplitude

                    ind = np.flipud(ind)[:number_peaks] # number_peaks highest peaks
                        
                    dist = np.abs(freqs_welch[ind]-DFp[i,j])
                    df_peak = np.where(dist==np.min(dist))[0][0]
                    DF_peaks[i,j] = freqs_welch[int(ind[df_peak])]
        
#                        
#                
#                # detect N largest peaks, select by activation
#
#                
#                freq_peaks = detect_peaks(f_welch[i,j,fmin_samples:fmax_samples],
#                                           edge = 'rising',mph = 0.1*np.max(f_welch[i,j,:]))
#                if len(freq_peaks)==0:
#                    freq_peaks = detect_peaks(f_welch[i,j,fmin_samples:fmax_samples],
#                                      edge = 'rising',mph = 0.01*np.max(f_welch[i,j,:]))
#                DFs = freqs_welch[freq_peaks+fmin_samples]
#                act_dist = np.abs(DFp[i,j]-DFs)
#                DF_peaks[i,j] = DFs[np.where(act_dist==np.nanmin(act_dist))]
      

    elif det_method == 2 or det_method ==3: # Welch + OI + Activation
        print('Frequency resolution: %.2f'%(fs/nseg))
        
        DFp,CL = DF_by_Wavelet(isig,fs,slope = 1)#DF_dVdt(isig_filt,fs,df_range[1])#DF_by_zc(isig_filt,fs,df_range[1])#*******
        harm_ratios = np.log2([1/8,.25,1/3,.5,2,3,4,5])
        
        for i in range(l):
            for j in range(c):
                # Detect if the vector is all nan and skip
                if np.sum(np.isnan(isig_filt[i,j,:]))==len(isig_filt[i,j,:]):
                    DF_peaks[i,j] = np.nan
                    continue
                DF_w_OI = []
                
                # detect all peaks, select by activation
                freqs_welch,f_welch[i,j,:] = sig.welch(isig_filt[i,j,:],
                                   fs,nperseg = nseg,nfft = nfft,
                                   noverlap = int(nseg*.9))
                fstep = freqs_welch[1]
                
                
                freq_peaks = detect_peaks(f_welch[i,j,fmin_samples:fmax_samples],
                                          edge = 'rising',mph = 0.1*np.max(f_welch[i,j,:]))
                if len(freq_peaks)==0:
                    freq_peaks = detect_peaks(f_welch[i,j,fmin_samples:fmax_samples],
                                          edge = 'rising')#,mph = 0.05*np.max(f_welch[i,j,:]))
                
                DFs = freqs_welch[freq_peaks+fmin_samples]
                # Find harmonics
                Harm_dict = {}
                for ii,f1 in enumerate(DFs):
                    for jj,f2 in enumerate(DFs[ii+1:]):
                        ratio = np.log2(f1/f2)
                        if np.sum(np.abs(ratio-harm_ratios)<.2)>=1: 
                            Harm_dict[f1] = np.asarray([f1,f2])
                    if f1 not in Harm_dict and (ii+1!=len(DFs) or len(DFs)==1): # last number
                        DF_w_OI.append(f1)
                        
                #Calculate OI for each pair in the dicts
                
                for k in Harm_dict:
                    RI, OI = getRIOI(isig_filt[i,j],Harm_dict[k],fs,fmin,fmax,5,band_oi)
                    if det_method==2:
                        OI = OI.flatten()
                        DF_w_OI.append(Harm_dict[k][np.where(OI==np.nanmax(OI))][0])
                    elif det_method == 3:
                        RI = RI.flatten()
                        DF_w_OI.append(Harm_dict[k][np.where(RI==np.nanmax(RI))][0])
                        
#                DF_w_OI = np.unique(DF_w_OI)
                # Distance from avg. activation frequency
                act_dist = np.abs(DFp[i,j]-DF_w_OI) #ACHEI um bug
                
                DF_peaks[i,j] = DF_w_OI[np.where(act_dist==np.min(act_dist))[0][0]]
    elif det_method == 4:
        DF_peaks = energy_WTMM(isig,fs,nscales = 40,return_act = False,lims = wav_lims)
#    
    ## RI, OI
    RI,OI = getRIOI(isig_filt,DF_peaks,fs,fmin,fmax,5,band_oi)
    
    if filter_option == 0 or filter_option =='NO_FILTER':
        dfs, counts = np.unique(DF_peaks[~np.isnan(DF_peaks)], return_counts=True)
#        if np.isnan(dfs)==np.prod(dfs.shape): 
#            HDF = -1
#        else:
        ind = np.where(counts==np.max(counts))
        moda = dfs[ind][0]
            
        HDF = np.nanmax(DF_peaks)
    
    elif filter_option == 1 or filter_option =='OI_THRESH':
        if thresh_oi>1:
            thresh_oi = np.percentile(OI.flatten(),thresh_oi)
        
        oi_mask = np.copy(OI)
        oi_mask[oi_mask>=thresh_oi] = 1
        oi_mask[oi_mask<thresh_oi] = np.nan
        
        # Moda e frequência mais alta
    
        dfs, counts = np.unique(DF_peaks, return_counts=True)
        ind = np.where(counts==np.max(counts))
        moda = dfs[ind][0]
        
        HDF = np.nanmax(DF_peaks*oi_mask)
        
    elif filter_option == 2 or filter_option =='NO_HARM':
        dfs, counts = np.unique(DF_peaks[~np.isnan(DF_peaks)], return_counts=True)
        if np.isnan(dfs)==np.prod(dfs.shape): 
            HDF = -1
        else:      
            sum_true = np.zeros(shape = dfs.shape)
            ind = np.where(counts==np.max(counts))
            moda = dfs[ind][0]
            for i,f in enumerate(dfs):
                ratio = np.unique(np.round(dfs/f,decimals = 1))
                sum_true[i] = np.sum(np.log2(ratio[ratio<=1])- np.round(np.log2(ratio[ratio<=1]))==0) # the "<=1" if to look only for subharmonics!
            
            HDF = 0
            for i,f in enumerate(dfs[sum_true<=1]): # Get only the frequencies wo. subharm.
                if f>HDF:
                    HDF = f
                    
    elif filter_option==3 or filter_option == 'SPATIAL':
        dfs = np.sort(DF_peaks[~np.isnan(DF_peaks)].flatten())
        HDF = np.nanmax(dfs[:int(len(dfs)*(1-spat_filt))]) ## Ignore (spat_filt)% highest frequency values
        dfs, counts = np.unique(DF_peaks, return_counts=True)
        ind = np.where(counts==np.max(counts))
        moda = dfs[ind][0]

    return DF_peaks,moda,HDF,RI,OI,isig_filt

####

def energy_WTMM(data,fs,nscales = 100,return_act = False,lims = [2,30]):
    '''
    This method uses the same gaussian wavelets from the WTMM I presented at the qualification exam.
    However, I optimized the scales to use based on our models. This needs to be tested in other 
    data sets, e.g. the patients, to see if it holds.
    
    Also, these were not tested for EGMs! (I believe it would work similarly though)
    
    I fixed the following options from the last routine:
        - No choice for scale (obviously);
        - It always detects the positive slopes;
    
    The _data_ variable must not be filtered.
    '''
    #### Define scales (somewhere around 34) and wavelet
#    S = np.arange(10,40,2)#*fs/500 # because the design was made for 500 Hz sampling frequency
    
    wav = pywt.ContinuousWavelet('gaus1')
    S = (np.linspace(lims[0],lims[1],nscales)/fs*1/pywt.scale2frequency(wav,1))**-1 # Equivalent to selecting frequencies between 1 and 100
#    S = 2**np.arange(10)

    #### Adjust dimensions
    original_dim = data.ndim
    if original_dim==3:
        r,c,n = data.shape
        data = np.reshape(data,(r*c,n))
    elif original_dim == 1:
        data = np.reshape(data,(1,len(data)))
             
    #### Wavelet portion
    freq_estimates = -1*np.ones(data.shape[0])  
    act_matrix = np.zeros_like(data)
    for i in range(0,data.shape[0]):
        ### Perform the CWT
        cwt,_ = pywt.cwt(-data[i,:],scales = S, wavelet = wav)
        
        energy = [np.sum(np.abs(cwt[j,:])**2) for j in range(len(S))]
        ind = np.where(energy==np.max(energy))[0]#detect_peaks(energy)#
         
        if len(ind)!=0:
            ind = ind[0]
#            smooth_cwt = sig.medfilt(cwt[ind,:],7)
            b,a = sig.butter(10,30/(fs/2),'low');
            smooth_cwt = sig.filtfilt(b,a,cwt[ind,:].flatten())
            
            pk_ind = detect_peaks(smooth_cwt.flatten(),edge = 'rising',
                                  #mph = 0.05*np.max(cwt[ind,:]),
                                  mpd = int(50*1e-3*fs))
           
            act_matrix[i,pk_ind] = 1
            
            freq_estimates[i] = len(pk_ind)/(data.shape[-1]/fs) #np.nanmean(1/np.diff(pk_ind/fs))#mean instantaneous freq.   #
    #### Adjust dimensions
    if original_dim==3:
        data = np.reshape(data,(r,c,n))
        act_matrix = np.reshape(act_matrix,(r,c,n))
        freq_estimates = np.reshape(freq_estimates,(r,c))
    elif original_dim == 1:
        data = data.flatten()
        act_matrix = act_matrix.flatten()
        freq_estimates = freq_estimates.flatten()
        
        
    if return_act:
        return freq_estimates,act_matrix
    else:
        return freq_estimates

####