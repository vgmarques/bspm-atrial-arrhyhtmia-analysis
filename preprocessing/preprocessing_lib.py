# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 14:42:23 2020

Preprocessing functions

@author: vgmar
"""
#%% Python libraries
import numpy as np
from scipy.interpolate import griddata


#%% Other dependencies



#%% Dimension manipulation

def adjustDimSignal(signals,ndims,final_dims = ()):
    '''
    This function reshapes the data to 2D if it is 3D or 1D or does the opposite
    based on ndims
    
    ndims !=2 makes the function simply follow final_dims
    '''    
    if ndims==2:
        
        if signals.ndim==3:
            r,c,n = signals.shape
            signals = np.reshape(signals,(r*c,n))
        elif signals.ndim == 1:
            signals = np.reshape(signals,(1,len(signals)))
        else:
            signals = signals.flatten()
            
    else:
        signals = np.reshape(signals,final_dims)
        
        
    return signals

####

def dict2matrix(Dict,matrix_shape):
    out_matrix = np.zeros(matrix_shape) 
    
    for sp in Dict:
        temp_sp = np.asarray(Dict[sp])
        temp_sp = temp_sp[temp_sp[:,2].argsort()] 
        active_frames = np.unique(temp_sp[:,2])
        
        for i,f in enumerate(active_frames):
            xa,ya,za = (np.mean(temp_sp[temp_sp[:,2]==f],axis = 0))
            out_matrix[int(xa),int(ya),int(za)] = 1
            
    return out_matrix

####

def cylinderProjection(points,view,R = 0,center = None):
    # Projects the points of a irregular 3D geometry into a 
    # cylinder of center corresponding to the baycenter of the 
    # points composing the 3D geometry and radius equal to the
    # greatest distance to this point.
    
    # Parameters:
        # points: location of each point in the 3D geometry
        # view: offset used to select the point which will 
        #       stay at the center of the image when the 
        #       cylinder is opened to a rectangle
        #       The following points are of special interest 
        #       for BSPM applications:
                #   - 0 = left side 
                #   - pi/2 = back
                #   - pi = right side
                #   - 3*pi/2 = front 
    
    # Returns: (M,2) array containing the 2D coordinates of the M points
    
    # Obs: better to remove points on upper and lower parts of image
    # In case of the models from Miguel, that means points 32 and 109 and after 737

    X = np.asarray(points[:,0])
    Y = np.asarray(points[:,1])
    Z = np.asarray(points[:,2])
    Npts = X.shape[0]
    # Get center of shape
    
    
    # Center shape
    if center is None:
        cX = np.mean(X)
        cY = np.mean(Y)
    else:
        cX = center[0]
        cY = center[1]
    
    X -= cX
    Y -= cY
    # Get outmost radio
    if R==0:
        R = np.sqrt(pow(X,2)+ pow(Y,2));
        R0 = R.max()
    else:
        R0 = R
    # Get angles wrt. this cylinder in each level
    theta = np.arctan2(Y,X)

    
    # Convert angles to a X axis coordinate
    L = np.zeros(shape = (Npts))
    for i in range(0,Npts):
        L[i] = R0*(theta[i])#**-view
    # Adjust according to selected view
    val_left = L.min()
    val_right = L.max()
    rang = np.abs(val_left)+np.abs(val_right)
       
    for i in range(0,Npts):
        L[i] -= R0*(view)
        if L[i]<val_left:
           L[i] += rang
        elif L[i]>val_right:
           L[i] -= rang
        
        
    Cpts = np.asarray([L,Z]).T

    return Cpts,R0

####

def wrapMap(image,margin):
    '''
    This function wraps the maps, so that they are treated as a cylinder rather
    than a rectangle.
    '''
    
    if image.ndim==2:
        w,h = image.shape
        mapOut = np.zeros((w+2*margin,h+2*margin))
        # Copy Center
        mapOut[margin:-margin,margin:-margin] = image
        # Wrap borders
        mapOut[margin:-margin,:margin] = image[:,-margin:]
        mapOut[margin:-margin,-margin:] = image[:,:margin]
    elif image.ndim==3:
        w,h,f = image.shape
        mapOut = np.zeros((w,h+2*margin,f)) 
        # Copy Center
        mapOut[:,margin:-margin,:] = image
        # Wrap borders
        mapOut[:,:margin,:] = image[:,-margin:,:]
        mapOut[:,-margin:,:] = image[:,:margin,:]
    
    
    return mapOut

####

def InterpolateAndReorganize(signals, meas_points,R0, nCols,
                             nLines,xlim = [],ylim = []): 
    """
    Reviewed: 12/12/2018
    Important changes relative to the function available in the rotor_detection library
    - d was ignored, instead I use the difference between the cylinder size and the 
    electrode positions to correct the distances between electrodes
    - Removed the d/2 correction when wrapping the projected points
    - temporary x axis' length was configured to be always proportional to the 
    number of the columns in the portion of the torso that was wrapped
    """
    
    frames = signals.shape[-1]  
     
    
    
    # 1. Sets the new axes, for being used after interpolation
    if len(xlim)==0:
        xmin, xmax = meas_points[:, 0].min(), meas_points[:, 0].max()
    else:
        xmin = xlim[0]; xmax = xlim[1]
        
    difference_in_radius = 2*np.pi*R0-(xmax-xmin)
    
    # 2. Gets the original data and wraps it, make several temporary variables
    
    portion_to_wrap = 1#0.2 # model parameter, percentage TODO: only works with 1
    
    d  = 2*R0*np.pi # largura do torso (TODO: check if this can be improved)
    
    Wbeg_ind = np.where(meas_points[:, 0]>xmax-d*portion_to_wrap)[0] #indexes of the electrodes being mirroed to the beginning, only x
    Wend_ind = np.where(meas_points[:, 0]<xmin+d*portion_to_wrap)[0]# indexes of the electrodes being mirroed to the end, only x
    
    #Allocate space for electrodes to be appended
    Wbeg = np.zeros(shape = (np.size(Wbeg_ind), 2))
    Wend = np.zeros(shape = (np.size(Wend_ind), 2))
    
    # Get the x values from the original measurement points, than, based on that, sets the new distance appended to the ends

    Wbeg[:, 0] = meas_points[Wbeg_ind, 0]-(d+difference_in_radius)
    Wbeg[:, 1] = meas_points[Wbeg_ind, 1]

    Wend[:, 0] = meas_points[Wend_ind, 0]+(d+difference_in_radius)
    Wend[:, 1] = meas_points[Wend_ind, 1]

    # puts new "imaginary" electrodes at the end
    temp_nodes = np.append(meas_points, Wbeg, 0)
    temp_nodes = np.append(temp_nodes, Wend, 0)

    # Make a temporary signal to allow for the interpolation
    temp_signal = np.zeros(shape = (signals.shape[0]+np.size(Wbeg_ind)+np.size(Wend_ind), signals.shape[1]))
    last_sig_ind = signals.shape[0]
    temp_signal[0:last_sig_ind, :] = signals

    temp_signal[last_sig_ind:last_sig_ind+np.size(Wbeg_ind), :] = signals[Wbeg_ind, :]
    
    last_sig_ind = last_sig_ind+np.size(Wbeg_ind)
    temp_signal[last_sig_ind:last_sig_ind+np.size(Wend_ind), :] = signals[Wend_ind, :]

    # Temporary Axes
    
    temp_lines = int(nLines) #todo: improve these names
    temp_cols = int(nCols*(1+2*portion_to_wrap)) #


    linspace_Wbeg = np.linspace(Wbeg[:,0].min(),Wbeg[:,0].max(),int(nCols*portion_to_wrap))
    linspace_meas = np.linspace(-np.pi*R0,np.pi*R0,nCols)
    linspace_Wend = np.linspace(Wend[:,0].min(),Wend[:,0].max(),int(nCols*portion_to_wrap))
    
    xi_temp = np.append(linspace_Wbeg,linspace_meas)
    xi_temp = np.append(xi_temp,linspace_Wend)
    
    if len(ylim)==0:
        yi = np.linspace(temp_nodes[:, 1].min(),temp_nodes[:, 1].max(),temp_lines)
    else:
        yi = np.linspace(ylim[0],ylim[1],temp_lines)

    # 3. Loop over each time instant and interpolate, storing the data in a 3D matrix
   
    i_signals_temp = np.zeros(shape = (temp_lines, temp_cols, frames))
    for i in range(0, frames):
        i_signals_temp[:, :, i]  = griddata((temp_nodes[:, 0], temp_nodes[:, 1]), temp_signal[:, i], 
        (xi_temp[None,:], yi[:,None]), method='cubic')
   
   # 4. Removes "wrapped" part to remain with only the original electrodes
    down_lim_x = int(portion_to_wrap*nCols) #TODO see how the difference in radius affects this
    up_lim_x = int(2*portion_to_wrap*nCols) 
    i_signals = i_signals_temp[:,down_lim_x:-down_lim_x, :]
    
    # make axes for possible use in other functions
    xi = xi_temp[down_lim_x:up_lim_x]

    return  i_signals, xi, yi

####

def electrode_interp_v2(data,X,Y,Horz,Vert):
    ### Wrap the points and generate new position and data files
    
    ## Get the spacing between electrodes
    avg_hspace = np.mean(np.diff(np.unique(X))) 
    
    perimeter = X.max()-X.min()
    
    ## Generate the arrays to be appended
    X_end = X+(perimeter+avg_hspace)
    X_beg = X-(perimeter+avg_hspace)
    
    ## Generate single vectors
    
    x_axis = np.append(np.append(X_beg,X),X_end)
    y_axis = np.append(np.append(Y,Y),Y)
    
    expanded_data = np.append(np.append(data,data,axis = 0),data,axis = 0)
    
    ### Make an axis with the number of points we want
    xi_temp = np.linspace(x_axis.min(),x_axis.max(),3*Horz)
    yi = np.linspace(y_axis.min(),y_axis.max(),Vert)
    xx,yy = np.meshgrid(xi_temp,yi)
    
    ### Interpolate
    int_signals = np.zeros((len(yi),len(xi_temp),data.shape[-1])) 
    
    for i in range(int_signals.shape[-1]):
        temp_signal = griddata(np.array([x_axis,y_axis]).T, expanded_data[:, i], 
                                       np.array([xx.flatten(),yy.flatten()]).T, method='cubic')
        int_signals[:,:,i] = np.reshape(temp_signal,(Vert,3*Horz))
    
    ### Get only the center, generate interpolated indexes
    
    final_signal = int_signals[:,Horz:2*Horz]
    xi = xi_temp[Horz:2*Horz]

    return final_signal,xi,yi

####

