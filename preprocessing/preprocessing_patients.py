# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 10:15:28 2020

@author: vgmarques
"""

from __future__ import print_function

import os
root_folder = os.path.join(os.path.expanduser('~'),'Documents','MEGAsync',
                              'Master','Python','files_journal')
os.chdir(root_folder)
import scipy.signal as sig
import numpy as np
import refined_pf as pf
import scipy.io as sio
from scipy.interpolate import griddata
import matplotlib.pyplot as plt


save_folder = os.path.join(os.path.expanduser('~'),'Documents','Data','files_journal','Patients (corrected_dims)')

folder_list = ['090623_1975743DVV','090901_10994500JCF','090902_1949357AMG_remap','091007__FPR',
'091222_1874079_GLB','100205_TIM_1551135_n2','100212_AGRA_1970271','100323_ATG_2022604',
'100407_MGR_1555397',#100505_1832765_ASE',
'100513_M062891_JDM','100615_2079456','100922_2045839_AAB',#'110308_SFS'
]

filepath = os.path.join(os.path.expanduser('~'),'Documents','MEGAsync',
                              'Master','BSPM Signals','models_Miguel')
data_path = os.path.join(os.path.expanduser('~'),'Documents','Data')

layout = ''# '' ou '_32' ou '_16'

#%%
for M, basename in enumerate(folder_list):    
    
    load_path = os.path.join(data_path,'CircAE',basename)
    
    
    Data  = pf.fastMatRead(load_path, 2,'datos.mat','ECG_ADE',
                               struct_name = 'registros', inner_struct ='ECG')
    
    fs = pf.fastMatRead(load_path, 2,'datos.mat','fs',
                               struct_name = 'registros', inner_struct ='ECG')
    
    Data = Data[:-1,:int(4*fs)] # 4 seconds segments
    l,N = Data.shape         
    
    pos_electrodes = pf.fastMatRead(load_path, 2,'datos.mat','poselectrodes',
                                    struct_name = 'registros', inner_struct ='ECG')
    
    inter_electrode_dist = 5 #cm
    #%%
    X = (pos_electrodes[0,0]['X']-1).squeeze() # Matlab
    Y = (pos_electrodes[0,0]['Y']-1).squeeze()
    X *= inter_electrode_dist; Y *= inter_electrode_dist; 
    
    Xmax = X.max()+inter_electrode_dist/2
    Xmin = X.min()-inter_electrode_dist/2
    perimeter = Xmax-Xmin
    
    if layout == '_32':
        indexes = [0,11,27,38,53,5,23,34,43,60,2,9,15,24,35,44,51,57,7,25,36,45,62,4,13,16,31,46,49,55,64,65]
        X = X[indexes]
        Y = Y[indexes]
        Data = Data[indexes]
    
    elif layout == '_16':
        indexes = [0,27,53,23,43,2,15,35,51,7,36,62,13,31,49,64]
        X = X[indexes]
        Y = Y[indexes] 
        Data = Data[indexes]
    
    

    
    
    ### Rotation 
    clock_rot = -np.pi/2 #counter-clockwise direction
    
    final_angle = clock_rot+(2*np.pi)# Angle in which the right portion...
        
    
    
    theta = (final_angle-clock_rot)*(X-Xmin)/(Xmax-Xmin)+clock_rot
    
    #(final_angle-clock_rot)*(X-(X.min()-inter_electrode_dist))/ \
    #        inter_electrode_dist((X.max()+inter_electrode_dist)- \
    #         (X.min()-inter_electrode_dist))+clock_rot
    
    theta [theta>np.pi] -= (2*np.pi)
    
    theta = pf.norm_2vals(theta,[-perimeter/2,perimeter/2])
    
    Cpts = np.asarray([theta,Y]).T
    
    
    # Interpolation
    
    nCols = 65
    nLines = 30
    
      
    # 1. Sets the new axes, for being used after interpolation
    
    
    # 2. Gets the original meas points and wraps it
    
    extended_meas = np.zeros((Cpts.shape[0]*3,Cpts.shape[1]))
    
    extended_meas[0:Cpts.shape[0],:] = np.asarray([Cpts[:,0]-perimeter,Cpts[:,1]]).T
    extended_meas[Cpts.shape[0]:2*Cpts.shape[0],:] = Cpts
    extended_meas[2*Cpts.shape[0]:,:] = np.asarray([Cpts[:,0]+perimeter,Cpts[:,1]]).T
    
    # 3. Gets data points and extend the array to match extended_meas
    
    extended_sig =  np.zeros((Cpts.shape[0]*3,N))
    
    extended_sig[0:Cpts.shape[0],:] = Data
    extended_sig[Cpts.shape[0]:2*Cpts.shape[0],:] = Data
    extended_sig[2*Cpts.shape[0]:,:] = Data
    
    # 4. Generate interpolation axis, interpolate for each frame
    xi_temp = np.linspace(extended_meas[:,0].min(),extended_meas[:,0].max(),3*nCols)
    
    xi_temp = np.append(np.linspace(-3/2*perimeter,-perimeter/2,nCols),
                         np.linspace(-perimeter/2,perimeter/2,nCols))
    xi_temp = np.append(xi_temp,
                         np.linspace(perimeter/2,3/2*perimeter,nCols))
    
    yi = np.linspace(extended_meas[:,1].min(),extended_meas[:,1].max(),nLines) 
    
    i_signals_extended = np.zeros(shape = (nLines, 3*nCols, N))
    for i in range(0, N):
        i_signals_extended[:, :, i]  = griddata((extended_meas[:, 0], extended_meas[:, 1]), extended_sig[:, i], 
        (xi_temp[None,:], yi[:,None]), method='cubic')
    
    # 5. Remove wrapped portion
        
    i_signals = i_signals_extended[:,nCols:2*nCols, :]
    xi = xi_temp[nCols:2*nCols]
    

    savepath = os.path.join(save_folder,basename)
    
    if not os.path.exists(savepath):
        os.makedirs(savepath)    
    os.chdir(savepath)
    
    os.chdir(savepath)
    
    sio.savemat('isig_pat'+layout+'.mat',{'isig':i_signals})
    os.chdir('..')
    sio.savemat('Cpts'+layout+'.mat',{'Cpts':Cpts})
    sio.savemat('xi'+layout+'.mat',{'xi':xi})
    sio.savemat('yi'+layout+'.mat',{'yi':yi})
    
    print(basename+' is ready')
    
    os.chdir(root_folder)

