# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 14:40:06 2020

Accessory functions for the other routines

@author: vgmar
"""

#%% Python libraries
import numpy as np
import math

#%% Other dependencies




#%%
def corr2(x,y):
    '''
    2D pearson's correlation coefficient
    '''
    if x.shape!=y.shape:
        print('Error: x and y must have the same shape!')
        return np.nan
    
    xm = x-np.mean(x)
    ym = y-np.mean(y)
    
    r = (xm*ym).sum() / math.sqrt((xm*xm).sum() * (ym*ym).sum())
    
    return r

####

def norm_2vals(signal,interval= [-1,1]):
    norm_signal = (interval[1]-interval[0])*(signal-np.min(signal))/(np.max(signal)-np.min(signal))+interval[0]

    return norm_signal

####
def dict2matrix(Dict,matrix_shape):
    out_matrix = np.zeros(matrix_shape) 
    
    for sp in Dict:
        temp_sp = np.asarray(Dict[sp])
        temp_sp = temp_sp[temp_sp[:,2].argsort()] 
        active_frames = np.unique(temp_sp[:,2])
        
        for i,f in enumerate(active_frames):
            xa,ya,za = (np.mean(temp_sp[temp_sp[:,2]==f],axis = 0))
            out_matrix[int(xa),int(ya),int(za)] = 1
            
    return out_matrix


####


