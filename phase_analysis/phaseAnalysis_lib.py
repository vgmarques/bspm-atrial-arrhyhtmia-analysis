# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 14:28:52 2020

Functions for calculating phase maps, singularity points, filaments and heatmaps

@author: Victor G. Marques (v.goncalvesmarques@maastrichtuniversity.nl)

"""
#%% Python libraries
import sys
import os
import numpy as np
import scipy.signal as sig
import cv2
from sklearn.linear_model import LinearRegression

#%% Other dependencies
root = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(root,'accessory'))
sys.path.append(os.path.join(root,'preprocessing'))

from detect_peaks import detect_peaks
import preprocessing_lib as prep


#%% Phase calculation

def instPhase(signals):
    '''
    Returns the instantaneous phase of a signal based on its Hilbert transform
    # "signal" is a 2D matrix where each time instant is a column.
    # Returns: matrix of same size containing the instantaneous phases
    '''
    
    inst_phase = np.zeros(shape = signals.shape)
    for i in range(0, signals.shape[0]):
        xa = sig.hilbert(signals[i, :])
        xi = np.imag(xa)
        xr = signals [i,:] 
        inst_phase[i,:] = np.arctan2(xi, xr)  #!
             
    return inst_phase

####

def narrowBandHilbert(signals, filter_freq, fs,bandwidth = 1,n_filt = 4,cut = 200):
    
    '''
    Performs the Hilbert transform after filtering around a bandwidth of a chosen DF
    
    Parameters:
        - signals: signals
        - filter_freq: frequency around which the signals are filtered (Hz)
        - fs: sampling frequency (Hz)
        - bandwidth: bandwidth of the filter  (Hz), default= 1
        - n_filt: filter order
        - cut: portion to remove from begin and end of signal (ms)
    '''  
    
    #Adjust dimensions
    original_dim = signals.ndim
    original_shape = signals.shape
    signals = prep.adjustDimSignal(signals,2)
    
    if filter_freq==-1:
        # Obtain phase via Hilbert transform
        phase = instPhase(signals)
    else:
        b, a  = sig.butter(n_filt, [(filter_freq-bandwidth)/(fs/2),
                               (filter_freq+bandwidth)/(fs/2)],
                               'bandpass', analog=False)
        # Obtain phase via Hilbert transform
        phase = instPhase(sig.filtfilt(b,a,signals))
    
    cut = int(cut*fs*1e-3)
    if cut!=0:
        video =  phase[:,cut:-cut] 
    else:
        video =  phase
    
    #Adjust back the dimensions
    video = prep.adjustDimSignal(video,original_dim,(original_shape[0],
                                                    original_shape[1],
                                                    original_shape[2]-2*cut))

    return video

#%% SP detection

def skeleton_endpoints(skel):
    '''
    Custom endpoint detection based on an edge image
    '''
    # make out input nice, possibly necessary
    skel = skel.copy()
    skel[skel!=0] = 1
    skel = np.uint8(skel)
    
    src_depth = -1

    kernel = np.uint8([[1,  1, 1],
                       [1, 10, 1],
                       [1,  1, 1]])
    
    # Up
    tUR_kernel = np.uint8([[1,  1, 1],
                          [1, 10, 1],
                          [1,  7,7]]) #!!*
    tUL_kernel = np.uint8([[1,  1, 1],
                          [1, 10, 1],
                          [7,  7,1]]) #!!*
    #Down
    tDR_kernel = np.uint8([[1,  7, 7],
                          [1, 10, 1],
                          [1,  1,1]]) #!!*
    tDL_kernel = np.uint8([[7,  7, 1],
                          [1, 10, 1],
                          [1,  1,1]]) #!!*
    #Left
    tLU_kernel = np.uint8([[1,  1,1],
                          [7, 10, 1],
                          [7,  1,1]]) #!!*
    tLD_kernel = np.uint8([[7,1, 1],
                          [7, 10, 1],
                          [1,  1,1]]) #!!*
    #Right
    tRU_kernel = np.uint8([[1,  1, 1],
                          [1, 10, 7],
                          [1,  1,7]]) #!!*
    tRD_kernel = np.uint8([[1,1, 7],
                          [1, 10, 7],
                          [1,  1,1]]) #!!*
    
    Filtered  = np.zeros(shape = (skel.shape[0],skel.shape[1],9) ) 
    kernel_dict = {0:kernel,1:tUR_kernel,2:tUL_kernel,3:tDR_kernel,4:tDL_kernel,
                  5:tLU_kernel,6:tLD_kernel,7:tRU_kernel,8:tRD_kernel}
    
    
    for K in range(9):
        Filtered[:,:,K] = cv2.filter2D(skel,src_depth,kernel_dict[K])
        if K==0:
            Filtered[Filtered[:,:,K]!=11,K] = 0
            Filtered[Filtered[:,:,K]==11,K] = 1
        else:
            Filtered[Filtered[:,:,K]!=24,K] = 0
            Filtered[Filtered[:,:,K]==24,K] = 1            
    
    all_SP = np.sum(Filtered,axis = 2)
    all_SP[all_SP!=1]==0
    
    return all_SP 

####

def EP_Analysis(image,th = [500,700],dilate = True,kernel_size = 3):
    '''
    Detects endpoints from edges detected by Canny's algorithm
    
    Parameters:
        - image: colored or grayscale image
        - th: Cannys thresholds
        - dilate: if True, return the dilated endpoint
        - kernel_size: for dilation
    '''
    
    # Normalize between 0 and 2*pi
    pmax = np.max(image)
    pmin = np.min(image)
    mapN = (image-pmin)*2*np.pi/(pmax-pmin)

    mapN_canny = np.uint8(mapN/(2*np.pi)*255)

    #Detect edges and endpoints
    
    # step 3: Edge detection                   
    MG = cv2.Canny(mapN_canny,th[0],th[1]) #todo: These thresholds are very important for the success of the detection
    
    mapEP = skeleton_endpoints(MG) # Find endpoints
    ep_nodil = np.asarray(np.where(mapEP!=0)).T 
    

    
    if dilate:
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                           (kernel_size,kernel_size))
        
        mapEP = cv2.dilate(mapEP,kernel)
        ep_dil = np.asarray(np.where(mapEP!=0)).T
        
        return ep_dil,ep_nodil,MG
    
    else: 
        
        return ep_nodil,MG

####

def local_interpolation(signal,xi,yi,center,radius,ppc = 12,k = 8):
    '''
    Obtains interpolated values in positions determined by a circle around a
    central point.
    
    This function interpolates locally, obtaining values with finer positions
    than the spacing of the grid points. This allows for investigating the phase
    progression in any radius around a SP candidate
    
    Parameters:
        - signal: the signal to interpolate, either time or phase. Tested only for 3D data
        - xi and yi: axis related to the position of the signals. Allows for conversion
        between indexes and cm
        - center: the central point around which the signals are being analyzed. In
        phase analysis, it is a SP candidate. Index, not cm!!!
        - radius: radius of the circle around center to interpolate the signals. In cm.
        - ppc: points per circle. Determines how fine should the interpolation be
        - n_neighbors: the number of neighbors of each point used for interpolating the data
    Returns:
        - interpolated signal
        - position of the circle
        
    TODO: how to deal with upper and lower borders? I can't wrap in this case
    currently it gets the closest neighbors and tries to interpolate even outside of the map
    '''

    NNeighbors = k # Por algum motivo inexplicável, não consigo passar esse argumento    
    # =========================================================================
    #      Wrap signal if needed

    
    # See how much wrapping is needed
    def_x = (xi[1]-xi[0])
    margin = int(np.ceil(radius/def_x))
    
    #Wrap signal
    if len(signal.shape)==3:
        wrap_signal = prep.wrapMap(signal,margin)
        int_sigs = np.zeros((ppc,signal.shape[-1]))
    else:
        wrap_signal = prep.wrapMap(signal,margin)
        int_sigs = np.zeros(ppc)
   
    #extend xi for calculating distances only
    stretch_xi = np.linspace(def_x,(margin)*def_x,margin)
    
    wrap_xi = np.zeros((xi.shape[0]+2*(margin)));
    wrap_xi[margin:-margin] = xi
    wrap_xi[:margin] = xi[0]-np.flipud(stretch_xi); 
    wrap_xi[-margin:] = xi[-1]+stretch_xi;
    
    # Correct center
    center[1] += margin
    # =========================================================================    
     
    # define circle
    circle = np.zeros((ppc,2))
    
    circle[:,0] = [yi[center[0]]+radius*np.sin(2*np.pi/ppc*i)  for i in range(ppc)]
    circle[:,1] = [wrap_xi[center[1]]+radius*np.cos(2*np.pi/ppc*i) for i in range(ppc)]

    # Loop over circle and already interpolate with the neighbors
    X,Y = np.meshgrid(wrap_xi,yi)

    
    for i,C in enumerate(circle):
        
        # Get distance from every point
        dist_matrix = np.sqrt((X-C[1])**2+(Y-C[0])**2)
        
        idx = np.argsort(dist_matrix,axis = None)[1:NNeighbors+1] # get n_neighbors closest points
        
        neighbors = np.unravel_index(idx,dist_matrix.shape) #2d positions
        neighbors= np.asarray(neighbors).T

        
        # Interpolate
        weights = dist_matrix[neighbors[:,0],neighbors[:,1]]**-2
        int_sigs[i] = np.dot(weights,wrap_signal[neighbors[:,0],neighbors[:,1]])/np.sum(weights)      
    
    
    # Correct circle (to show  the wrapping)
    circle[np.where(circle>xi.max()),1] -= np.abs((xi.max()-xi.min()))
    circle[np.where(circle<xi.min()),1] += np.abs((xi.max()-xi.min()))
    
    return int_sigs, circle

#%% Filaments

def extractFeaturesPhase(phase_prog,
                         feat_list = ['p_range','%_ord','big_leaps','avg_leap','r2'],
                         norm_range = True, leap = np.pi):
    
    '''
    Scalable function that extracts certain features from the phase progressions
    based on a list given by the used. 
    
    Current features:
        - phase range
        - % of ordered phase
        - are there any big leaps? boolean
        - average leap
        - std from leaps
        - r2 from line fit to the phase progression
    '''
    features = np.empty(len(feat_list))
    phase_round = np.round(phase_prog*10)/10 
    phase_diff = np.diff(phase_round)
    
    if 'p_range' in feat_list:
        index = np.where([feat_list[i]=='p_range' for i in range(len(feat_list))])[0]

        
        if norm_range:
            features[index] = (np.nanmax(phase_prog)-np.nanmin(phase_prog))/(2*np.pi)
        else:
            features.append(np.nanmax(phase_prog)-np.nanmin(phase_prog)) # some problem here
     
    if '%_ord' in feat_list:

        index = np.where([feat_list[i]=='%_ord' for i in range(len(feat_list))])[0]
              
        features[index] = phase_diff[phase_diff>=0].shape[0]/phase_diff.shape[0]
     
    if 'big_leaps' in feat_list:
        index = np.where([feat_list[i]=='big_leaps' for i in range(len(feat_list))])[0]
        #Attention: this function has been changed after the qualification 
        # I don't care anymore about the percentage of big leaps. any big leap is bad
#        features[index] = phase_diff[np.abs(phase_diff)>=leap].shape[0]/phase_diff.shape[0]
        
        if np.nanmax(phase_diff)>leap:
            features[index]  = -1
        else:
            features[index]  = 1 # No big leaps
         
    if 'avg_leap' in feat_list:
        index = np.where([feat_list[i]=='avg_leap' for i in range(len(feat_list))])[0]
        phase_round = np.round(phase_prog*10)/10 
        phase_diff = np.diff(phase_round)
        features[index] = np.mean(phase_diff)
        
    if 'std_leap' in feat_list:
        index = np.where([feat_list[i]=='std_leap' for i in range(len(feat_list))])[0]
        phase_round = np.round(phase_prog*10)/10 
        phase_diff = np.diff(phase_round)
        features[index] = np.std(phase_diff)
        
    if 'r2' in feat_list:

        index = np.where([feat_list[i]=='r2' for i in range(len(feat_list))])[0]       
        lm = LinearRegression(fit_intercept=False)
        X = np.linspace(0,phase_prog.shape[0],phase_prog.shape[0]).reshape(-1,1)
        lm.fit(X,phase_prog)
        features[index] = lm.score(X,phase_prog)
        
    return np.asarray(features)

####

def connectComponents3D(volume,nbhood = 26,length = 10):
    '''
     Finds connected components in a binary 3D space
     Parameters:
         volume: binary 3D object (np array)
         nbhood: neighborhood to be searched (work in progress)
         length: to customize 3x3 neighborhoods on the fly
     Returns: Dictionary with connected components 
    '''
    
    # Define neighborhood
    if nbhood==0:
        kernel = np.ones((3,3,length),np.int8)
        k_i,k_j,k_k = kernel.shape
        center = int(np.floor(k_i/2)) 
    if nbhood==26:
        kernel = np.ones((3,3,3),np.int8)
        k_i,k_j,k_k = kernel.shape
        center = int(np.floor(k_i/2))
    else:
        kernel = np.ones(nbhood,np.int8)
        k_i,k_j,k_k = kernel.shape
        center = int(np.floor(k_i/2))
        
    # Negate volume
    volume = np.array(volume,np.int8)
    volume[volume!=0] = -1
    
    # Find points equal to -1
    fi,fj,fk = np.where(volume==-1)
    
    I,J,K = volume.shape
    aux_volume = np.zeros((volume.shape[0]+center*2,
                           volume.shape[1]+center*2,
                           volume.shape[2]+center*2))
    aux_volume[center:I+center,center:J+center,center:K+center] = volume
    
    out_dic = {}
    label_list = []
    label = 0
    for p in range(0,fi.size):# For each point:
        #get mini volume
        target = [fi[p],fj[p],fk[p]]
        minivol = aux_volume[fi[p]:fi[p]+k_i,
                             fj[p]:fj[p]+k_j,
                             fk[p]:fk[p]+k_k]
#        print(minivol)
#        print(label_list)
        # Is there a labelled point in the neighborhood?
        if label!=0:
            ch1 = np.array([item in minivol for item in label_list])
            if ch1.any():
                # if Yes: is there more than one label?
                singular = np.unique(minivol[minivol>0])
                if singular.size>1:
                    # If yes: assign all points from both labels to the lowest label
#                    print('Multiple Labels: %s'%target)
                    new_lab = singular.min()
                    for obj in singular:
                        if obj!=new_lab:
                            out_dic[new_lab] += out_dic[obj]
                            for t in out_dic[obj]:
                                aux_volume[t[0]+center,
                                           t[1]+center,
                                           t[2]+center] = new_lab                               
                            out_dic.pop(obj)
                            # put in auxiliary
                elif singular.size!=0:
                    # IF no: assign label to this point
#                    print('Old Label: %s, %s'%(target,singular[0]))

                    out_dic[int(singular[0])].append(target)
                    aux_volume[fi[p]+center,
                           fj[p]+center,
                           fk[p]+center] = int(singular[0])
                    
            else:
                # If No: assign new label to point
                label+=1
#                print('New Label in: %s,%s'%(target,label))
                out_dic.update({label:[target]})                
                label_list.append(label)
                aux_volume[fi[p]+center,
                           fj[p]+center,
                           fk[p]+center] = label
                 
        else:     
            # If No: assign new label to point
            label+=1 
#            print('New Label out: %s, %s'%(target,label))
            out_dic.update({label:[target]})                
            label_list.append(label)
            aux_volume[fi[p]+center,
                       fj[p]+center,
                       fk[p]+center] = label
    
    #Correct labels afterwards
    final_dic = {}
    key = 1
    for item in out_dic:
        final_dic.update({key:out_dic[item]})
        key += 1

    return final_dic

####

def filterCCDict(ccdict,fs, ms_thresh, get_duration = 1):
    '''
     Elliminates rotors with less then a determined duration threshold
     Parameters:
         ccdict: Dictionary containing rotors
         fs: sampling frequency
         ms_thresh: duration threshold in ms
         get_duration: boolean, selects if a dictionary containing the individual
                       durations should be returned
    '''
    out_dict = {}
    min_length = int(np.round(ms_thresh*fs/1000))
    key = 1
    duration_dict = {}
    for i in ccdict:
        length = np.asarray(ccdict[i])[:, 2].max() - np.asarray(ccdict[i])[:, 2].min() +1
        if length>min_length:
            out_dict.update({key:ccdict[i]})
            
            if get_duration ==1:
                duration_dict.update({key: length*1000/fs})
            
            key += 1
    
    if get_duration==1:
        return out_dict, duration_dict
    else:
        return out_dict

####

def getObjectInfo(image,th): 
    '''
     Gets the center of objects found after applying a certain threshold
     Could be done outside of a function if the contour info is important
    '''
    
    if th>0 and th<=1:
        th = int(th*255)
    
    image = np.uint8((image-image.min())/(image.max()-image.min())*255)
    
    ret,thresh1 = cv2.threshold(image,th,255,cv2.THRESH_BINARY) 
    
    total_area = int(cv2.moments(thresh1)["m00"]/255)
    
    # Find contours 
    cnts,_ = cv2.findContours(thresh1.copy(),mode = cv2.RETR_EXTERNAL,method = cv2.CHAIN_APPROX_SIMPLE)
#    cnts = cnts[0] 
    
    n_obj = len(cnts)
    
    centers = np.zeros((n_obj,2))
    area = np.zeros(n_obj)

    for i,c in enumerate(cnts):
#        c = np.uint8(c)
        
        # compute the center of the contour
        M = cv2.moments(c)
        try:
            centers[i,0] = int(M["m10"] / M["m00"])
            centers[i,1] = int(M["m01"] / M["m00"])
            area[i] = M["m00"]
        except:
            c = c.reshape(-1,2)
            centers[i,0] = np.mean(c[:,0])
            centers[i,1] = np.mean(c[:,1])
            area = 0
        
    thresh1[thresh1!=0] = 1
    return centers,thresh1,total_area

####

def getCenteredFrames(ep,HMG,side,nFrames,iF = 0,it = 2):
    '''
    Centers objects in an edge image. TODO: comment!!
    '''
    
    # Explanation
    cx = int(side/2)
    cy = int(side/2)
    p_dict ={}
    
    # Get uncentered frames
    for j in range(0,nFrames):
        temp_hmg = prep.wrapMap(HMG[:,:,j+iF],cx)
    
        p_dict[j] = np.zeros((side,side))
        temp_hmg = temp_hmg[(ep[0]+(cx+1))-cx:(ep[0]+(cx+1))+cx+1,
                            (ep[1]+cy+1)-cy:(ep[1]+cy+1)+cy+1] 
        p_dict[j][0:temp_hmg.shape[0],0:temp_hmg.shape[1]] = temp_hmg 
    is_empty = np.zeros(len(p_dict))
    #### Elliminate Components
    for p in p_dict:
        img = np.uint8(p_dict[p])   
                
        _,components = cv2.connectedComponents(img, connectivity = 8)
        N = np.unique(components)
    
        i_dict = {}
        n_max = 0        
        for k in N:
            if k!=0:
                x,y = np.where (components==k)
                n_temp = x.shape[0]
                i_dict[k] = np.zeros_like(components)
                i_dict[k][x,y] = 1
                if n_temp > n_max:
                    n_max = n_temp
                    p_dict[p] = i_dict[k]
                    
    #### Center Object
        for j in range(it):
            img = np.uint8(p_dict[p])      
            cnt,_ = cv2.findContours(np.copy(img),
                                         mode = cv2.RETR_EXTERNAL,
                                         method = cv2.CHAIN_APPROX_NONE)
            cnt = np.asarray(cnt)
            try:
                C = cnt[0]
            except:
                is_empty[p] = 1
                continue # Means there's no contour in the image
            # This gets the distance of the corners from the center and use the least
            #distance to center the object
            rect = cv2.minAreaRect(C)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            x = box[:,0]
            y = box[:,1]
        
            d = np.zeros(x.shape)
        
            for k in range(x.shape[0]):
                d[k] = np.sqrt((x[k]-cx)**2+(y[k]-cy)**2)
                
            ind = np.where(d == d.min())[0][0]
            translation_matrix = np.float32([ [1,0,-(x[ind]-cx)],
                                              [0,1,-(y[ind]-cy)] ])
                
            p_dict[p] = cv2.warpAffine(img, translation_matrix, img.shape) # Translation 
            p_dict[p] = np.uint8(p_dict[p])
            
    return p_dict, is_empty

####

def getAngleProgression(frame_array,side):

    '''
    Gets angle progression from rotating edges around filaments. TODO: comment!!
    '''
    # Loop over frames
    angle = np.zeros(frame_array.shape[-1])
    for f in  range(frame_array.shape[-1]):
        
        cframe = frame_array[:,:,f]   
        cx = int(side/2) # Get center of frame
        
        x,y = np.where(cframe!=0)
        x = x-cx; y = y-cx # Get non zero points of frame wrt. center
        
        if x.size==0 or y.size==0:
            angle[f] = angle[f-1]
            continue
        
        # Fit linear model
        model = LinearRegression(fit_intercept=False)
        model.fit(y.reshape(-1,1),x) # Note the inversion of x and y due to the coordinate system
                
        # Determine predominant side
        left = np.sum(cframe[:,:cx+1])
        right = np.sum(cframe[:,cx+1:])

                        
        if left>right:
            angle[f] = np.arctan(model.coef_)+np.pi
        elif right >= left:
            if np.arctan(model.coef_)>=0: #Correct angles to be between 0 and 2*np.pi
                angle[f] = np.arctan(model.coef_)
            else:
                angle[f] = np.arctan(model.coef_)+2*np.pi
        else:
            print('Vish: %d'%f) # In case it is exactly 0
            
    # Check direction of rotation based on derivative
    ag = sig.medfilt(angle,kernel_size=7) # not obligatory

    if np.sum(np.diff(ag)<0) > np.sum(np.diff(ag)>0):
        # Sentido Horário
        sinn = 1
    else:
        # Sentido Anti-Horário
        sinn = -1  
    
    return angle, sinn

####

def evaluateAngleProgression(angle,sinn,fs):
    """
    Evaluates the angle progression obtained via line fit (or, technically, any other method)
    """
    # Normalize the rotation wrt. first angle. Different for clockwise or counterclockwise
    if sinn==-1:
        n_rot = np.zeros_like(angle)
        for i in range(len(angle)):
            if angle[i]>=angle[0]:
                n_rot[i] = angle[i]-angle[0]
            else:
                n_rot[i] = (2*np.pi-(angle[0]-angle[i])) # Half round + difference on other half
        ag = sig.medfilt(n_rot,kernel_size=7)
    else:
        n_rot = np.zeros_like(angle)
        for i in range(len(angle)):
            if angle[i]>angle[0]:
                n_rot[i] = (2*np.pi-(angle[i]-angle[0]))# Half round + difference on other half
            else:
                n_rot[i] = angle[0]-angle[i] 
        ag = sig.medfilt(n_rot,kernel_size=7)
        
    # Detect peaks to evaluate number of cycles
    peaks = detect_peaks(np.diff(ag),valley = True,edge = 'falling',mpd = 10,mph = 1)
    
    # Check the presence of valleys between peaks to see if it is a true cycle
    # or simply noise
    cycle_length = []
    for i in range(len(peaks)-1):
        probe = ag[peaks[i]:peaks[i+1]]
        valleys = detect_peaks(probe,valley = True,mpd = 10)
        if len(valleys)>0:
            cycle_length.append(peaks[i])
            if i == len(peaks)-2:
                cycle_length.append(peaks[i+1])

    # If the number of cycles is smaller than 2, it is hard to predict the frequency
    cycle_length = np.asarray(cycle_length)
    cycle_length = np.diff(cycle_length)/fs
    
    # Estimate number of full cycles and average frequency
    n_cycles = len(cycle_length)
    avg_freq = 1/np.mean(cycle_length) 
    
    return n_cycles, avg_freq

def cyclesFromFilament(filament,HMG,side,fs):
    """
    This merges the routine to extract cycles with the routine to extract
    SP features and the 3D component Analysis
    
    It follows the SPs, that might meander, based on the 3D components, and 
    then marks the cycles
    """
    
    filament = np.asarray(filament)
    
    # Mark beginning and ending frames
    fi = np.min(filament[:,2])
    fe = np.max(filament[:,2])
    
    nF = fe-fi
    # Array to store frames
    frame_array = np.zeros((side,side,nF))
    
    l,c,_ = HMG.shape
    
    for f in range(fi,fe):
        # get the center of the SPs on that particular frame
        sp_loc = filament[filament[:,2]==f]        
        img = np.zeros((l,c),dtype = 'uint8')
        # Guess: maybe take mean right away
        for element in sp_loc:
            img[element[0],element[1]] = 1
        
        center,area,_ = getObjectInfo(img,0.5)
        if len(center)>=2: # todo: mean may not always be a good idea
            center = np.median(center,axis = 0)

        if len(center)!=0:
            # arange the data to fit the other function
            ep = np.zeros(3)
            ep[0] = center.squeeze()[1]
            ep[1] = center.squeeze()[0]
            ep[2] = f
            ep = np.uint8(ep)
            
            # Apply function
            pdict,is_empty = getCenteredFrames(ep,HMG,side,nFrames = 1,iF = f)
        else:
             is_empty = True
         
        if not is_empty:
            try:
                frame_array[:,:,f-fi] = pdict[0]
            except:
                print(f,fi,fe,pdict[0].shape, frame_array.shape)
    if len(np.unique(frame_array))>1:      
    ########################
    # Now, check the rotation behavior
    
        rot,sinn = getAngleProgression(frame_array,side)
        n_cycles,freq = evaluateAngleProgression(rot,sinn,fs)
        
        return freq,n_cycles,sinn#,r2 old
    else:
        return np.nan,np.nan,np.nan#,np.nan
    
def fil_displacement(Dict,xi,yi):
    '''
    This function calculates the displacement of the filament as 
    np.sqrt((displacement in X)**2 + (displacement in Y)**2)
    
    returns median and iqr or mean and std
    '''
    displacement = []
    for sp in Dict:
        temp_sp = np.asarray(Dict[sp])
        temp_sp = temp_sp[temp_sp[:,2].argsort()] 
        x_disp = np.diff(xi[temp_sp[:,1]])        
        y_disp = np.diff(xi[temp_sp[:,0]])        
        
        displacement.append(np.mean(np.sqrt(x_disp**2+y_disp**2)))
        
    displacement = np.asarray(displacement)
                
    ctm = np.mean(displacement)
    dispersion = np.std(displacement)
        
    return ctm, dispersion
#%% Heatmaps

def HeatMap(rotors):
    HM = np.zeros((rotors.shape[0],rotors.shape[1])) 
    for f in range(0,rotors.shape[2]):
        HM += -rotors[:,:,f]
     
    return HM 