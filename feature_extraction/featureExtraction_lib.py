# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 14:28:52 2020

Functions feature extraction


@author: Victor G. Marques (v.goncalvesmarques@maastrichtuniversity.nl)

"""

#%% Python libraries
import sys
import os
import numpy as np
import pandas as pd
import cv2

#%% Other dependencies
root = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(root,'fileIO'))
sys.path.append(os.path.join(root,'accessory'))
sys.path.append(os.path.join(root,'preprocessing'))
sys.path.append(os.path.join(root,'phase_analysis'))

import fileIO_lib as fio
import accessory_lib as acc
import preprocessing_lib as prp
import phaseAnalysis_lib as pha

#%% 

def get_feature(feature_vector,DF_folder,**kwargs):
    ''' 
    This function simply gets the correct filepath for the feature in question and
    loads the data based on optional arrhythmia or basename restrictions.
    If both restrictions are given, only basename is used.
    
    returns feature vector according to names given
    ''' 
    doc_folder =os.path.join('D:\\','vgmar')
    rot_list = [9,18,27,36,45]
    trl_list = [1,2,3,4,5]
    
#    var_names = {'rotation':'rot_','translation':'trl_'}
    parameter = {'rotation':rot_list,'translation':trl_list}
    

    
    option = kwargs.get('option','normal')
    arrhythmia = kwargs.get('arrhythmia','')
    basename = kwargs.get('basename','')
    layout = kwargs.get('layout','HR.mat') 
    

    Layouts = {'HR.mat':'HR',
           'layout_cardioinsight.mat':'layout_cardioinsight',
           'layout_leicester_healthy.mat':'layout_leicester_healthy',
           'layout_guillem.mat':'layout_guillem',
           'layout_salinet.mat':'layout_salinet',
           'layout_uniform32.mat':'layout_uniform32',
           'layout_uniform16.mat':'layout_uniform16',
           'layout_uniform64.mat':'layout_uniform64',
           'layout_bojarnejad.mat':'layout_bojarnejad',
           'layout_oosterom.mat':'layout_oosterom'}
    layout = Layouts[layout]

    
    if option=='segments':
        
        folder_list = ['AFL_0','AFL_1','AFL_10','AFL_11','AFL_12','AFL_2','AFL_3','AFL_4','AFL_5','AFL_6','AFL_7','AFL_8','AFL_9',
                       'AF_0','AF_1','AF_10','AF_11','AF_12','AF_13','AF_14','AF_15','AF_16','AF_17','AF_18','AF_19','AF_2','AF_20',
                       'AF_21','AF_22','AF_23','AF_24','AF_25','AF_26','AF_27','AF_28','AF_29','AF_3','AF_30','AF_31','AF_32','AF_33',
                       'AF_34','AF_35','AF_36','AF_37','AF_38','AF_39','AF_4','AF_40','AF_41','AF_42','AF_43','AF_44','AF_45','AF_46',
                       'AF_47','AF_48','AF_49','AF_5','AF_50','AF_51','AF_52','AF_53','AF_54','AF_55','AF_56','AF_57','AF_58','AF_59',
                       'AF_6','AF_60','AF_61','AF_62','AF_63','AF_64','AF_65','AF_66','AF_67','AF_7','AF_8','AF_9',
                       'AT_0','AT_1','AT_10','AT_11','AT_2','AT_3','AT_4','AT_5','AT_6','AT_7','AT_8','AT_9']
        
        arrhythmia_lims = {'AT':[81,93],'AFL':[0,13],'AF':[13,81]}
    elif option=='rotation' or option=='translation':
        folder_list = ['AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV',
                        'RA_0p25_35','RAA_1to0',
                        'Rotor_RIPV','AF_RotorRIPV','AF_RSPV'];
    else:
        folder_list = np.array(['TA_ICV','TA_LSPV','TA_RAA','TA_RIPV',
                        'Flutter_tip','Flutter_tiphor','Flutter_VCI','Flutter_VP',
                        'AF_LA_09','AF_LA_10','AF_LIPV','Rotor_LSPV',
                        'Rotor_RA','RA_0p25_35','Rotor_RAA','RAA_1to0',
                        'Rotor_RIPV','AF_RotorRIPV','AF_RSPV'])
        
    
        arrhythmia_lims = {'AT':[0,4],'AFL':[4,8],'AF':[8,19]}
        
    if option=='rotation' or option=='translation':
        feature_to_path = {
            'model_freq':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_'+option+'.csv'),
            'HDF_1':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_'+option+'.csv'),
            'mean_1':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_'+option+'.csv'),
            'moda_1':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_'+option+'.csv'),
            'median_1':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_'+option+'.csv'),
            'layout':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_'+option+'.csv'),
            'subject':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_'+option+'.csv'),
            'arrhythmia':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_'+option+'.csv'),
            'number of regions':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'regions_hdf_'+option+'.csv'),
            'avg. size':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'regions_hdf_'+option+'.csv'),
            'std. size':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'regions_hdf_'+option+'.csv'),
            'range':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'regions_hdf_'+option+'.csv'),
            'total area':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'regions_hdf_'+option+'.csv'),
            'Mean Duration (% duration)':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis_'+option+'.csv'),
            'Std. Duration (% Duration)':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis_'+option+'.csv'),
            'Mean Frequency (Hz)':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis_'+option+'.csv'),
            'Std. Frequency (Hz)':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis_'+option+'.csv'),
            'Mean direction of rotation':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis_'+option+'.csv'),
            'Filament rate (N fil/s)':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis_'+option+'.csv'),
            'Number of regions':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis_'+option+'.csv'),
            'Mean region size':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis_'+option+'.csv'),
            'Std. region size':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis_'+option+'.csv'),
            'Mean  %SP/area':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis_'+option+'.csv'),
            'Std. %Sps/area':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis_'+option+'.csv'),
            'SPs/s':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis_'+option+'.csv'),
            '%fdr_front':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%fdr_back':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%fdr_left':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%fdr_right':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%SP_front':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%SP_back':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%SP_left':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%SP_right':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'OI_front':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'OI_back':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'OI_left':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'OI_right':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'OI_mean':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'OI_std':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'IQR_DF':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'IQR_OI':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'fil_displacement_mean':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'fil_displacement_std':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%SP_area_front':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%SP_area_back':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%SP_area_left':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            '%SP_area_right':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features_'+option+'.csv'),
            'labels':os.path.join(doc_folder,'Documents','Data','machine_learning','Models',layout),
            'rotation':os.path.join(doc_folder,'Documents','Data','machine_learning','Models',layout),
            'translation':os.path.join(doc_folder,'Documents','Data','machine_learning','Models',layout),
            'size':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'clusters_filaments_'+option+'.csv'),
            'density':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'clusters_filaments_'+option+'.csv'),
            'bounding_box':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'clusters_filaments_'+option+'.csv'),

            }
    else:
        
        feature_to_path = {
            'model_freq':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_results.csv'),
            'HDF_1':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_results.csv'),
            'mean_1':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_results.csv'),
            'moda_1':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_results.csv'),
            'median_1':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_results.csv'),
            'layout':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_results.csv'),
            'subject':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_results.csv'),
            'arrhythmia':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'DF_results.csv'),
            'number of regions':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'regions_hdf.csv'),
            'avg. size':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'regions_hdf.csv'),
            'std. size':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'regions_hdf.csv'),
            'range':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'regions_hdf.csv'),
            'total area':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'regions_hdf.csv'),
            'Mean Duration (% duration)':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis.csv'),
            'Std. Duration (% Duration)':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis.csv'),
            'Mean Frequency (Hz)':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis.csv'),
            'Std. Frequency (Hz)':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis.csv'),
            'Mean direction of rotation':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis.csv'),
            'Filament rate (N fil/s)':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'filament_analysis.csv'),
            'Number of regions':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis.csv'),
            'Mean region size':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis.csv'),
            'Std. region size':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis.csv'),
            'Mean  %SP/area':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis.csv'),
            'Std. %Sps/area':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis.csv'),
            'SPs/s':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'heatmap_filaments_analysis.csv'),
            '%fdr_front':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%fdr_back':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%fdr_left':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%fdr_right':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%SP_front':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%SP_back':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%SP_left':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%SP_right':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'OI_front':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'OI_back':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'OI_left':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'OI_right':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'OI_mean':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'OI_std':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'IQR_DF':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'IQR_OI':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'fil_displacement_mean':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'fil_displacement_std':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%SP_area_front':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%SP_area_back':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%SP_area_left':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            '%SP_area_right':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'extra_features.csv'),
            'labels':os.path.join(doc_folder,'Documents','Data','machine_learning','Models',layout),
            'size':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'clusters_filaments.csv'),
            'density':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'clusters_filaments.csv'),
            'bounding_box':os.path.join(doc_folder,'Documents','Results','Models',layout,DF_folder,'clusters_filaments.csv'),
            }
     
    if feature_vector=='labels': 
        if option=='segments':
            labels = np.zeros((len(folder_list),2))
            for i, basename in enumerate(folder_list):
                labels[i,0] = fio.fastMatRead(os.path.join(feature_to_path[feature_vector],basename),   
                                          0,'isig_'+layout+'.mat','label_0')
                labels[i,1] = fio.fastMatRead(os.path.join(feature_to_path[feature_vector],basename),
                                          0,'isig_'+layout+'.mat','label_1')
            return labels
        elif option=='normal':
            labels = np.zeros((len(folder_list),2))
            labels[:,0] = np.array([0,0,0,0,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2])
            labels[:,1] = np.array([1,0,1,0,1,1,1,0,0,0,0,0,1,1,1,1,0,0,0])
            return labels
        elif option=='rotation' or option=='translation':
            labels = np.zeros((len(folder_list)*len(parameter[option]),2))
            part_labels = np.array([2,2,2,2,2,2,2,2,2])
            labels[:,0] = np.asarray([part_labels,part_labels,part_labels,part_labels,part_labels]).flatten()
            part_labels = np.array([0,0,0,0,1,1,0,0,0])
            labels[:,1] = np.asarray([part_labels,part_labels,part_labels,part_labels,part_labels]).flatten()
            return labels
        
    if basename=='' and arrhythmia=='':
        if option=='rotation' or option=='translation':
            feature_array = np.zeros((len(folder_list)*len(parameter[option]),len(feature_vector)))
            for i,feature in enumerate(feature_vector):
                for j,par in enumerate(parameter[option]):
                    pd_df = pd.read_csv(feature_to_path[feature])
                    try: # normal indexing
                        feature_array[j*len(folder_list):(j+1)*len(folder_list),i] = np.asarray(pd_df[pd_df[option]==par][feature])
                    except: # dumb indexing (check the csv to understand why)
                        feature_array[j*len(folder_list):(j+1)*len(folder_list),i] = np.asarray(pd_df[feature])[j::5]
                        
        else:
            feature_array = np.zeros((len(folder_list),len(feature_vector)))
            for i,feature in enumerate(feature_vector):
                feature_array[:,i] = np.asarray(pd.read_csv(feature_to_path[feature])[feature])
                feature_array[np.isnan(feature_array[:,i]),i] = np.nanmean(feature_array[:,i])

    elif basename!='': # To be implemented for rotation and translation
        feature_array = np.zeros((1,len(feature_vector)))
        for i,feature in enumerate(feature_vector):
            M = np.where(folder_list==basename)[0][0] 
            feature_array[:,i] = np.asarray(pd.read_csv(feature_to_path[feature])[feature])[M]

    elif arrhythmia!='':# To be implemented for rotation and translation
        feature_array = np.zeros((arrhythmia_lims[arrhythmia][1]-arrhythmia_lims[arrhythmia][0],len(feature_vector)))
        for i,feature in enumerate(feature_vector):  
            feature_array = np.asarray(pd.read_csv(feature_to_path[feature])[feature])
            feature_array[:,i]  =feature_array[arrhythmia_lims[arrhythmia][0]:arrhythmia_lims[arrhythmia][1]]

    # Feature corrections (change from range 0-1 to 0-100 and etc.)
    for i,feature in enumerate(feature_vector):
        # Normalize frequencies to range (2-14 Hz)
        if feature in ['model_freq','HDF_1','mean_1','moda_1','median_1','range']:
#            feature_array[:,i] = norm_2vals(feature_array[:,i],[0,1],extreme_values=[2,14]) 
            'a'
        #Normalize % ranges to 0-1
        elif feature in ['avg. size','total area','Mean Duration (% duration)','Mean region size',
                         'Mean  %SP/area','fil_displacement_mean','fil_displacement_std']:
            feature_array[np.isnan(feature_array[:,i]),i] = np.nanmean(feature_array[:,i])
            feature_array[:,i] = acc.norm_2vals(feature_array[:,i],[0,1])
    
    return feature_array

####

def fil_displacement(Dict,xi,yi):
    '''
    This function calculates the displacement of the filament as 
    np.sqrt((displacement in X)**2 + (displacement in Y)**2)
    
    returns median and iqr or mean and std
    '''
    displacement = []
    for sp in Dict:
        temp_sp = np.asarray(Dict[sp])
        temp_sp = temp_sp[temp_sp[:,2].argsort()] 
        x_disp = np.diff(xi[temp_sp[:,1]])        
        y_disp = np.diff(xi[temp_sp[:,0]])        
        
        displacement.append(np.mean(np.sqrt(x_disp**2+y_disp**2)))
        
    displacement = np.asarray(displacement)
                
    ctm = np.mean(displacement)
    dispersion = np.std(displacement)
        
    return ctm, dispersion


def DFMapFeatures(dfMap,modelFrequency,**kwargs):
    
    OutputDict = {}
    
    r,c = dfMap.shape

    DF_thresh = np.copy(dfMap)
    
    # Regions limited by +- 1 Hz
    DF_thresh[dfMap<(modelFrequency-1)] = 0
    DF_thresh[dfMap>(modelFrequency+1)] = 0
    DF_thresh[dfMap!=0] = 1
    
    # Total percentage
    OutputDict['TotalHDFPercentage'] = np.sum(DF_thresh!=0)/(r*c)*100
    
    
    # HDF Regions
    margin = c
    DF_thresh = prp.wrapMap(DF_thresh,margin)[margin:-margin,:]
    DF_thresh = np.asarray(DF_thresh,dtype = np.uint8)
    
    a,b = np.where(DF_thresh[:,margin:margin+c]==1)
    e,f = np.where(DF_thresh[:,margin:margin+c]==0)   
    # Connected components
    Ncc,labels,stats,centroids = cv2.connectedComponentsWithStats(DF_thresh,connectivity = 8)

    
    # Number of regions
    labs_hdf = np.unique(labels[:,margin:margin+c][a,b])   #unique labels   
    
    # Re-label to remain with only labs_hdf
    for i in labs_hdf:
        lab_area = stats[i,-1]
        if lab_area>1:
            copies = np.where(stats[:,-1]==lab_area)[0]
        else:
            copies = []        
        for C in copies:
            ind_x,ind_y = np.where(labels==C)
            labels[ind_x,ind_y] = i
    
    # get only central portion of labels
    labels = labels[:,margin:margin+c]
    
    # get areas
    un_areas = np.asarray([len(np.where(labels==i)[0]) for i in labs_hdf])

    # Unique labels
    OutputDict['NumberOfRegions']  = len(un_areas)
    
    # Area sizes
    sz_areas = np.sort(un_areas)
    
    OutputDict['MeanAreaSize'] = np.nanmean(sz_areas/(r*c)*100)
    OutputDict['StdAreaSize'] = np.nanstd(sz_areas/(r*c)*100)

    # gradient
    OutputDict['FrequencyRange'] = modelFrequency-np.nanmin(dfMap)
    
    # From every region
    OutputDict['IndividualAreaSize'] = sz_areas/(r*c)*100

    ## For the maps
    if 'ra_list' in kwargs and 'la_list' in kwargs and 'basename' in kwargs: 
        labels[labels!=0] = 1
        if kwargs['basename'] in kwargs['ra_list']:
            OutputDict['DFMapRA'] = labels
        elif kwargs['basename'] in kwargs['la_list']:
            OutputDict['DFMapLA'] = labels = labels
            
    return OutputDict


def FilamentFeatures(spMaps,filamentDurationsList,filamentPositionsDict,edges,samplingFrequency):
    
    
    AllOutputDict = {}
    IndividualOutputDict = {}
    
    # Get overall duration
    IndividualOutputDict['Simulation duration'] = spMaps.shape[-1]/samplingFrequency

    #### Duration
    try:
        AllOutputDict['Length'] = filamentDurationsList/1000/IndividualOutputDict['Simulation duration']*100 # SPs_dur is in ms, 100 is for the %
    
        IndividualOutputDict['Mean Duration'] = np.mean(filamentDurationsList/1000/IndividualOutputDict['Simulation duration'])*100
        IndividualOutputDict['Std. Duration'] = np.std(filamentDurationsList/1000/IndividualOutputDict['Simulation duration'])
        # Get "appearance rate"
        IndividualOutputDict['Filament rate'] = len(filamentPositionsDict)/IndividualOutputDict['Simulation duration']
    except:
        AllOutputDict['Length'] = np.nan
        IndividualOutputDict['Mean Duration'] = np.nan
        IndividualOutputDict['Std. Duration'] = np.nan
        IndividualOutputDict['Filament rate'] = np.nan
        
    # Get other info
    freq_fil = {}
    cycles_fil = {}
    sinn_fil = {}
    
    for key in filamentPositionsDict:
        freq_fil[key],cycles_fil[key],\
        sinn_fil[key] = pha.cyclesFromFilament(filamentPositionsDict[key],edges,11,samplingFrequency)
            
    
    # Frequency
    freqs = np.asarray([freq_fil[key] for key in freq_fil])
    AllOutputDict['Frequency'] = freqs
    
    AllOutputDict['Mean Frequency'] = np.mean(freqs)
    AllOutputDict['Std. Frequency'] = np.std(freqs)
    
    # Sinn
    sinns = np.asarray([sinn_fil[key] for key in sinn_fil])
    IndividualOutputDict['Direction'] = sinns
    AllOutputDict['Mean direction of rotation']= np.mean(sinns)
    
    return AllOutputDict,IndividualOutputDict

def HMFeatures(spMaps,filamentPositionsDict,OI = None):

    
    OIOutputDict = {}
    HMOutputDict = {}
    
    r,c,n = spMaps.shape

    rotors_fil = acc.dict2matrix(filamentPositionsDict,spMaps.shape)
    
    hml = pha.HeatMap(-rotors_fil)
    
    
    hml_b = prp.wrapMap(hml,c)[c:-c,:]
    hml_b[hml_b!=0] = 255; 
    
    hml_b = np.asarray(hml_b,np.uint8)
    # Connected components
    Ncc,mask_L,stats,centroids = cv2.connectedComponentsWithStats(hml_b,
                                                connectivity = 8)
    
    mask = np.zeros_like(mask_L[:,c:-c])
    for i in np.unique(mask_L):
        a,b = np.where(mask_L ==i)
        
        b[b<=c] += c
        b[b>=2*c] -= c
        b[:] -= c
        mask[a,b] = i
    cc_labels = np.unique(mask) # I'm a bit confused now (12/11) as to why this works. but it does
     
    
    stats = stats[cc_labels,:]
    centroids = centroids[cc_labels,:]
        
    ###########################
    # SPs per region
    sp_r = np.zeros(len(cc_labels)-1)
    for i,L in enumerate(cc_labels[1:]):
        temp_mask = np.copy(mask)
        temp_mask[mask!=L] = 0
        temp_mask[mask == L] = 1
        sp_r[i] = np.nansum(temp_mask*hml)

    sp_r = sp_r/np.nansum(sp_r)*100
    ###########################
    # OI   
    if OI is not None:      
        OIOutputDict['Mean OI in Regions'] = np.nanmean(OI[mask!=0])
        OIOutputDict['Std OI in Regions'] = np.nanstd(OI[mask!=0])
        OIOutputDict['Mean OI outside Regions'] = np.nanmean(OI[mask==0])
        OIOutputDict['Std OI outside Regions'] = np.nanstd(OI[mask==0])
    
    ##########################   
#    ax.boxplot(sp_r*100,whis = [5,95],positions = [M])
    
    HMOutputDict['Number of regions'] = len(sp_r) # number of regions
    HMOutputDict['Mean area'] = np.nanmean(stats[1:,-1]/(r*c)*100) # mean area (%)
    HMOutputDict['Std area'] = np.nanstd(stats[1:,-1]/(r*c)*100 )  # std area (%)
    HMOutputDict['Mean SP density'] = np.nanmean(sp_r/(stats[1:,-1]))# mean #SPs/point
    HMOutputDict['Std SP density'] = np.nanstd(sp_r/(stats[1:,-1]))# std #SPs/point
    HMOutputDict['Max SP density']= np.nansum(hml)/n #  max #SPs/point
    
    ## For all clusters
    if OI is not None:
        return HMOutputDict,OIOutputDict
    else:
        return HMOutputDict
    
def MixedFeatures(limits,DFmap,OIMap,modelFreq,spMaps,filamentPositionsDict,xi,yi):
    #limits = front, back, left,right
    torsoHeight = len(yi)
    FreqOutputDict = {} 

    
    FreqOutputDict['IQR_DF'] = np.percentile(DFmap.flatten(),75)-np.percentile(DFmap.flatten(),25)
    FreqOutputDict['IQR_OI'] = np.percentile(OIMap.flatten(),75)-np.percentile(OIMap.flatten(),25)
    
    #Front    
    DF_seg = np.copy(DFmap)[:,limits[0][0]:limits[0][1]]
    
    DF_seg[DF_seg<modelFreq-1] = 0
    DF_seg[DF_seg>modelFreq+1] = 0
    DF_seg[DF_seg!=0] = 1
    
    FreqOutputDict['%fdr_front'] = np.sum(DF_seg)/((limits[0][1]-limits[0][0])*torsoHeight)
    
    #OI
    FreqOutputDict['OI_front'] = np.mean(OIMap[:,limits[0][0]:limits[0][1]])
    
    #Back
    DF_seg = np.copy(DFmap)[:,limits[1][0]:limits[1][1]]
    
    DF_seg[DF_seg<modelFreq-1] = 0
    DF_seg[DF_seg>modelFreq+1] = 0
    DF_seg[DF_seg!=0] = 1
    
    FreqOutputDict['%fdr_back'] = np.sum(DF_seg)/((limits[1][1]-limits[1][0])*torsoHeight)    
    
    #OIMap
    FreqOutputDict['OI_back'] = np.mean(OIMap[:,limits[1][0]:limits[1][1]])    
    
    #Left
    DF_seg = np.copy(DFmap)[:,limits[2][0]:limits[2][1]]
    
    DF_seg[DF_seg<modelFreq-1] = 0
    DF_seg[DF_seg>modelFreq+1] = 0
    DF_seg[DF_seg!=0] = 1
    
    FreqOutputDict['%fdr_left'] = np.sum(DF_seg)/((limits[2][1]-limits[2][0])*torsoHeight)     
    
    #OI
    FreqOutputDict['OI_left'] = np.mean(OIMap[:,limits[2][0]:limits[2][1]])
    
    #Right    
    DF_seg = np.concatenate((np.copy(DFmap)[:,limits[3][0]:limits[3][1]],
                             np.copy(DFmap)[:,limits[3][2]:limits[3][3]]),axis = 1)
    
    DF_seg[DF_seg<modelFreq-1] = 0
    DF_seg[DF_seg>modelFreq+1] = 0
    DF_seg[DF_seg!=0] = 1
    
    FreqOutputDict['%fdr_right'] = np.sum(DF_seg)/((limits[3][1]-limits[3][0]+limits[3][3]-limits[3][2])*torsoHeight)     
   
    #OI
    FreqOutputDict['OI_right'] = np.mean(np.concatenate((np.copy(OIMap)[:,limits[3][0]:limits[3][1]],
                                                     np.copy(OIMap)[:,limits[3][2]:limits[3][3]]),axis = 1))
    
    
    #% Phase
    PhaseOutputDict = {}
    
    r,c,n = spMaps.shape
    
    rotors_fil = acc.dict2matrix(filamentPositionsDict,spMaps.shape)
    HM = pha.HeatMap(-rotors_fil)
    
    # Percentage of SPs wrt. total of SPs
    if np.sum(HM)==0:
        PhaseOutputDict['%SP_front'] = 0
        PhaseOutputDict['%SP_back'] = 0
        PhaseOutputDict['%SP_left'] = 0
        PhaseOutputDict['%SP_right'] = 0
    else:
        #Front    
        HM_seg = np.copy(HM)[:,limits[0][0]:limits[0][1],]
        
        HM_seg[HM_seg!=0] = 1
        
        PhaseOutputDict['%SP_front'] = np.sum(HM_seg)/np.sum(HM)
        
        #Back
        HM_seg = np.copy(HM)[:,limits[1][0]:limits[1][1]]
    
        HM_seg[HM_seg!=0] = 1
        
        PhaseOutputDict['%SP_back'] = np.sum(HM_seg)/np.sum(HM)    
        
        #Left
        HM_seg = np.copy(HM)[:,limits[2][0]:limits[2][1]]
    
        HM_seg[HM_seg!=0] = 1
        
        PhaseOutputDict['%SP_left'] = np.sum(HM_seg)/np.sum(HM)     
        
        #Right    
        HM_seg = np.concatenate((np.copy(HM)[:,limits[3][0]:limits[3][1]],
                                 np.copy(HM)[:,limits[3][2]:limits[3][3]]),axis = 1)
        
        HM_seg[HM_seg!=0] = 1
        
        PhaseOutputDict['%SP_right'] =np.sum(HM_seg)/np.sum(HM)
    
    # Percentage of SPs wrt. area
     #Front    
    HM_seg = np.copy(HM)[:,limits[0][0]:limits[0][1],]
    
    HM_seg[HM_seg!=0] = 1
    
    PhaseOutputDict['%SP_area_front'] = np.sum(HM_seg)/((limits[0][1]-limits[0][0])*torsoHeight)
    
    #Back
    HM_seg = np.copy(HM)[:,limits[1][0]:limits[1][1]]

    HM_seg[HM_seg!=0] = 1
    
    PhaseOutputDict['%SP_area_back'] = np.sum(HM_seg)/((limits[1][1]-limits[1][0])*torsoHeight)   
    
    #Left
    HM_seg = np.copy(HM)[:,limits[2][0]:limits[2][1]]

    HM_seg[HM_seg!=0] = 1
    
    PhaseOutputDict['%SP_area_left'] = np.sum(HM_seg)/((limits[2][1]-limits[2][0])*torsoHeight)      
    
    #Right    
    HM_seg = np.concatenate((np.copy(HM)[:,limits[3][0]:limits[3][1]],
                             np.copy(HM)[:,limits[3][2]:limits[3][3]]),axis = 1)
    
    HM_seg[HM_seg!=0] = 1
    
    PhaseOutputDict['%SP_area_right'] =np.sum(HM_seg)/((limits[3][1]-limits[3][0]+limits[3][3]-limits[3][2])*torsoHeight)     
    
    
    #Other phase features
    try:
        PhaseOutputDict['fil_displacement_mean'], PhaseOutputDict['fil_displacement_std']  = pha.fil_displacement(filamentPositionsDict,xi,yi)#  [0]   
    except: 
        PhaseOutputDict['fil_displacement_mean'] = np.nan  
        PhaseOutputDict['fil_displacement_std'] = np.nan              

    
    return FreqOutputDict,PhaseOutputDict
    

        